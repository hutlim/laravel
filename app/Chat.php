<?php namespace App;

use Ratchet\Wamp\WampServerInterface;
use Ratchet\ConnectionInterface;
use Log;


class Chat implements WampServerInterface {

    protected $clients;
    protected $topics;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
        var_dump('open');
        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $conn, $msg) {
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send($msg);
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
    
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible) {
        var_dump('publish');
        $topic->broadcast($event);
    }

    public function onCall(ConnectionInterface $conn, $id, $topic, array $params) {
        $conn->callError($id, $topic, 'RPC not supported');
    }

    // No need to anything, since WampServer adds and removes subscribers to Topics automatically
    public function onSubscribe(ConnectionInterface $conn, $topic) {
        var_dump('subscribe');
        $this->topics[$topic->getId()] = $topic;
    }
    
    public function onUnSubscribe(ConnectionInterface $conn, $topic) {
        var_dump('unsubscribe');    
    }
    
    public function getTopics(){
        return $this->topics;
    }
    
    public function getTopic($topic){
        return isset($this->topics[$topic]) ? $this->topics[$topic] : '';
    }
}