<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\TransactionType;
use Ramsey\Uuid\Uuid;

class PointTransaction extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'point_transactions';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['date', 'type', 'reference', 'description', 'credit', 'debit', 'balance', 'member_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'date'];
	
	protected $appends = ['type_title'];
    
    protected static function boot(){
        parent::boot();
        
        static::creating(function($model) {
            if(!$model->date){
                $model->date = date('Y-m-d H:i:s');
            }
            
            if(!$model->reference){
                $model->reference = strtoupper(Uuid::uuid4()->toString());
            }                      
            $model->balance = $model->member->point_balance + $model->credit - $model->debit;
        });
        
        static::created(function($model) {
            $amount = $model->credit - $model->debit; 
            if($amount > 0){
                $model->member->increment('point_balance', $amount);
            }
            else{
                $model->member->decrement('point_balance', abs($amount));    
            }
        });
    }
    
	public static function TypeList($all = false){
        $types = PointTransaction::groupBy('type')->get()->pluck('type', 'type')->toArray();
        $list = TransactionType::where('status', 1)->whereIn('code', $types)->get()->pluck('locale_title', 'code');
        if($all){
            $list->prepend(ucfirst(trans('admin_backend.all')), '');    
        }
         
        return $list;
    }

    public function getTypeTitleAttribute(){
        $list = self::TypeList();
        $title = $list->get($this->type);
        return $title ? $title : $this->type;
    }
	
	public function member()
    {
        return $this->belongsTo('App\Models\Member', 'member_id', 'id');
    }
    
    public function transactionType()
    {
        return $this->hasOne('App\Models\TransactionType', 'code', 'type');
    }
}
