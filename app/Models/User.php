<?php namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Param;

class User extends Authenticatable {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
	protected $appends = ['type_title', 'status_title'];

	public static function TypeList($all = false){
        $list = Param::where('status', 1)->where('type', 'user_type')->get()->pluck('locale_title', 'code');
        if($all){
            $list->prepend(ucfirst(trans('admin_backend.all')), '');    
        }
         
        return $list;
    }
    
    public static function StatusList($all = false){
        $list = collect(['1' => ucfirst(trans('user.active')), '0' => ucfirst(trans('user.inactive'))]);
        if($all){
            $list->prepend(ucfirst(trans('admin_backend.all')), '');        
        }
            
        return $list;
    }

	public function getTypeTitleAttribute(){
        $list = self::TypeList();
        $title = $list->get($this->type);
        return $title ? $title : $this->type;
    }

    public function getStatusTitleAttribute(){
        $list = self::StatusList();
        $title = $list->get($this->status);
        return $title ? $title : $this->status;
    }
}
