<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\MavroWallet;

class MavroType extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'mavro_type';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['code', 'title', 'dividend', 'dividend_period', 'dividend_period_type', 'status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
	protected $appends = [];

	public function scopePeriodList($all = false){
        $list = [
            'Daily' => trans('mavro_type.daily'), 
            'Weekly' => trans('mavro_type.weekly'), 
            'Bi-Weekly' => trans('mavro_type.bi-weekly'), 
            'Monthly' => trans('mavro_type.completed'), 
            'Semi-Annually' => trans('mavro_type.semi-annually'), 
            'Annually' => trans('mavro_type.annually')
        ];
        
        return $all ? ['' => trans('mavro_type.all')] + $list : $list;
	}
    
	public function mavroWallets()
    {
        return $this->hasMany('App\Models\MavroWallet', 'type', 'code');
    }
}
