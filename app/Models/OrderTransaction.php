<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

class OrderTransaction extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'order_transactions';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['reference', 'started', 'ended', 'amount', 'status', 'ph_order_id', 'ph_member_id', 'gh_order_id', 'gh_member_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'started', 'ended'];
	
	protected $appends = ['status_title'];

    protected static function boot(){
        parent::boot();
        
        static::creating(function($model) {
            $model->reference = strtoupper(Uuid::uuid4()->toString());
        });
        
        static::saved(function($model) {
            if($model->isDirty('status') && $model->status == 'Received'){
                if($model->phOrder->is_completed){
                    $model->phOrder->status = 'Completed';
                    $model->phOrder->save();
                }
                
                if($model->ghOrder->is_completed){
                    $model->ghOrder->status = 'Completed';
                    $model->ghOrder->save();
                }
            }
            
            if($model->isDirty('status') && ($model->status == 'Cancelled' || $model->status == 'Rejected')){
                $model->phOrder->outstanding += $model->amount;
                if($model->phOrder->outstanding == $model->phOrder->amount){
                    $model->phOrder->status = 'Queue';
                }
                $model->phOrder->save();
                
                $model->ghOrder->outstanding += $model->amount;
                if($model->ghOrder->outstanding == $model->ghOrder->amount){
                    $model->ghOrder->status = 'Queue';
                }
                $model->ghOrder->save();
            }
        });
    }
    
	public function scopeStatusList(){
		return ['Pending' => trans('order_transaction.pending'), 'Paid' => trans('order_transaction.paid'), 'Received' => trans('order_transaction.received'), 'Cancelled' => trans('order_transaction.cancelled'), 'Rejected' => trans('order_transaction.rejected')];
	}

	public function getStatusTitleAttribute(){
		$statusList = self::statusList();
		return isset($statusList[$this->status]) ? $statusList[$this->status] : $this->status;
	}
	
	public function phOrder()
    {
        return $this->belongsTo('App\Models\PHOrder', 'ph_order_id', 'id');
    }
	
	public function phMember()
    {
        return $this->belongsTo('App\Models\Member', 'ph_member_id', 'id');
    }
	
	public function ghOrder()
    {
        return $this->belongsTo('App\Models\GHOrder', 'gh_order_id', 'id');
    }
	
	public function ghMember()
    {
        return $this->belongsTo('App\Models\Member', 'gh_member_id', 'id');
    }
}
