<?php namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Countries;
use App;
use App\Models\Sponsor;
use App\Models\Placement;
use App\Models\Param;

class Member extends Authenticatable {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'members';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['username', 'email', 'name', 'type', 'status', 'mobile', 'passport', 'joined', 'register_country', 'address', 'city', 'region', 'postal', 'country'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token', 'security_password'];

	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'joined'];
	
	protected $appends = ['type_title', 'status_title', 'register_country_title', 'country_title', 'bank_country_title'];
	
    protected static function boot(){
        parent::boot();
        
        static::deleted(function($model) {
            if($model->sponsor){
                $model->sponsor->upline_id = 0;
                $model->sponsor->save();
            }

            if($model->placement){
                $model->placement->upline_id = 0;
                $model->placement->save();
            }
        });
    }
    
    public static function TypeList($all = false){
        $list = Param::where('status', 1)->where('type', 'member_type')->get()->pluck('locale_title', 'code');
        if($all){
            $list->prepend(ucfirst(trans('admin_backend.all')), '');    
        }
         
        return $list;
	}
	
	public static function StatusList($all = false){
        $list = collect(['1' => ucfirst(trans('member.active')), '0' => ucfirst(trans('member.inactive'))]);
        if($all){
            $list->prepend(ucfirst(trans('admin_backend.all')), '');        
        }
		    
        return $list;
	}

	public function getTypeTitleAttribute(){
		$list = self::TypeList();
        $title = $list->get($this->type);
        return $title ? $title : $this->type;
	}

	public function getStatusTitleAttribute(){
		$list = self::StatusList();
        $title = $list->get($this->status);
        return $title ? $title : $this->status;
	}
	
	public function getRegisterCountryTitleAttribute(){
        if($this->register_country){
		    $country = Countries::getOne($this->register_country, App::getLocale());
		    return $country ? $country : $this->register_country;
        }
	}
	
	public function getCountryTitleAttribute(){
        if($this->country){
		    $country = Countries::getOne($this->country, App::getLocale());
            return $country ? $country : $this->country;
        }
	}
	
	public function getBankCountryTitleAttribute(){
        if($this->bank_country){
		    $country = Countries::getOne($this->bank_country, App::getLocale());
            return $country ? $country : $this->bank_country;
        }
	}
	
	public function sponsor()
    {
        return $this->hasOne('App\Models\Sponsor', 'member_id', 'id');
    }
	
	public function placement()
    {
        return $this->hasOne('App\Models\Placement', 'member_id', 'id');
    }
	
	public function mavroWallets()
    {
        return $this->hasMany('App\Models\MavroWallet', 'member_id', 'id');
    }
	
	public function phOrders()
    {
        return $this->hasMany('App\Models\PHOrder', 'member_id', 'id');
    }
	
	public function ghOrders()
    {
        return $this->hasMany('App\Models\GHOrder', 'member_id', 'id');
    }
	
	public function updateSponsor($username){
		if($this->username != Sponsor::defaultSponsor()){
			if(!$this->sponsor){
				$sponsor = new Sponsor();
				$sponsor->id = $this->id;
				$upline = Sponsor::whereHas('member', function($query) use($username)
				{
			    	$query->where('members.username', '=', $username);
				})->firstOrFail();
				$sponsor->upline()->associate($upline);
				$this->sponsor()->save($sponsor);
			}
			else{
				$upline = Sponsor::whereHas('member', function($query) use($username)
				{
			    	$query->where('members.username', '=', $username);
				})->firstOrFail();
				$this->sponsor->upline()->associate($upline);
				$this->sponsor->save();
			}
		}
		
		return $this;
	}
	
	public function updatePlacement($username, $position = null){
		if($this->username != Sponsor::defaultSponsor()){
			if(!$this->placement){
				$placement = new Placement();
				$placement->id = $this->id;
				$upline = Placement::whereHas('member', function($query) use($username)
				{
			    	$query->where('members.username', '=', $username);
				})->firstOrFail();
				$placement->upline()->associate($upline);
				
				if($position != null){
					$placement->seqno = $position;
				}
				
				$this->placement()->save($placement);
			}
			else{
				$upline = Placement::whereHas('member', function($query) use($username)
				{
			    	$query->where('members.username', '=', $username);
				})->firstOrFail();
				$this->placement->upline()->associate($upline);
				
				if($position != null){
					$this->placement->seqno = $position;
				}
				
				$this->placement->save();
			}
		}
		
		return $this;
	}
}
