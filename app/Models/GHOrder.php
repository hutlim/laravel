<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\MavroTransaction;
use Ramsey\Uuid\Uuid;
use Countries;
use App;

class GHOrder extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'gh_orders';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['reference', 'amount', 'outstanding', 'status', 'bank_name', 'bank_currency', 'swift_code', 'bank_country', 'account_number', 'account_holder', 'member_id', 'mavro_transaction_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
	protected $appends = ['status_title', 'status_color', 'bank_country_title', 'is_completed'];

    protected static function boot(){
        parent::boot();
        
        static::creating(function($model) {
            $model->status = 'Queue';
            $model->reference = strtoupper(Uuid::uuid4()->toString());
            $model->outstanding = $model->amount;
        });
        
        static::created(function($model) {
            $mavroTransaction = new MavroTransaction();
            $mavroTransaction->date = date('Y-m-d H:i:s');
            $mavroTransaction->reference = $model->reference;
            $mavroTransaction->type = 'Request Withdrawal';
            $mavroTransaction->debit = $model->amount;
            $model->mavroWallet->mavroTransactions()->save($mavroTransaction);
        });
        
        static::updating(function($model) {
            if($model->isDirty('outstanding') && $model->outstanding != $model->amount && $model->status == 'Queue'){
                $model->status = 'Process';
            }
        });
        
        static::saved(function($model) {
            if($model->isDirty('status') && $model->status == 'Cancelled'){
                $mavroTransaction = new MavroTransaction();
                $mavroTransaction->date = date('Y-m-d H:i:s');
                $mavroTransaction->reference = $model->reference;
                $mavroTransaction->type = 'Cancelled Withdrawal';
                $mavroTransaction->credit = $model->amount;
                $model->mavroWallet->mavroTransactions()->save($mavroTransaction);
            }
            
            if($model->isDirty('status') && $model->status == 'Completed'){
                if($model->mavroWallet->balance <= 0){
                    $model->mavroWallet->closed = date('Y-m-d');
                    $model->mavroWallet->status = 'Closed';
                    $model->mavroWallet->save();
                }
            }
        });
    }
    
	public function scopeStatusList(){
		return ['Queue' => trans('gh_order.queue'), 'Cancelled' => trans('gh_order.cancelled'), 'Process' => trans('gh_order.process'), 'Completed' => trans('gh_order.completed')];
	}

	public function getStatusTitleAttribute(){
		$statusList = self::statusList();
		return isset($statusList[$this->status]) ? $statusList[$this->status] : $this->status;
	}
	
	public function getStatusColorAttribute(){
		switch ($this->getAttribute('status')) {
		    case 'Cancelled':
		        return 'danger';
		        break;
		    case 'Process':
		        return 'warning';
		        break;
		    case 'Completed':
		        return 'success';
		        break;
		    default:
		        return 'info';
		}
	}
	
	public function getBankCountryTitleAttribute(){
		$countryList = Countries::getList(App::getLocale());
		return isset($countryList[$this->bank_country]) ? $countryList[$this->bank_country] : $this->bank_country;
	}
	
	public function getIsCompletedAttribute(){
		return $this->orderTransactions()->where('status', 'Received')->sum('amount') == $this->amount;
	}
	
	public function member()
    {
        return $this->belongsTo('App\Models\Member', 'member_id', 'id');
    }
	
	public function mavroWallet()
    {
        return $this->belongsTo('App\Models\MavroWallet', 'mavro_wallet_id', 'id');
    }
	
	public function orderTransactions()
    {
        return $this->hasMany('App\Models\OrderTransaction', 'gh_order_id', 'id');
    }

	public function match()
	{
		$ghOrder = $this;
		$phOrders = PHOrder::whereIn('status', ['Queue', 'Process'])->whereNotIn('member_id', $ghOrder->orderTransactions()->whereIn('status', ['Cancelled', 'Rejected'])->lists('ph_member_id'))->where('outstanding', '>', 0)->get();
		$amount = $ghOrder->outstanding;
		foreach($phOrders as $phOrder){
			if($amount > 0){
				if($amount > $phOrder->outstanding){
					$ghOrder->outstanding -= $phOrder->outstanding;
					$orderTransaction = new OrderTransaction([
						'amount' => $phOrder->outstanding, 
						'started' => date('Y-m-d H:i:s'), 
						'ended' => date('Y-m-d H:i:s', strtotime('+1 days')),
						'status' => 'Pending'
					]);
					$orderTransaction->ghOrder()->associate($ghOrder);
					$orderTransaction->ghMember()->associate($ghOrder->member);
					$orderTransaction->phOrder()->associate($phOrder);
					$orderTransaction->phMember()->associate($phOrder->member);
					$orderTransaction->save();
					$amount -= $phOrder->outstanding;
					$phOrder->outstanding = 0;
					$phOrder->save();
				}
				else{
					$ghOrder->outstanding -= $amount;
					$orderTransaction = new OrderTransaction([
						'amount' => $amount, 
						'started' => date('Y-m-d H:i:s'), 
						'ended' => date('Y-m-d H:i:s', strtotime('+1 days')),
						'status' => 'Pending'
					]);
					$orderTransaction->ghOrder()->associate($ghOrder);
					$orderTransaction->ghMember()->associate($ghOrder->member);
					$orderTransaction->phOrder()->associate($phOrder);
					$orderTransaction->phMember()->associate($phOrder->member);
					$orderTransaction->save();
					$phOrder->outstanding -= $amount;
					$amount -= $phOrder->outstanding;
					$phOrder->save();
					break;
				}
			}
			else{
				break;
			}
		}
		
		$ghOrder->save();
	}
}
