<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;

class Param extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'params';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['type', 'code', 'title', 'status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'array'
    ];
	
    protected $dates = ['created_at', 'updated_at'];
    
    protected $appends = ['status_title'];
    
    public static function StatusList($all = false){
        $list = collect(['1' => ucfirst(trans('param.active')), '0' => ucfirst(trans('param.inactive'))]);
        if($all){
            $list->prepend(ucfirst(trans('admin_backend.all')), '');        
        }
            
        return $list;
    }
    
    public function getStatusTitleAttribute(){
        $list = self::StatusList();
        $title = $list->get($this->status);
        return $title ? $title : $this->status;
    }
    
    public function getLocaleTitleAttribute(){
        $locale = App::getLocale();
        if(isset($this->title[$locale])){
            return $this->title[$locale];
        } 
    }
}
