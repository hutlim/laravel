<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sponsor extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sponsors';
	
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['left', 'right', 'level', 'seqno', 'member_id', 'upline_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
    protected static function boot(){
        parent::boot();
        
        static::creating(function($model) {
            if($model->upline_id){
                $upline = Sponsor::findOrFail($model->upline_id);
                Sponsor::where('right', '>=', $upline->right)->increment('right', 2);
                Sponsor::where('left', '>=', $upline->right)->increment('left', 2);
                
                $model->left = $upline->right;
                $model->right = $upline->right + 1;
                $model->level = $upline->level + 1;
                $model->seqno = $model->downlines()->count() + 1;
            }
            else{
                $max = Sponsor::max('right');
                $model->left = $max + 1;
                $model->right = $max + 2;
                $model->level = 0;
                $model->seqno = 0;
            }
        });
        
        static::updating(function($model) {
            if($model->isDirty('upline_id')){
                if($model->upline_id){
                    $upline = Sponsor::findOrFail($model->upline_id);
                    $origin_left = $model->left;
                    $origin_right = $model->right;
                    $new_parent_right = $upline->right;
                    $origin_level = $model->level;
                    $new_parent_level = $upline->level;
                    if($new_parent_right < $origin_left){
                        $sql = sprintf("UPDATE sponsors SET `level` = `level` + CASE WHEN `left` BETWEEN %s AND %s - 1 THEN %s - %s + 1 ELSE 0 END, `left` = `left` + CASE WHEN `left` BETWEEN %s AND %s THEN %s - %s WHEN `left` BETWEEN %s AND %s - 1 THEN %s - %s + 1 ELSE 0 END, `right` = `right` + CASE WHEN `right` BETWEEN %s AND %s THEN %s - %s WHEN `right` BETWEEN %s AND %s - 1 THEN %s - %s + 1 ELSE 0 END WHERE `left` BETWEEN %s AND %s OR `right` BETWEEN %s AND %s", $origin_left, $origin_right, $new_parent_level, $origin_level, $origin_left, $origin_right, $new_parent_right, $origin_left, $new_parent_right, $origin_left, $origin_right, $origin_left, $origin_left, $origin_right, $new_parent_right, $origin_left, $new_parent_right, $origin_left, $origin_right, $origin_left, $new_parent_right, $origin_right, $new_parent_right, $origin_right);
                        DB::update($sql);
                    }
                    else if($new_parent_right > $origin_right){
                        $sql = sprintf("UPDATE sponsors SET `level` = `level` + CASE WHEN `left` BETWEEN %s AND %s - 1 THEN %s - %s + 1 ELSE 0 END, `left` = `left` + CASE WHEN `left` BETWEEN %s AND %s THEN %s - %s - 1 WHEN `left` BETWEEN %s + 1 AND %s - 1 THEN %s - %s - 1 ELSE 0 END, `right` = `right` + CASE WHEN `right` BETWEEN %s AND %s THEN %s - %s - 1 WHEN `right` BETWEEN %s + 1 AND %s - 1 THEN %s - %s - 1 ELSE 0 END WHERE `left` BETWEEN %s AND %s OR `right` BETWEEN %s AND %s", $origin_left, $origin_right, $new_parent_level, $origin_level, $origin_left, $origin_right, $new_parent_right, $origin_right, $origin_right, $new_parent_right, $origin_left, $origin_right, $origin_left, $origin_right, $new_parent_right, $origin_right, $origin_right, $new_parent_right, $origin_left, $origin_right, $origin_left, $new_parent_right, $origin_left, $new_parent_right);
                        DB::update($sql);
                    }
                }
                else{
                    $decrement = ($model->groups()->count() * 2) + 2;
                    $new_left = Sponsor::max('right') + 1;
                    $increment = $new_left - $model->left;
                    Sponsor::where('left', '>=', $model->left)->where('right', '<=', $model->right)->increment('left', $increment);
                    Sponsor::where('left', '>=', $new_left)->increment('right', $increment);
                    Sponsor::where('left', '>=', $new_left)->decrement('level', $model->level);
                    Sponsor::where('left', '>', $model->right)->decrement('left', $decrement);
                    Sponsor::where('right', '>', $model->right)->decrement('right', $decrement);
                }
            }
        });
    }
    
	public static function DefaultSponsor(){
		return config('site.default_sponsor');
	}

	public function member()
    {
        return $this->belongsTo('App\Models\Member', 'member_id', 'id');
    }
	
	public function upline()
    {
        return $this->belongsTo('App\Models\Sponsor', 'upline_id', 'id');
    }
	
	public function downlines(){
		return $this->hasMany('App\Models\Sponsor', 'upline_id', 'id');
	}
	
	public function groups($position = null){
		if($position !== null){
			$downline = $this->downlines()->where('seqno', $position)->first();
			$left = $downline ? $downline->left : -1;
			$right = $downline ? $downline->right : -1;
			return self::where('left', '>=', $left)->where('right', '<=', $right);
		}
		
		return self::where('left', '>', $this->left)->where('right', '<', $this->right);
	}
}
