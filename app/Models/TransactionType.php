<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PointTransaction;
use App\Models\CreditTransaction;
use App;

class TransactionType extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'transaction_types';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['code', 'title', 'status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'array'
    ];
	
    protected $dates = ['created_at', 'updated_at'];
	
	protected $appends = ['status_title'];
    
    public static function StatusList($all = false){
        $list = collect(['1' => ucfirst(trans('transaction_type.active')), '0' => ucfirst(trans('transaction_type.inactive'))]);
        if($all){
            $list->prepend(ucfirst(trans('admin_backend.all')), '');        
        }
            
        return $list;
    }
    
    public function getStatusTitleAttribute(){
        $list = self::StatusList();
        $title = $list->get($this->status);
        return $title ? $title : $this->status;
    }
    
    public function getLocaleTitleAttribute(){
        $locale = App::getLocale();
        if(isset($this->title[$locale])){
            return $this->title[$locale];
        } 
    }
    
	public function pointTransactions()
    {
        return $this->hasMany('App\Models\PointTransaction', 'type', 'code');
    }

	public function creditTransactions()
    {
        return $this->hasMany('App\Models\CreditTransaction', 'type', 'code');
    }
}
