<?php namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class DatePivot extends Pivot {
   protected $dates = ['created_at', 'updated_at', 'deleted_at', 'started', 'ended'];
   
}