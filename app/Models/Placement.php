<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Placement extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'placements';
	
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['left', 'right', 'level', 'seqno', 'member_id', 'upline_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
	protected $casts = [
		'max_placement' => 'integer',
		'show_level' => 'integer',
	    'left_point' => 'integer',
	    'right_point' => 'integer'
	];
	
	protected $appends = ['position_title', 'max_placement', 'show_level', 'left_point', 'right_point'];

    protected static function boot(){
        parent::boot();
        
        static::extend_bv(function($model) {
            $model->left_bv = 100;
            $model->left_accumulate_bv = 200;
            $model->left_today_bv = 100;
            $model->left_forward_bv = 300;
            
            $model->right_bv = 400;
            $model->right_accumulate_bv = 200;
            $model->right_today_bv = 100;
            $model->right_forward_bv = 300;
        });
        
        static::creating(function($model) {
            if($model->upline_id){
                $upline = Placement::findOrFail($model->upline_id);
                
                if(!$model->seqno){
                    $model->seqno = 1;
                    $weak_groups = $upline->groups(1)->count();
                    for($i = 2; $i <= Placement::maxDownline(); $i++) {
                        $position_groups = $upline->groups($i)->count();
                        if($weak_groups > $position_groups){
                            $model->seqno = $i;
                            $weak_groups = $position_groups;
                        }
                    }
                    
                    $downline = Placement::where('upline_id', $upline->id)->where('seqno', $model->seqno)->first();
                    while($downline){
                        $model->upline_id = $downline->id;
                        $upline = Placement::findOrFail($downline->id);
                        $downline = Placement::where('upline_id', $upline->id)->where('seqno', $model->seqno)->first();
                    }
                }

                Placement::where('right', '>=', $upline->right)->increment('right', 2);
                Placement::where('left', '>=', $upline->right)->increment('left', 2);
                
                $model->left = $upline->right;
                $model->right = $upline->right + 1;
                $model->level = $upline->level + 1;
            }
            else{
                $max = Placement::max('right');
                $model->left = $max + 1;
                $model->right = $max + 2;
                $model->level = 0;
                $model->seqno = 0;
            }
        });
        
        static::updating(function($model) {
            if($model->isDirty('upline_id')){
                if($model->upline_id){
                    $upline = Placement::findOrFail($model->upline_id);
                    $origin_left = $model->left;
                    $origin_right = $model->right;
                    $new_parent_right = $upline->right;
                    $origin_level = $model->level;
                    $new_parent_level = $upline->level;
                    if($new_parent_right < $origin_left){
                        $sql = sprintf("UPDATE placements SET `level` = `level` + CASE WHEN `left` BETWEEN %s AND %s - 1 THEN %s - %s + 1 ELSE 0 END, `left` = `left` + CASE WHEN `left` BETWEEN %s AND %s THEN %s - %s WHEN `left` BETWEEN %s AND %s - 1 THEN %s - %s + 1 ELSE 0 END, `right` = `right` + CASE WHEN `right` BETWEEN %s AND %s THEN %s - %s WHEN `right` BETWEEN %s AND %s - 1 THEN %s - %s + 1 ELSE 0 END WHERE `left` BETWEEN %s AND %s OR `right` BETWEEN %s AND %s", $origin_left, $origin_right, $new_parent_level, $origin_level, $origin_left, $origin_right, $new_parent_right, $origin_left, $new_parent_right, $origin_left, $origin_right, $origin_left, $origin_left, $origin_right, $new_parent_right, $origin_left, $new_parent_right, $origin_left, $origin_right, $origin_left, $new_parent_right, $origin_right, $new_parent_right, $origin_right);
                        DB::update($sql);
                    }
                    else if($new_parent_right > $origin_right){
                        $sql = sprintf("UPDATE placements SET `level` = `level` + CASE WHEN `left` BETWEEN %s AND %s - 1 THEN %s - %s + 1 ELSE 0 END, `left` = `left` + CASE WHEN `left` BETWEEN %s AND %s THEN %s - %s - 1 WHEN `left` BETWEEN %s + 1 AND %s - 1 THEN %s - %s - 1 ELSE 0 END, `right` = `right` + CASE WHEN `right` BETWEEN %s AND %s THEN %s - %s - 1 WHEN `right` BETWEEN %s + 1 AND %s - 1 THEN %s - %s - 1 ELSE 0 END WHERE `left` BETWEEN %s AND %s OR `right` BETWEEN %s AND %s", $origin_left, $origin_right, $new_parent_level, $origin_level, $origin_left, $origin_right, $new_parent_right, $origin_right, $origin_right, $new_parent_right, $origin_left, $origin_right, $origin_left, $origin_right, $new_parent_right, $origin_right, $origin_right, $new_parent_right, $origin_left, $origin_right, $origin_left, $new_parent_right, $origin_left, $new_parent_right);
                        DB::update($sql);
                    }
                }
                else{
                    $decrement = ($model->groups()->count() * 2) + 2;
                    $new_left = Placement::max('right') + 1;
                    $increment = $new_left - $model->left;
                    Placement::where('left', '>=', $model->left)->where('right', '<=', $model->right)->increment('left', $increment);
                    Placement::where('left', '>=', $new_left)->increment('right', $increment);
                    Placement::where('left', '>=', $new_left)->decrement('level', $model->level);
                    Placement::where('left', '>', $model->right)->decrement('left', $decrement);
                    Placement::where('right', '>', $model->right)->decrement('right', $decrement);
                }
            }
        });
    }
    
	public static function MaxDownline(){
		return config('site.max_downline');
	}
	
	public static function ShowLevel(){
		return config('site.show_level');
	}
	
	public static function PositionList($all = false){
		$positionList = [];
		for ($i = 1; $i <= self::maxDownline(); $i++) {
    		$positionList[$i] = trans('placement.position', ['position' => $i]);
		}
        
        $positionList = collect($positionList);
        
        if($all){
            $positionList->prepend(ucfirst(trans('admin_backend.all')), '');        
        }
        
		return $positionList;
	}

	public function getPositionTitleAttribute(){
		$positionList = self::positionList();
		return isset($positionList[$this->seqno]) ? $positionList[$this->seqno] : $this->seqno;
	}
	
	public function member()
    {
        return $this->belongsTo('App\Models\Member', 'member_id', 'id');
    }
	
	public function upline()
    {
        return $this->belongsTo('App\Models\Placement', 'upline_id', 'id');
    }
	
	public function downlines(){
		return $this->hasMany('App\Models\Placement', 'upline_id', 'id');
	}

	public function groups($position = null){
		if($position !== null){
			$downline = $this->downlines()->where('seqno', $position)->first();
			$left = $downline ? $downline->left : -1;
			$right = $downline ? $downline->right : -1;
			return self::where('left', '>=', $left)->where('right', '<=', $right);
		}
		
		return self::where('left', '>', $this->left)->where('right', '<', $this->right);
	}
	
	public static function extend_bv($callback, $priority = 0)
	{
		static::registerModelEvent('extend_bv', $callback, $priority);
	}

	public function setLeftBvAttribute($value){
		$this->left_bv = $value;
	}
	
	public function getLeftBvAttribute(){
		$this->fireModelEvent('extend_bv');
		return $this->left_bv;
	}
	
	public function setRightBvAttribute($value){
		$this->right_bv = $value;
	}
	
	public function getRightBvAttribute(){
		$this->fireModelEvent('extend_bv');
		return $this->right_bv;
	}
	
	public function setLeftAccumulateBvAttribute($value){
		$this->left_accumulate_bv = $value;
	}
	
	public function getLeftAccumulateBvAttribute(){
		$this->fireModelEvent('extend_bv');
		return $this->left_accumulate_bv;
	}
	
	public function setRightAccumulateBvAttribute($value){
		$this->right_accumulate_bv = $value;
	}
	
	public function getRightAccumulateBvAttribute(){
		$this->fireModelEvent('extend_bv');
		return $this->right_accumulate_bv;
	}
	
	public function setLeftTodayBvAttribute($value){
		$this->left_today_bv = $value;
	}
	
	public function getLeftTodayBvAttribute(){
		$this->fireModelEvent('extend_bv');
		return $this->left_today_bv;
	}
	
	public function setRightTodayBvAttribute($value){
		$this->right_today_bv = $value;
	}
	
	public function getRightTodayBvAttribute(){
		$this->fireModelEvent('extend_bv');
		return $this->right_today_bv;
	}
	
	public function setLeftForwardBvAttribute($value){
		$this->left_forward_bv = $value;
	}
	
	public function getLeftForwardBvAttribute(){
		$this->fireModelEvent('extend_bv');
		return $this->left_forward_bv;
	}
	
	public function setRightForwardBvAttribute($value){
		$this->right_forward_bv = $value;
	}
	
	public function getRightForwardBvAttribute(){
		$this->fireModelEvent('extend_bv');
		return $this->right_forward_bv;
	}
}
