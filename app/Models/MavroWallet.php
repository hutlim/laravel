<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MavroWallet extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'mavro_wallets';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['type', 'reference', 'started', 'closed', 'next_payout', 'balance', 'status', 'member_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'started', 'closed', 'next_payout'];
	
	protected $appends = ['type_title', 'status_title'];

    protected static function boot(){
        parent::boot();
        
        static::creating(function($model) {
            $model->reference = strtoupper(Uuid::uuid4()->toString());
        });
    }
    
	public function scopeTypeList(){
		return ['30_MONTHLY' => '30% Monthly'];
	}

	public function getTypeTitleAttribute(){
		$typeList = self::typeList();
		return isset($typeList[$this->type]) ? $typeList[$this->type] : $this->type;
	}
	
	public function scopeStatusList($all = false){
        $list = [
            'Pending' => trans('marvo_wallet.pending'), 
            'Started' => trans('marvo_wallet.started'), 
            'Closed' => trans('marvo_wallet.closed'), 
            'Cancelled' => trans('marvo_wallet.cancelled')
        ];
        
		return $all ? ['' => trans('marvo_wallet.all')] + $list : $list;;
	}

	public function getStatusTitleAttribute(){
		$statusList = self::statusList();
		return isset($statusList[$this->status]) ? $statusList[$this->status] : $this->status;
	}
	
	public function member()
    {
        return $this->belongsTo('App\Models\Member', 'member_id', 'id');
    }
	
	public function phOrder()
    {
        return $this->hasOne('App\Models\PHOrder', 'mavro_wallet_id', 'id');
    }
	
	public function ghOrder()
    {
        return $this->hasOne('App\Models\GHOrder', 'mavro_wallet_id', 'id');
    }
	
	public function mavroTransactions()
    {
        return $this->hasMany('App\Models\MavroTransaction', 'mavro_wallet_id', 'id');
    }
    
    public function mavroType()
    {
        return $this->hasOne('App\Models\MavroType', 'type', 'code');
    }
}
