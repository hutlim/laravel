<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MavroTransaction extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'mavro_transactions';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['date', 'type', 'reference', 'credit', 'debit', 'balance', 'mavro_wallet_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'date'];
	
	protected $appends = ['type_title'];

    protected static function boot(){
        parent::boot();
        
        static::creating(function($model) {
            $model->balance = $model->mavroWallet->balance + $model->credit - $model->debit;
        });
        
        static::created(function($model) {
            $amount = $model->credit - $model->debit; 
            if($amount > 0){
                $model->mavroWallet->increment('balance', $amount);
            }
            else{
                $model->mavroWallet->decrement('balance', abs($amount));    
            }
        });
    }
    
	public function scopeTypeList(){
		return ['30_MONTHLY' => '30% Monthly'];
	}

	public function getTypeTitleAttribute(){
		$typeList = self::typeList();
		return isset($typeList[$this->type]) ? $typeList[$this->type] : $this->type;
	}
	
	public function mavroWallet()
    {
        return $this->belongsTo('App\Models\MavroWallet', 'mavro_wallet_id', 'id');
    }
	
	public function phOrder()
    {
        return $this->belongsTo('App\Models\PHOrder', 'reference', 'reference');
    }
	
	public function ghOrder()
    {
        return $this->belongsTo('App\Models\GHOrder', 'reference', 'reference');
    }
}
