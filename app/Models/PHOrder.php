<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\MavroWallet;
use App\Models\MavroTransaction;
use Ramsey\Uuid\Uuid;

class PHOrder extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ph_orders';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['type', 'reference', 'amount', 'outstanding', 'status', 'member_id', 'mavro_transaction_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
	protected $appends = ['status_title', 'status_color', 'type_title', 'is_completed'];

    protected static function boot(){
        parent::boot();
        
        static::creating(function($model) {
            $model->status = 'Queue';
            $model->reference = strtoupper(Uuid::uuid4()->toString());
            $model->outstanding = $model->amount;
            
            $mavroWallet = new MavroWallet();
            $mavroWallet->type = $model->type;
            $mavroWallet->status = 'Pending';
            $mavroWallet->member()->associate($model->member);
            $mavroWallet->save();
            
            $model->mavroWallet()->associate($mavroWallet);
        });
        
        static::created(function($model) {
            $mavroTransaction = new MavroTransaction();
            $mavroTransaction->date = date('Y-m-d H:i:s');
            $mavroTransaction->reference = $model->reference;
            $mavroTransaction->type = 'Request Deposit';
            $mavroTransaction->credit = $model->amount;
            $model->mavroWallet->mavroTransactions()->save($mavroTransaction);
        });
        
        static::updating(function($model) {
            if($model->isDirty('outstanding') && $model->outstanding != $model->amount && $model->status == 'Queue'){
                $model->status = 'Process';
            }
        });
        
        static::saved(function($model) {
            if($model->isDirty('status') && $model->status == 'Cancelled'){
                $model->mavroWallet->status = 'Cancelled';
                $model->mavroWallet->save();
            }
            
            if($model->isDirty('status') && $model->status == 'Completed'){
                $model->mavroWallet->started = date('Y-m-d');
                $model->mavroWallet->next_payout = date('Y-m-d', strtotime('+30 days'));
                $model->mavroWallet->status = 'Started';
                $model->mavroWallet->save();
            }
        });
    }
    
	public function scopeStatusList(){
		return ['Queue' => trans('ph_order.queue'), 'Cancelled' => trans('ph_order.cancelled'), 'Process' => trans('ph_order.process'), 'Completed' => trans('ph_order.completed')];
	}

	public function getStatusTitleAttribute(){
		$statusList = self::statusList();
		return isset($statusList[$this->status]) ? $statusList[$this->status] : $this->status;
	}
	
	public function getStatusColorAttribute(){
		switch ($this->getAttribute('status')) {
		    case 'Cancelled':
		        return 'danger';
		        break;
		    case 'Process':
		        return 'warning';
		        break;
		    case 'Completed':
		        return 'success';
		        break;
		    default:
		        return 'info';
		}
	}
	
	public function getTypeTitleAttribute(){
		$typeList = MavroWallet::typeList();
		return isset($typeList[$this->type]) ? $typeList[$this->type] : $this->type;
	}
	
	public function getIsCompletedAttribute(){
		return $this->orderTransactions()->where('status', 'Received')->sum('amount') == $this->amount;
	}
	
	public function member()
    {
        return $this->belongsTo('App\Models\Member', 'member_id', 'id');
    }
	
	public function mavroWallet()
    {
        return $this->belongsTo('App\Models\MavroWallet', 'mavro_wallet_id', 'id');
    }
	
	public function orderTransactions()
    {
        return $this->hasMany('App\Models\OrderTransaction', 'ph_order_id', 'id');
    }
	
	public function match()
	{
		$phOrder = $this;
		$ghOrders = GHOrder::whereIn('status', ['Queue', 'Process'])->whereNotIn('id', $phOrder->orderTransactions()->whereIn('status', ['Cancelled', 'Rejected'])->lists('gh_member_id'))->where('outstanding', '>', 0)->get();
		$amount = $phOrder->outstanding;
		foreach($ghOrders as $ghOrder){
			if($amount > 0){
				if($amount > $ghOrder->outstanding){
					$phOrder->outstanding -= $ghOrder->outstanding;
					$orderTransaction = new OrderTransaction([
						'amount' => $ghOrder->outstanding, 
						'started' => date('Y-m-d H:i:s'), 
						'ended' => date('Y-m-d H:i:s', strtotime('+1 days')),
						'status' => 'Pending'
					]);
					$orderTransaction->phOrder()->associate($phOrder);
					$orderTransaction->phMember()->associate($phOrder->member);
					$orderTransaction->ghOrder()->associate($ghOrder);
					$orderTransaction->ghMember()->associate($ghOrder->member);
					$orderTransaction->save();
					$amount -= $ghOrder->outstanding;
					$ghOrder->outstanding = 0;
					$ghOrder->save();
				}
				else{
					$phOrder->outstanding -= $amount;
					$orderTransaction = new OrderTransaction([
						'amount' => $amount, 
						'started' => date('Y-m-d H:i:s'), 
						'ended' => date('Y-m-d H:i:s', strtotime('+1 days')),
						'status' => 'Pending'
					]);
					$orderTransaction->phOrder()->associate($phOrder);
					$orderTransaction->phMember()->associate($phOrder->member);
					$orderTransaction->ghOrder()->associate($ghOrder);
					$orderTransaction->ghMember()->associate($ghOrder->member);
					$orderTransaction->save();
					$ghOrder->outstanding -= $amount;
					$amount -= $ghOrder->outstanding;
					$ghOrder->save();
					break;
				}
			}
			else{
				break;
			}
		}
		
		$phOrder->save();
	}
}
