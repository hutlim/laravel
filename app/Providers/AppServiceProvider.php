<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Member;
use App\Models\Placement;
use App\Models\MavroWallet;
use Validator;
use DB;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Validator::extend('nonZero', function($attribute, $value, $parameters)
		{
    		return is_numeric($value) && ($value > 0 || $value < 0);
		});
		
		Validator::extend('mavroBalance', function($attribute, $value, $parameters)
		{
			if (count($parameters) < 1)
			{
				throw new InvalidArgumentException("Validation rule mavro_balance requires at least 1 parameters.");
			}
			
			if(isset($parameters[1]) && $parameters[1]){
				$value = abs($value);
			}

			$mavroWallet = MavroWallet::findOrFail($parameters[0]);
    		return $mavroWallet->balance >= $value;
		});
		
		Validator::extend('sponsorUsername', function($attribute, $value, $parameters)
		{
			$id = isset($parameters[0]) ? $parameters[0] : 0;
			$member = Member::where('username', '=', $value)->where('id', '!=', $id)->first();
    		return $member ? $member->sponsor->exists : false;
		});
		
		Validator::extend('placementUsername', function($attribute, $value, $parameters)
		{
			$id = isset($parameters[0]) ? $parameters[0] : 0;
			$member = Member::where('username', '=', $value)->where('id', '!=', $id)->first();
    		return $member ? $member->placement->exists : false;
		});
		
		Validator::extend('placementValidate', function($attribute, $value, $parameters)
		{
			if (count($parameters) < 1)
			{
				throw new InvalidArgumentException("Validation rule placement validate requires at least 1 parameters.");
			}

			$placement = Placement::whereHas('member', function($query) use($parameters)
			{
		    	$query->where('members.username', '=', $parameters[0]);
			})->first();
			
			if($placement){
				return Placement::where('left', '>=', $placement->left)->where('right', '<=', $placement->right)
				->whereHas('member', function($query) use($value)
				{
			    	$query->where('members.username', '=', $value);
				})->count();
			}
			
			return false;
		});
		
		Validator::extend('positionValidate', function($attribute, $value, $parameters)
		{
			if (count($parameters) < 1)
			{
				throw new InvalidArgumentException("Validation rule position validate requires at least 1 parameters.");
			}

			if(!$parameters[0]){
				return true;
			}
			
			$placement = Placement::whereHas('member', function($query) use($value)
			{
		    	$query->where('members.username', '=', $value);
			})->first();
			
			if($placement){
				if(isset($parameters[1])){
					return !Placement::where('seqno', '=', $parameters[0])->where('upline_id', '=', $placement->id)->whereHas('member', function($query) use($parameters)
					{
				    	$query->where('members.id', '!=', $parameters[1]);
					})->count();
				}
				else {
					return !Placement::where('seqno', '=', $parameters[0])->where('upline_id', '=', $placement->id)->count();
				}
			}
			
			return false;
		});
        
        Validator::extend('walletBalance', function($attribute, $value, $parameters)
        {
            if($value > 0){
                return true;    
            }
            
            if (count($parameters) < 2)
            {
                throw new InvalidArgumentException("Validation rule mavro_balance requires at least 2 parameters.");
            }

            $balance_field = sprintf('%s_balance', $parameters[0]);
            $wallet = Member::findOrFail($parameters[1]);
            return $wallet->{$balance_field} >= abs($value);
        });
	}

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
