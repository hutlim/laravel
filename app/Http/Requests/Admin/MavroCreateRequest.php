<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class MavroCreateRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'type' => 'required',
			'username' => 'required|exists:members,username',
            'amount' => 'required|numeric|min:0.01'
		];
	}

	/**
	 * Determine if the member is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::guard('admin')->check();
	}

}
