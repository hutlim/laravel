<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class MemberEditRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'sponsor_username' => sprintf('required|sponsor_username:%s', $this->segment(4)),
			'placement_username' => sprintf('required|placement_username:%s|placement_validate:%s|position_validate:%s,%s', $this->segment(4), $this->input('sponsor_username'), $this->input('placement_position'), $this->segment(4)),
			'username' => sprintf('required|alpha_num|min:6|unique:members,username,%d', $this->segment(4)),
			'email' => 'required|email',
            'name' => 'required|min:3',
            'password' => 'confirmed|min:5',
            'security_password' => 'confirmed|min:5',
            'mobile' => 'required',
            'joined' => 'required|date',
            'register_country' => 'required',
            'type' => 'required'
		];
	}

	/**
	 * Determine if the member is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::guard('admin')->check();
	}

}
