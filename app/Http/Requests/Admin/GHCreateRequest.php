<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class GHCreateRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'username' => 'required|exists:members,username',
			'mavro_wallet_id' => 'required|exists:mavro_wallets,id',
            'amount' => sprintf('required|mavro_balance:%s|numeric|min:0.01', $this->input('mavro_wallet_id')),
            'bank_name' => 'required',
            'bank_currency' => 'required',
            'swift_code' => 'required',
            'bank_country' => 'required',
            'account_number' => 'required',
            'account_holder' => 'required'
		];
	}

	/**
	 * Determine if the member is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::guard('admin')->check();
	}
}
