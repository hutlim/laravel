<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class PlacementEditRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'sponsor_username' => sprintf('required|sponsor_username:%s', $this->segment(4)),
			'placement_username' => sprintf('required|placement_username:%s|placement_validate:%s|position_validate:%s,%s', $this->segment(4), $this->input('sponsor_username'), $this->input('placement_position'), $this->segment(4))
		];
	}

	/**
	 * Determine if the member is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::guard('admin')->check();
	}

}
