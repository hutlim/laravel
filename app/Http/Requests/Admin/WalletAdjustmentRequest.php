<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class WalletAdjustmentRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{                  
		return [
            'adjust_amount' => sprintf('required|numeric|non_zero|wallet_balance:%s,%d', strtolower($this->input('wallet_type')), $this->segment(4)),
            'description' => 'required|min:5',
		];
	}

	/**
	 * Determine if the member is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::guard('admin')->check();
	}

}
