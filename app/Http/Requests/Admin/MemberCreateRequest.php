<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class MemberCreateRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'sponsor_username' => 'required|sponsor_username',
			'placement_username' => sprintf('required|placement_username|placement_validate:%s|position_validate:%s', $this->input('sponsor_username'), $this->input('placement_position')),
			'username' => 'required|alpha_num|min:6|unique:members,username',
			'email' => 'required|email',
            'name' => 'required|min:3',
            'password' => 'required|confirmed|min:5',
            'security_password' => 'required|confirmed|min:5',
            'mobile' => 'required',
            'joined' => 'required|date',
            'register_country' => 'required',
            'type' => 'required'
		];
	}

	/**
	 * Determine if the member is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::guard('admin')->check();
	}

}
