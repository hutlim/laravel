<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class ParamCreateRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'type' => 'required|regex:/^[a-zA-Z_]+$/|min:5',
            'code' => sprintf("required|regex:/^[a-zA-Z_]+$/|min:5|unique:params,code,null,id,type,%s", strtolower($this->input('type'))),
            'title.*' => 'required'
		];
	}

	/**
	 * Determine if the member is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::guard('admin')->check();
	}

}
