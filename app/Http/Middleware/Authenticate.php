<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
	{
        $auth = Auth::guard($guard);
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            }

            if($guard == 'admin'){
                return redirect()->guest(route('auth.admin.login'));
            }
            else if($guard == 'member'){
                return redirect()->guest(route('auth.member.login'));
            }
            else if($guard == 'api'){
                return response('Unauthorized.', 401);
            }
            
            return redirect()->guest('/');
        }

        return $next($request);
	}

}
