<?php namespace App\Http\Middleware;

use Closure;

class Type {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $type
     * @return mixed
     */
    public function handle($request, Closure $next, $data)
    {
        list($type, $guard) = explode("|", $data);
        if($request->user($guard)->type != $type) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            }
            
            return redirect()->guest('/');
        }

        return $next($request);
    }

}
