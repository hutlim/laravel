<?php
namespace App\Http\Controllers\Crud;

use App\Http\Controllers\Controller;
use DB;
use HTML;
use Input;
use Illuminate\Support\Str;
use Validator;
use App\Helpers\Utils;
use Session;
use Redirect;
use Datatables;

class CrudController extends Controller
{
	protected $cruds;
	protected $data;

	function __construct()
	{
		$this->middleware('type:super|admin');
		$this->data['cruds'] = DB::table('crud_table')->get();
		$this->data['table_name'] = '';
	}

	public function getList()
	{
		$this->data['rows'] = DB::table("crud_table")->get();
		return view('crud.list', $this->data);
	}

	public function getCreate()
	{
		return view('crud.create', $this->data);
	}

	public function postCreate()
	{
		$v = Validator::make([
		'crud_name' => Input::get('crud_name'),
		'table_name' => Input::get('table_name'),
		'needle' => Input::get('needle'),
		'creatable' => Input::has('creatable'),
		'editable' => Input::has('editable'),
		'viewable' => Input::has('viewable'),
		'removable' => Input::has('removable'),
		'listable' => Input::has('listable')], [
		'crud_name' => 'required',
		'table_name' => 'required|unique:crud_table,table_name',
		'needle' => 'required',
		'creatable' => 'required',
		'editable' => 'required',
		'viewable' => 'required',
		'removable' => 'required',
		'listable' => 'required']);

		if($v->fails())
		{
			Session::flash('error_msg', Utils::buildMessages($v->errors()->all()));
			return Redirect::to("/crud/create")->withErrors($v)->withInput();
		}
		else
		{
			DB::table('crud_table')->insert([
			'crud_name' => Input::get('crud_name'),
			'table_name' => Input::get('table_name'),
			'slug' => Str::slug(Input::get('table_name')),
			'fontawesome_class' => Input::get('fontawesome_class', 'fa fa-ellipsis-v'),
			'needle' => Input::get('needle'),
			'creatable' => Input::has('creatable'),
			'editable' => Input::has('editable'),
			'viewable' => Input::has('viewable'),
			'removable' => Input::has('removable'),
			'listable' => Input::has('listable'),
			'created_at' => Utils::timestamp(),
			'updated_at' => Utils::timestamp()]);
		}

		Session::flash('success_msg', 'CRUD created successfully');

		return Redirect::to("/table/" . Input::get('table_name') . "/settings");
	}

	public function getEdit($id)
	{
		$this->data['crud'] = DB::table("crud_table")->where('id', $id)->first();
		return view('crud.edit', $this->data);
	}

	public function postEdit($id)
	{
		$this->data['crud'] = DB::table("crud_table")->where('id', $id)->first();

		$v = Validator::make([
		'crud_name' => Input::get('crud_name'),
		'table_name' => Input::get('table_name'),
		'needle' => Input::get('needle'),
		'creatable' => Input::has('creatable'),
		'editable' => Input::has('editable'),
		'viewable' => Input::has('viewable'),
		'removable' => Input::has('removable'),
		'listable' => Input::has('listable')], [
		'crud_name' => 'required',
		'table_name' => 'required',
		'needle' => 'required',
		'creatable' => 'required',
		'editable' => 'required',
		'viewable' => 'required',
		'removable' => 'required',
		'listable' => 'required']);

		if($v->fails())
		{
			Session::flash('error_msg', Utils::buildMessages($v->errors()->all()));
			return Redirect::to("/crud/edit/" . $id)->withErrors($v)->withInput();
		}
		else
		{

			if(DB::table('crud_table')->where('table_name', Input::get('table_name'))->where('id', '!=', $id)->count() > 0)
			{
				Session::flash('error_msg', 'Table name already exist');
				return Redirect::to("/crud/edit/" . $id)->withInput();
			}

			DB::table('crud_table')->where('id', $id)->update([
			'crud_name' => Input::get('crud_name'),
			'table_name' => Input::get('table_name'),
			'slug' => Str::slug(Input::get('table_name')),
			'fontawesome_class' => Input::get('fontawesome_class', 'fa fa-ellipsis-v'),
			'needle' => Input::get('needle'),
			'creatable' => Input::has('creatable'),
			'editable' => Input::has('editable'),
			'viewable' => Input::has('viewable'),
			'removable' => Input::has('removable'),
			'listable' => Input::has('listable'),
			'created_at' => Utils::timestamp(),
			'updated_at' => Utils::timestamp()]);
		}

		Session::flash('success_msg', 'CRUD updated successfully');

		return Redirect::to("/crud/list");
	}

	public function getDelete($id)
	{
		$crud_table = DB::table('crud_table')->where('id', $id)->first();

		DB::table('crud_table_rows')->where('table_name', $crud_table->table_name)->delete();

		DB::table('crud_table')->where('id', $id)->delete();
		Session::flash('success_msg', 'CRUD deleted successfully');
		return Redirect::to("/crud/list")->withInput();
	}

	public function getData()
	{
		$query = DB::table('crud_table')->select('id', 'crud_name', 'table_name', 'fontawesome_class', 'needle', 'creatable', 'editable', 'viewable', 'removable', 'listable', 'created_at');
		return Datatables::of($query)->add_column('actions', '<a href="{{{ URL::to(\'crud/edit/\' . $id ) }}}" class="btn btn-success btn-sm" ><span class="glyphicon glyphicon-pencil"></span> Edit</a> <a href="{{{ URL::to(\'crud/delete/\' . $id ) }}}" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</a>')->make();
	}

}
