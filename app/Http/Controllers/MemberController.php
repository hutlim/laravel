<?php namespace App\Http\Controllers;

use Auth;

class MemberController extends Controller {
    /**
     * Initializer.
     *
     * @return \MemberController
     */
    public function __construct()
    {
    	$this->auth = Auth::guard('member');
		$this->middleware('auth:member');
    }
}
