<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController as Authentication;
use Illuminate\Http\Request;

class AuthController extends Authentication {
    protected $username = 'username';
    protected $guard = 'admin';
    protected $loginView = 'admin.auth.login';
 
    /**
     * Create a new authentication controller instance.
     *
     * @param  Authenticator  $auth
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
		$this->redirectTo = route('admin.home');
        $this->redirectAfterLogout = route('auth.admin.login');  
    }
}
