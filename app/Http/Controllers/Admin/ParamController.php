<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Param;
use App\Http\Requests\Admin\ParamCreateRequest;
use App\Http\Requests\Admin\ParamEditRequest;
use Datatables;
use Illuminate\Http\Request;
use DB;

class ParamController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{
		$statusList = Param::StatusList(true);
		return view('admin.param.list', compact('statusList'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
        $statusList = Param::StatusList();
		return view('admin.param.create', compact('statusList'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate(ParamCreateRequest $request)
	{
		DB::beginTransaction();
		try {
			$param = new Param();
            $param->type = $request->type;
			$param->code = $request->code;
			$param->title = $request->title;
            $param->status = $request->status;
			$param->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.param.create')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.param.edit', ['id' => $param->id])->with('success', trans('admin_backend.data_created'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getEdit($id)
	{

		$param = Param::findOrFail($id);
        $statusList = Param::StatusList();
		return view('admin.param.edit', compact('param', 'statusList'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function postEdit(ParamEditRequest $request, $id)
	{
		DB::beginTransaction();
		try {
			$param = Param::findOrFail($id);
			$param->title = $request->title;
            $param->status = $request->status;
			$param->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.param.edit', ['id' => $param->id])->withErrors($e->getErrors());
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.param.edit', ['id' => $param->id])->with('success', trans('admin_backend.data_saved'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		DB::beginTransaction();
		try {
			$param = Param::findOrFail($id);
			$param->delete();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.param.list')->withErrors($e->getErrors());
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.param.list')->with('success', trans('admin_backend.data_deleted'));
	}
	
    /**
     * Batch activate the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function getBatchActivate(Request $request)
    {
        $i = 0;
        DB::beginTransaction();
        try {
            if($request->ajax()){
                if(is_array($request->ids)){
                    foreach($request->ids as $id){
                        $param = Param::findOrFail($id);
                        if($param->status == 0){
                            $param->status = 1;
                            $param->save();
                            $i++;
                        }
                    }
                }
            }
        } catch(ValidationException $e)
        {
            DB::rollback();
            $i = 0;
        } catch(\Exception $e)
        {
            DB::rollback();
            throw $e;
        }
        DB::commit();
        
        $data = array('total' => $i, 'message' => trans('admin_backend.total_activated', ['total' => $i]));
        return collect($data)->toJson();
    }

    /**
     * Batch deactivate the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function getBatchDeactivate(Request $request)
    {
        $i = 0;
        DB::beginTransaction();
        try {
            if($request->ajax()){
                if(is_array($request->ids)){
                    foreach($request->ids as $id){
                        $param = Param::findOrFail($id);
                        if($param->status == 1){
                            $param->status = 0;
                            $param->save();
                            $i++;
                        }
                    }
                }
            }
        } catch(ValidationException $e)
        {
            DB::rollback();
            $i = 0;
        } catch(\Exception $e)
        {
            DB::rollback();
            throw $e;
        }
        DB::commit();
        
        $data = array('total' => $i, 'message' => trans('admin_backend.total_deactivated', ['total' => $i]));
        return collect($data)->toJson();
    }
    
	/**
	 * Batch delete the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchDelete(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$param = Param::findOrFail($id);
						$param->delete();
						$i++;
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_deleted', ['total' => $i]));
		return collect($data)->toJson();
	}

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		$params = Param::select(array(
			'params.id',
			'params.created_at',
            'params.type',
			'params.code',
			'params.title',
            'params.status'
		));
		
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['created_from'])){
			$params->where('params.created_at', '>=', $filterData['created_from'] . ' 00:00:00');
		}
		
		if(!empty($filterData['created_to'])){
			$params->where('params.created_at', '<=', $filterData['created_to'] . ' 59:59:59');
		}
        
        if(!empty($filterData['type'])){
            $params->where('params.type', '=', $filterData['type']);
        }
		
		if(!empty($filterData['code'])){
			$params->where('params.code', '=', $filterData['code']);
		}
        
        if(!empty($filterData['status'])){
            $params->where('params.status', '=', $filterData['status']);
        }
		
		return Datatables::of($params)
			->edit_column('created_at', function ($param) {
                return $param->created_at->format('d/m/Y H:i:s');
            })
            ->edit_column('title', function ($param) {
                return $param->locale_title;
            })
            ->edit_column('status', function ($param) {
                return $param->status_title;
            })
			->add_column('actions', '<a href="{{ route(\'admin.param.edit\', [\'id\' => $id]) }}" class="btn btn-success btn-sm edit-action" data-title="@lang(\'admin_backend.edit_params\')"><span class="glyphicon glyphicon-pencil"></span> @lang(\'admin_backend.edit\')</a> <a href="{{ route(\'admin.param.delete\', [\'id\' => $id]) }}" class="btn btn-danger btn-sm delete-action"><span class="glyphicon glyphicon-trash"></span> @lang(\'admin_backend.delete\')</a>')
			->make(true);
	}
}
