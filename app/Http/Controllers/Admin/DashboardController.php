<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Member;
use App\Models\PHOrder;
use App\Models\GHOrder;
use App\Models\MavroWallet;
use DB;

class DashboardController extends AdminController {
    public function index()
    {
    	$totalPH = PHOrder::where('status', 'Completed')->get()->sum('amount');
		$totalGH = PHOrder::where('status', 'Completed')->get()->sum('amount');
		$totalMember = Member::get()->count();
		$totalMavro = MavroWallet::where('status', '=', 'Started')->get()->sum('balance');
    	$members = Member::orderBy('joined', 'desc')->limit(8)->get();
		$phOrders = PHOrder::orderBy('created_at', 'desc')->limit(4)->get();
		$ghOrders = GHOrder::orderBy('created_at', 'desc')->limit(4)->get();
		$queuePH = PHOrder::where('status', 'Queue')->get();
		$processPH = PHOrder::where('status', 'Process')->get();
		$queueGH = GHOrder::where('status', 'Queue')->get();
		$processGH = GHOrder::where('status', 'Process')->get();
		$topPH = PHOrder::where('status', 'Completed')->select(DB::raw('member_id, sum(amount) AS total_amount'))->groupBy('member_id')->orderBy('total_amount', 'desc')->limit(3)->get();
        return view('admin.dashboard', compact('totalPH', 'totalGH', 'totalMember', 'totalMavro', 'members', 'phOrders', 'ghOrders', 'queuePH', 'processPH', 'queueGH', 'processGH', 'topPH'));
    }
}
