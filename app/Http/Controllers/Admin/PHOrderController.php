<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Sponsor;
use App\Models\Member;
use App\Models\MavroWallet;
use App\Models\GHOrder;
use App\Models\OrderTransaction;
use App\Models\PHOrder;
use App\Http\Requests\Admin\PHCreateRequest;
use App\Http\Requests\Admin\PHEditRequest;
use Datatables;
use Illuminate\Http\Request;
use DB;

class PHOrderController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{
		// Show the page
		$typeList = ['' => 'All'] + MavroWallet::typeList();
		$statusList = ['' => 'All'] + PHOrder::statusList();
		return view('admin.ph_order.list', compact('typeList', 'statusList'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		$typeList = MavroWallet::typeList();
		$statusList = PHOrder::statusList();
		return view('admin.ph_order.create', compact('typeList', 'statusList'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate(PHCreateRequest $request)
	{
		DB::beginTransaction();
		try {
			$member = Member::where('username', '=', $request->username)->firstOrFail();
			$phOrder = new PHOrder();
			$phOrder->type = $request->type;
			$phOrder->amount = $request->amount;
			$member->phOrders()->save($phOrder);
			
			if($phOrder->outstanding > 0 && ($phOrder->status == 'Queue' || $phOrder->status == 'Process')){
				$phOrder->match();
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.ph_order.create')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.ph_order.edit', ['id' => $phOrder->id])->with('success', trans('admin_backend.data_created'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getEdit($id)
	{

		$phOrder = PHOrder::findOrFail($id);
		$typeList = MavroWallet::typeList();
		$statusList = PHOrder::statusList();
		return view('admin.ph_order.edit', compact('phOrder', 'typeList', 'statusList'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function postEdit(PHEditRequest $request, $id)
	{
		DB::beginTransaction();
		try {
			$phOrder = PHOrder::findOrFail($id);
			$phOrder->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.ph_order.edit', ['id' => $phOrder->id])->withErrors($e->getErrors());
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.ph_order.edit', ['id' => $phOrder->id])->with('success', trans('admin_backend.data_saved'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		DB::beginTransaction();
		try {
			$phOrder = PHOrder::findOrFail($id);
			$phOrder->delete();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.ph_order.list')->withErrors($e->getErrors());
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.ph_order.list')->with('success', trans('admin_backend.data_deleted'));
	}
	
	/**
	 * Batch cancel the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchCancel(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$phOrder = PHOrder::findOrFail($id);
						if($phOrder->status == 'Queue'){
							$phOrder->status = 'Cancelled';
							$phOrder->save();
							$i++;
						}
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_cancelled', ['total' => $i]));
		return collect($data)->toJson();
	}
	
	/**
	 * Update the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getCancelled($id)
	{
		DB::beginTransaction();
		try {
			$orderTransaction = OrderTransaction::findOrFail($id);
			$orderTransaction->status = 'Cancelled';
			$orderTransaction->save();
			$orderTransaction->phOrder->match();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.ph_order.list')->withErrors($e->getErrors());
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.ph_order.list')->with('success', trans('admin_backend.data_cancelled'));
	}
	
	/**
	 * Update the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getPaid($id)
	{
		DB::beginTransaction();
		try {
			$orderTransaction = OrderTransaction::findOrFail($id);
			$orderTransaction->status = 'Paid';
			$orderTransaction->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.ph_order.list')->withErrors($e->getErrors());
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.ph_order.list')->with('success', trans('admin_backend.data_paid'));
	}

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		$phOrders = PHOrder::join('members', 'ph_orders.member_id', '=', 'members.id')->join('sponsors', 'sponsors.member_id', '=', 'members.id')->leftJoin('members as uplines', 'sponsors.upline_id', '=', 'uplines.id')->select(array(
			'ph_orders.id',
			'ph_orders.created_at',
			'uplines.username as sponsor_username',
			'members.username',
			'ph_orders.type',
			'ph_orders.reference',
			'ph_orders.amount',
			'ph_orders.outstanding',
			'ph_orders.status'
		));
		
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['created_from'])){
			$phOrders->where('created_at', '>=', $filterData['created_from'] . ' 00:00:00');
		}
		
		if(!empty($filterData['created_to'])){
			$phOrders->where('created_at', '<=', $filterData['created_to'] . ' 59:59:59');
		}
		
		if(!empty($filterData['sponsor_username'])){
			$sponsor = Sponsor::whereHas('member', function($query) use($filterData)
			{
		    	$query->where('members.username', '=', $filterData['sponsor_username']);
			})->first();
			
			if($sponsor){
				$phOrders->where('sponsors.left', '>=', $sponsor->left)->where('sponsors.right', '<=', $sponsor->right);
			}
			else{
				$phOrders->where('sponsors.left', '>=', 0)->where('sponsors.right', '<=', 0);
			}
		}
		
		if(!empty($filterData['username'])){
			$phOrders->where('members.username', '=', $filterData['username']);
		}
		
		if(!empty($filterData['type'])){
			$phOrders->where('ph_orders.type', '=', $filterData['type']);
		}
		
		if(!empty($filterData['reference'])){
			$phOrders->where('ph_orders.reference', '=', $filterData['reference']);
		}
		
		if(!empty($filterData['status'])){
			$phOrders->where('ph_orders.status', '=', $filterData['status']);
		}
		
		if(isset($filterData['amount']) && $filterData['amount'] > 0){
			$phOrders->where('ph_orders.amount', '>=', $filterData['amount']);
		}

		if(isset($filterData['outstanding']) && $filterData['outstanding'] > 0){
			$phOrders->where('ph_orders.outstanding', '>=', $filterData['outstanding']);
		}
		
		return Datatables::of($phOrders)
			->edit_column('created_at', function ($phOrder) {
                return $phOrder->created_at->format('d/m/Y H:i:s');
            })
			->edit_column('type', function ($phOrder) {
                return $phOrder->type_title;
            })
			->edit_column('status', function ($phOrder) {
                return $phOrder->status_title;
            })
			->add_column('actions', '<a href="{{ route(\'admin.ph_order.edit\', [\'id\' => $id]) }}" class="btn btn-success btn-sm edit-action" data-title="@lang(\'admin_backend.edit_ph_order\')"><span class="glyphicon glyphicon-pencil"></span> @lang(\'admin_backend.edit\')</a> <a href="{{ route(\'admin.ph_order.delete\', [\'id\' => $id]) }}" class="btn btn-danger btn-sm delete-action"><span class="glyphicon glyphicon-trash"></span> @lang(\'admin_backend.delete\')</a>')
			->make(true);
	}

	public function getLoadMember(Request $request){
		$data = array();
		if($request->ajax()){
			$list = Member::where('username', 'like', sprintf('%%%s%%', $request->search))->limit(10)->get();
			foreach($list as $item){
				$data[] = $item->username;
			}
		}
		
		return collect($data)->toJson();
	}

	public function getCheckMember(Request $request){
		$data = array('result' => 'error', 'message' => trans('admin_backend.invalid_member_username'));
		if($request->ajax()){
			if($member = Member::where('username', '=', $request->username)->first()){
				$data = array(
					'result' => 'success',
					'data' => array(
						'id' => $member->id,
						'name' => $member->name
					),
					'message' => ''
				);
			}
		}
		
		return collect($data)->toJson();
	}
}
