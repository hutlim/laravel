<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Member;
use App\Models\TransactionType;
use App\Models\PointTransaction;
use App\Models\CreditTransaction;
use App\Http\Requests\Admin\WalletAdjustmentRequest;
use Datatables;
use Illuminate\Http\Request;
use DB;

class WalletBalanceController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{                    
		// Show the page 
		return view('admin.wallet_balance.list');
	}
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getAdjustment(Request $request, $id)
    {
        $member = Member::findOrFail($id);
        $walletTypeList = ['Point' => ucfirst(trans('admin_backend.point')), 'Credit' => ucfirst(trans('admin_backend.credit'))];
        $transactionTypeList = TransactionType::where('status', 1)->get()->pluck('locale_title', 'code');
        return view('admin.wallet_balance.adjustment', compact(['member', 'walletTypeList', 'transactionTypeList']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postAdjustment(WalletAdjustmentRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            // temporary use full path
            $model = sprintf('\App\Models\%sTransaction', $request->wallet_type);
            $transaction = new $model;
            $transaction->type = $request->transaction_type;
            $transaction->description = $request->description;
            if($request->adjust_amount > 0){
                $transaction->credit = abs($request->adjust_amount);
            }
            else{
                $transaction->debit = abs($request->adjust_amount);    
            }
            $transaction->member_id = $id;
            $transaction->save();
        } catch(ValidationException $e)
        {
            DB::rollback();
            return redirect()->route('admin.wallet_balance.adjustment', ['id' => $id])->withErrors($e->getErrors())->withInput();
        } catch(\Exception $e)
        {
            DB::rollback();
            throw $e;
        }
        
        DB::commit();
        return redirect()->route('admin.wallet_balance.adjustment', ['id' => $id])->with('success', trans('admin_backend.data_created'));
    }

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		$members = Member::select(array(
            'members.id',
			'members.username',
			'members.name',
			'members.email',
			'members.mobile',
			'members.point_balance',
			'members.credit_balance'
		));
		
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['username'])){
			$members->where('members.username', '=', $filterData['username']);
		}
        
        if(!empty($filterData['mobile'])){
            $members->where('members.mobile', '=', $filterData['mobile']);
        }
		
		if(!empty($filterData['email'])){
            $members->where('members.email', '=', $filterData['email']);
        }
        
        if(!empty($filterData['name'])){
            $members->where('members.name', 'like', '%' . $filterData['name'] . '%');
        }
		
		return Datatables::of($members)
            ->add_column('actions', '<a href="{{ route(\'admin.wallet_balance.adjustment\', [\'id\' => $id]) }}" class="btn btn-success btn-sm edit-action" data-title="@lang(\'admin_backend.adjustment\')"><span class="glyphicon glyphicon-pencil"></span> @lang(\'admin_backend.adjustment\')</a>')
			->make(true);
	}
}
