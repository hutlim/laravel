<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Member;
use App\Models\Sponsor;
use App\Models\Placement;
use App\Http\Requests\Admin\SponsorEditRequest;
use Datatables;
use Illuminate\Http\Request;
use DB;
use Countries;
use App;
use Response;

class SponsorController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{
		$defaultSponsor = Sponsor::DefaultSponsor();
		$typeList = Member::TypeList(true);
		$statusList = Member::StatusList(true);
		$countryList = ['' => 'All'] + Countries::getList(App::getLocale());
		return view('admin.sponsor.list', compact('defaultSponsor', 'typeList', 'statusList', 'countryList'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param id
	 * @return Response
	 */
	public function getEdit($id)
	{

		$member = Member::findOrFail($id);
		$positionList = Placement::PositionList();
		return view('admin.sponsor.edit', compact('member', 'positionList'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param id
	 * @return Response
	 */
	public function postEdit(SponsorEditRequest $request, $id)
	{
		DB::beginTransaction();
		try {
			$member = Member::findOrFail($id);
			$member->updateSponsor($request->sponsor_username);
			$member->updatePlacement($request->placement_username, $request->placement_position);
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.sponsor.edit', ['id' => $member->id])->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.sponsor.edit', ['id' => $member->id])->with('success', trans('admin_backend.data_saved'));
	}

	/*
	 * Display a tree of the resource.
	 *
	 * @return Response
	 */
	public function getTree()
	{
		$root = $this->Root();
		return view('admin.sponsor.tree', compact('root'));
	}
	
	public function getProcessTree(Request $request){
		if($request->operation && method_exists($this, $request->operation)) {
			$contents = $this->{$request->operation}($request->id);
			$response = Response::make($contents, 200); 
    		$response->header('Content-Type', 'application/json; charset=utf-8');
			$response->header('Pragma', 'no-cache');
			$response->header('Cache-Control', 'no-cache, must-revalidate');
    		return $response;
        }
		
		abort(404);
	}
	
	protected function Root(){
		return Sponsor::whereHas('member', function($query)
		{
	    	$query->where('members.username', '=', Sponsor::defaultSponsor());
		})->firstOrFail();
	}
	
	protected function Check($username){
		$left = $this->Root()->left;
		$right = $this->Root()->right;
		$sponsor = Sponsor::whereHas('member', function($query) use ($username, $left, $right)
		{
	    	$query->where('members.username', '=', $username);
		})->where('sponsors.left', '>=', $left)->where('sponsors.right', '<=', $right)->first();
		
		if($sponsor){
			$data = array(
				'result' => 'success',
				'id' => $sponsor->id
	        );
		}
		else{
			$data = array(
				'result' => 'error',
	            'msg' => trans('admin_backend.invalid_member_username')
	        );
		}

        return collect($data)->toJson();
	}
	
	protected function TreeRoot($id){
		$sponsor = Sponsor::where('sponsors.left', '>=', $this->Root()->left)->where('sponsors.right', '<=', $this->Root()->right)->find($id);
		
		if($sponsor){
			$nodes = array(
				$sponsor->left => array(
					'id' => sprintf('node_%s', $sponsor->id),
					'text' => sprintf('<span class="tree-title">%s</span> <span class="tree-title">%s</span> <span class="tree-title">%s</span> <span class="tree-title">%s</span> <span class="tree-title">%s</span> <span class="tree-title">%s</span>', strtoupper($sponsor->member->username), strtoupper($sponsor->member->type_title), strtoupper($sponsor->member->status_title), $sponsor->member->joined->format('d/m/Y'), $sponsor->downlines()->count(), $sponsor->groups()->count()),
					'data' => array(
						'id' => $sponsor->id,
						'username' => $sponsor->member->username,
						'type' => $sponsor->member->type,
						'status' => $sponsor->member->status
					),
					'children' => $sponsor->downlines()->count() ? true : false,
					'state' => array('opened' => $sponsor->downlines()->count() ? true : false)
				)
			);
			
			if($sponsor->id != $this->Root()->id){
				while($sponsor = Sponsor::where('sponsors.left', '>=', $this->Root()->left)->where('sponsors.right', '<=', $this->Root()->right)->find($sponsor->upline_id)){
					$nodes[$sponsor->left] = array(
						'id' => sprintf('node_%s', $sponsor->id),
						'text' => sprintf('<span class="tree-title">%s</span> <span class="tree-title">%s</span> <span class="tree-title">%s</span> <span class="tree-title">%s</span> <span class="tree-title">%s</span> <span class="tree-title">%s</span>', strtoupper($sponsor->member->username), strtoupper($sponsor->member->type_title), strtoupper($sponsor->member->status_title), $sponsor->member->joined->format('d/m/Y'), $sponsor->downlines()->count(), $sponsor->groups()->count()),
						'data' => array(
							'id' => $sponsor->id,
							'username' => $sponsor->member->username,
							'type' => $sponsor->member->type,
							'status' => $sponsor->member->status
						)
					);
				}
				
				ksort($nodes);
	        }

			$data = array(
				'result' => 'success',
	            'nodes' => array_values($nodes)
	        );
		}
		else{
			$data = array(
				'result' => 'error',
	            'msg' => 'Invalid member'
	        );
		}

        return collect($data)->toJson();
    }
	
	protected function TreeChild($id){
		$sponsors = Sponsor::where('sponsors.left', '>=', $this->Root()->left)->where('sponsors.right', '<=', $this->Root()->right)->where('sponsors.upline_id', '=', $id)->get();
		
		if($sponsors->count()){
			$nodes = array();
			foreach($sponsors as $sponsor){
				$nodes[$sponsor->left] = array(
					'id' => sprintf('node_%s', $sponsor->id),
					'text' => sprintf('<span class="tree-title">%s</span> <span class="tree-title">%s</span> <span class="tree-title">%s</span> <span class="tree-title">%s</span> <span class="tree-title">%s</span> <span class="tree-title">%s</span>', strtoupper($sponsor->member->username), strtoupper($sponsor->member->type_title), strtoupper($sponsor->member->status_title), $sponsor->member->joined->format('d/m/Y'), $sponsor->downlines()->count(), $sponsor->groups()->count()),
					'data' => array(
						'id' => $sponsor->id,
						'username' => $sponsor->member->username,
						'type' => $sponsor->member->type,
						'status' => $sponsor->member->status
					),
					'children' => $sponsor->downlines()->count() ? true : false
				);
				
				$data = array(
					'result' => 'success',
		            'nodes' => array_values($nodes)
		        );

	        }
		}
		else{
			$data = array(
				'result' => 'error',
	            'msg' => 'Invalid member'
	        );
		}

        return collect($data)->toJson();
    }
    
    function MemberDetails($id){
    	$sponsor = Sponsor::where('sponsors.left', '>=', $this->Root()->left)->where('sponsors.right', '<=', $this->Root()->right)->find($id);
		
		if($sponsor){
			$data = array(
				'result' => 'success',
				'contents' => view('admin.sponsor.member_details', compact('sponsor'))->render()
			);
		}
		else{
			$data = array(
				'result' => 'error',
	            'msg' => 'Invalid member'
	        );
		}
		
		return collect($data)->toJson();
    }

	/**
	 * Batch activate the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchActivate(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$member = Member::findOrFail($id);
						if($member->status == 0){
							$member->status = 1;
							$member->save();
							$i++;
						}
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_activated', ['total' => $i]));
		return collect($data)->toJson();
	}

	/**
	 * Batch deactivate the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchDeactivate(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$member = Member::findOrFail($id);
						if($member->status == 1){
							$member->status = 0;
							$member->save();
							$i++;
						}
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_deactivated', ['total' => $i]));
		return collect($data)->toJson();
	}

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['sponsor_username'])){
			$sponsor = Sponsor::whereHas('member', function($query) use($filterData)
			{
		    	$query->where('members.username', '=', $filterData['sponsor_username']);
			})->first();
		}
		else{
			$sponsor = Sponsor::whereHas('member', function($query)
			{
		    	$query->where('members.username', '=', Sponsor::defaultSponsor());
			})->first();
		}
		
		$members = Member::join('sponsors', 'sponsors.member_id', '=', 'members.id')->leftJoin('members as uplines', 'sponsors.upline_id', '=', 'uplines.id')->join('placements', 'placements.member_id', '=', 'members.id');
		
		if($sponsor){
			$members->select(array(
				'members.id',
				\DB::raw(sprintf('sponsors.level - %s as level', $sponsor->level)),
				'uplines.username as sponsor_username',
				'members.username',
				'members.type',
				'members.status',
				'members.joined',
				'members.register_country',
				\DB::raw('(SELECT count(sponsor_downlines.member_id) FROM sponsors as sponsor_downlines WHERE sponsor_downlines.upline_id = sponsors.id) as direct_downlines'),
				\DB::raw('(SELECT count(placement_downlines.member_id) FROM placements as placement_downlines WHERE placement_downlines.left > placements.left AND placement_downlines.left < placements.right) as group_placements'),
			));
			
			$members->where('sponsors.left', '>=', $sponsor->left)->where('sponsors.right', '<=', $sponsor->right);
		}
		else{
			$members->select(array(
				'members.id',
				'sponsors.level',
				'uplines.username as sponsor_username',
				'members.username',
				'members.type',
				'members.status',
				'members.joined',
				'members.register_country',
				\DB::raw('(SELECT count(sponsor_downlines.member_id) FROM sponsors as sponsor_downlines WHERE sponsor_downlines.upline_id = sponsors.id) as direct_downlines'),
				\DB::raw('(SELECT count(placement_downlines.member_id) FROM placements as placement_downlines WHERE placement_downlines.left > placements.left AND placement_downlines.left < placements.right) as group_placements'),
			));
			
			$members->where('sponsors.left', '>=', 0)->where('sponsors.right', '<=', 0);
		}
		
		if(isset($filterData['level_from']) && $filterData['level_from'] >= 0){
			$members->where('sponsors.level', '>=', $filterData['level_from']);
		}
		
		if(isset($filterData['level_to']) && $filterData['level_to'] > 0){
			$members->where('sponsors.level', '<=', $filterData['level_to']);
		}
		
		if(isset($filterData['direct_downlines_from']) && $filterData['direct_downlines_from'] >= 0){
			$members->having('direct_downlines', '>=', $filterData['direct_downlines_from']);
		}
		
		if(isset($filterData['direct_downlines_to']) && $filterData['direct_downlines_to'] > 0){
			$members->having('direct_downlines', '<=', $filterData['direct_downlines_to']);
		}

		if(isset($filterData['group_placements_from']) && $filterData['group_placements_from'] >= 0){
			$members->having('group_placements', '>=', $filterData['group_placements_from']);
		}
		
		if(isset($filterData['group_placements_to']) && $filterData['group_placements_to'] > 0){
			$members->having('group_placements', '<=', $filterData['group_placements_to']);
		}
		
		if(!empty($filterData['type'])){
			$members->where('members.type', '=', $filterData['type']);
		}
		
		if(!empty($filterData['status'])){
			$members->where('members.status', '=', $filterData['status']);
		}

		if(!empty($filterData['joined_from'])){
			$members->where('members.joined', '>=', $filterData['joined_from']);
		}
		
		if(!empty($filterData['joined_to'])){
			$members->where('members.joined', '<=', $filterData['joined_to']);
		}

		if(!empty($filterData['register_country'])){
			$members->where('members.register_country', '=', $filterData['register_country']);
		}

		return Datatables::of($members)
			->edit_column('type', function ($member) {
                return $member->type_title;
            })
			->edit_column('status', function ($member) {
                return $member->status_title;
            })
			->edit_column('joined', function ($member) {
                return $member->joined->format('d/m/Y');
            })
			->edit_column('register_country', function ($member) {
                return $member->register_country_title;
            })
			->add_column('actions', '@if ($id != "1")<a href="{{ route(\'admin.sponsor.edit\', [\'id\' => $id]) }}" class="btn btn-success btn-sm edit-action" data-title="@lang(\'admin_backend.edit_genealogy\')"><span class="glyphicon glyphicon-pencil"></span> @lang(\'admin_backend.edit\')</a>@endif')
			->make(true);
	}

}
