<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Sponsor;
use App\Models\Member;
use App\Models\MavroWallet;
use App\Models\MavroTransaction;
use App\Http\Requests\Admin\MavroCreateRequest;
use App\Http\Requests\Admin\MavroEditRequest;
use Datatables;
use Illuminate\Http\Request;
use DB;

class MavroWalletController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{
		// Show the page
		$typeList = MavroWallet::typeList(true);
		$statusList = MavroWallet::statusList(true);
		return view('admin.mavro_wallet.list', compact('typeList', 'statusList'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		$typeList = MavroWallet::typeList();
		$statusList = MavroWallet::statusList();
		return view('admin.mavro_wallet.create', compact('typeList', 'statusList'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate(MavroCreateRequest $request)
	{
		DB::beginTransaction();
		try {
			$member = Member::where('username', '=', $request->username)->firstOrFail();
			$mavroWallet = new MavroWallet();
			$mavroWallet->type = $request->type;
			$mavroWallet->started = date('Y-m-d');
			$mavroWallet->next_payout = date('Y-m-d', strtotime('+30 days'));
			$mavroWallet->status = 'Started';
			$member->mavroWallets()->save($mavroWallet);
			
			$mavroTransaction = new MavroTransaction();
			$mavroTransaction->date = date('Y-m-d H:i:s');
			$mavroTransaction->reference = $mavroWallet->reference;
			$mavroTransaction->type = 'Adjustment';
			$mavroTransaction->credit = $request->amount;
			$mavroWallet->mavroTransactions()->save($mavroTransaction);
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.mavro_wallet.create')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.mavro_wallet.edit', ['id' => $mavroWallet->id])->with('success', trans('admin_backend.data_created'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getEdit($id)
	{

		$mavroWallet = MavroWallet::findOrFail($id);
		$typeList = MavroWallet::typeList();
		$statusList = MavroWallet::statusList();
		return view('admin.mavro_wallet.edit', compact('mavroWallet', 'typeList', 'statusList'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function postEdit(MavroEditRequest $request, $id)
	{
		DB::beginTransaction();
		try {
			$mavroWallet = MavroWallet::findOrFail($id);
			$mavroTransaction = new MavroTransaction();
			$mavroTransaction->date = date('Y-m-d H:i:s');
			$mavroTransaction->reference = $mavroWallet->reference;
			$mavroTransaction->type = 'Adjustment';
			if($request->adjust > 0){
				$mavroTransaction->credit = $request->adjust;
			}
			else{
				$mavroTransaction->debit = abs($request->adjust);
			}
			$mavroWallet->mavroTransactions()->save($mavroTransaction);
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.mavro_wallet.edit', ['id' => $mavroWallet->id])->withErrors($e->getErrors());
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.mavro_wallet.edit', ['id' => $mavroWallet->id])->with('success', trans('admin_backend.data_saved'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		DB::beginTransaction();
		try {
			$mavroWallet = MavroWallet::findOrFail($id);
			$mavroWallet->delete();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.mavro_wallet.list')->withErrors($e->getErrors());
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.mavro_wallet.list')->with('success', trans('admin_backend.data_deleted'));
	}
	
	/**
	 * Batch cancel the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchCancel(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$mavroWallet = MavroWallet::findOrFail($id);
						if($mavroWallet->status == 'Pending'){
							$mavroWallet->status = 'Cancelled';
							$mavroWallet->save();
							$i++;
						}
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_cancelled', ['total' => $i]));
		return collect($data)->toJson();
	}
	
	/**
	 * Batch close the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchClose(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$mavroWallet = MavroWallet::findOrFail($id);
						if($mavroWallet->status != 'Cancelled' && $mavroWallet->status != 'Closed'){
							$mavroWallet->status = 'Closed';
							$mavroWallet->save();
							$i++;
						}
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_closed', ['total' => $i]));
		return collect($data)->toJson();
	}

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		$mavroWallets = MavroWallet::join('members', 'mavro_wallets.member_id', '=', 'members.id')->join('sponsors', 'sponsors.member_id', '=', 'members.id')->leftJoin('members as uplines', 'sponsors.upline_id', '=', 'uplines.id')->select(array(
			'mavro_wallets.id',
			'mavro_wallets.created_at',
			'uplines.username as sponsor_username',
			'members.username',
			'mavro_wallets.type',
			'mavro_wallets.reference',
			'mavro_wallets.started',
			'mavro_wallets.closed',
			'mavro_wallets.next_payout',
			'mavro_wallets.balance',
			'mavro_wallets.status'
		));
		
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['created_from'])){
			$mavroWallets->where('created_at', '>=', $filterData['created_from'] . ' 00:00:00');
		}
		
		if(!empty($filterData['created_to'])){
			$mavroWallets->where('created_at', '<=', $filterData['created_to'] . ' 59:59:59');
		}
		
		if(!empty($filterData['sponsor_username'])){
			$sponsor = Sponsor::whereHas('member', function($query) use($filterData)
			{
		    	$query->where('members.username', '=', $filterData['sponsor_username']);
			})->first();
			
			if($sponsor){
				$mavroWallets->where('sponsors.left', '>', $sponsor->left)->where('sponsors.right', '<', $sponsor->right);
			}
			else{
				$mavroWallets->where('sponsors.left', '>', 0)->where('sponsors.right', '<', 0);
			}
		}
		
		if(!empty($filterData['username'])){
			$mavroWallets->where('members.username', '=', $filterData['username']);
		}
		
		if(!empty($filterData['type'])){
			$mavroWallets->where('mavro_wallets.type', '=', $filterData['type']);
		}
		
		if(!empty($filterData['reference'])){
			$mavroWallets->where('mavro_wallets.reference', '=', $filterData['reference']);
		}
		
		if(!empty($filterData['started_from'])){
			$mavroWallets->where('mavro_wallets.started', '>=', $filterData['started_from'] . ' 00:00:00');
		}
		
		if(!empty($filterData['started_to'])){
			$mavroWallets->where('mavro_wallets.started', '<=', $filterData['started_to'] . ' 59:59:59');
		}
		
		if(!empty($filterData['closed_from'])){
			$mavroWallets->where('mavro_wallets.closed', '>=', $filterData['closed_from'] . ' 00:00:00');
		}
		
		if(!empty($filterData['closed_to'])){
			$mavroWallets->where('mavro_wallets.closed', '<=', $filterData['closed_to'] . ' 59:59:59');
		}
		
		if(!empty($filterData['payout_from'])){
			$mavroWallets->where('mavro_wallets.next_payout', '>=', $filterData['payout_from'] . ' 00:00:00');
		}
		
		if(!empty($filterData['payout_to'])){
			$mavroWallets->where('mavro_wallets.next_payout', '<=', $filterData['payout_to'] . ' 59:59:59');
		}
		
		if(!empty($filterData['status'])){
			$mavroWallets->where('mavro_wallets.status', '=', $filterData['status']);
		}
		
		if(isset($filterData['balance']) && $filterData['balance'] > 0){
			$mavroWallets->where('mavro_wallets.balance', '>=', $filterData['balance']);
		}
		
		return Datatables::of($mavroWallets)
			->edit_column('created_at', function ($mavroWallet) {
                return $mavroWallet->created_at->format('d/m/Y H:i:s');
            })
			->edit_column('started', function ($mavroWallet) {
                return $mavroWallet->started ? $mavroWallet->started->format('d/m/Y') : 'n/a';
            })
			->edit_column('closed', function ($mavroWallet) {
                return $mavroWallet->closed ? $mavroWallet->closed->format('d/m/Y') : 'n/a';
            })
			->edit_column('next_payout', function ($mavroWallet) {
                return $mavroWallet->next_payout ? $mavroWallet->next_payout->format('d/m/Y') : 'n/a';
            })
			->edit_column('type', function ($mavroWallet) {
                return $mavroWallet->type_title;
            })
			->edit_column('status', function ($mavroWallet) {
                return $mavroWallet->status_title;
            })
			->add_column('actions', '<a href="{{ route(\'admin.mavro_wallet.edit\', [\'id\' => $id]) }}" class="btn btn-success btn-sm edit-action" data-title="@lang(\'admin_backend.edit_mavro_wallet\')"><span class="glyphicon glyphicon-pencil"></span> @lang(\'admin_backend.edit\')</a> <a href="{{ route(\'admin.mavro_wallet.delete\', [\'id\' => $id]) }}" class="btn btn-danger btn-sm delete-action"><span class="glyphicon glyphicon-trash"></span> @lang(\'admin_backend.delete\')</a>')
			->make(true);
	}

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getTransactions(Request $request)
	{
		$mavroTransactions = MavroTransaction::select(array(
			'id',
			'date',
			'type',
			'reference',
			'credit',
			'debit',
			'balance'
		))->where('mavro_wallet_id', '=', $request->id);
		
		return Datatables::of($mavroTransactions)
			->edit_column('date', function ($mavroTransaction) {
                return $mavroTransaction->date->format('d/m/Y H:i:s');
            })
			->make(true);
	}
	
	public function getLoadMember(Request $request){
		$data = array();
		if($request->ajax()){
			$list = Member::where('username', 'like', sprintf('%%%s%%', $request->search))->limit(10)->get();
			foreach($list as $item){
				$data[] = $item->username;
			}
		}
		
		return collect($data)->toJson();
	}

	public function getCheckMember(Request $request){
		$data = array('result' => 'error', 'message' => trans('admin_backend.invalid_member_username'));
		if($request->ajax()){
			if($member = Member::where('username', '=', $request->username)->first()){
				$data = array(
					'result' => 'success',
					'data' => array(
						'id' => $member->id,
						'name' => $member->name
					),
					'message' => trans('admin_backend.member_username_valid')
				);
			}
		}
		
		return collect($data)->toJson();
	}
}
