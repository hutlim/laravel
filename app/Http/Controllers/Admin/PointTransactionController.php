<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Member;
use App\Models\PointTransaction;
use Datatables;
use Illuminate\Http\Request;
use DB;

class PointTransactionController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{
		// Show the page  
		$typeList = PointTransaction::TypeList(true); 
		return view('admin.point_transaction.list', compact('typeList'));
	}

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		$pointTransactions = PointTransaction::join('members', 'point_transactions.member_id', '=', 'members.id')->select(array(
			'point_transactions.id',
			'point_transactions.date',
			'members.username',
			'point_transactions.type',
			'point_transactions.reference',
			'point_transactions.description',
			'point_transactions.credit',
			'point_transactions.debit'
		));
		
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['date_from'])){
			$pointTransactions->where('point_transactions.date', '>=', $filterData['date_from'] . ' 00:00:00');
		}
		
		if(!empty($filterData['date_to'])){
			$pointTransactions->where('point_transactions.date', '<=', $filterData['date_to'] . ' 59:59:59');
		}
		
		if(!empty($filterData['username'])){
			$pointTransactions->where('members.username', '=', $filterData['username']);
		}
		
		if(!empty($filterData['type'])){
			$pointTransactions->where('point_transactions.type', '=', $filterData['type']);
		}
		
		if(!empty($filterData['reference'])){
			$pointTransactions->where('point_transactions.reference', '=', $filterData['reference']);
		}
		
		return Datatables::of($pointTransactions)
			->edit_column('date', function ($pointTransaction) {
                return $pointTransaction->date->format('d/m/Y H:i:s');
            })
			->edit_column('type', function ($pointTransaction) {
                return $pointTransaction->type_title;
            })
			->make(true);
	}
}
