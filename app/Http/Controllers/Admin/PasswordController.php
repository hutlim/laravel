<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\PasswordController as ResetPassword;
use Illuminate\Http\Request;
use App\Models\User;

class PasswordController extends ResetPassword {
	protected $guard = 'admin';
    protected $broker = 'admin';
    protected $linkRequestView = 'admin.auth.password';
    protected $resetView = 'admin.auth.reset';
    
    /**
     * Create a new authentication controller instance.
     *
     * @param  Authenticator  $auth
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->redirectTo = route('admin.home');
    }
    
    /**
     * Get the needed credentials for sending the reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getSendResetLinkEmailCredentials(Request $request)
    {
        $user = User::where('username', $request->only('username'))->first();
        return ['email' => $user ? $user->email : ''];
    }
}
