<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\TransactionType;
use App\Http\Requests\Admin\TransactionTypeCreateRequest;
use App\Http\Requests\Admin\TransactionTypeEditRequest;
use Datatables;
use Illuminate\Http\Request;
use DB;

class TransactionTypeController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{
		$statusList = TransactionType::StatusList(true);
		return view('admin.transaction_type.list', compact('statusList'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
        $statusList = TransactionType::StatusList();
		return view('admin.transaction_type.create', compact('statusList'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate(TransactionTypeCreateRequest $request)
	{
		DB::beginTransaction();
		try {
			$transactionType = new TransactionType();
			$transactionType->code = $request->code;
			$transactionType->title = $request->title;
            $transactionType->status = $request->status;
			$transactionType->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.transaction_type.create')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.transaction_type.edit', ['id' => $transactionType->id])->with('success', trans('admin_backend.data_created'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getEdit($id)
	{

		$transactionType = TransactionType::findOrFail($id);
        $statusList = TransactionType::StatusList();
		return view('admin.transaction_type.edit', compact('transactionType', 'statusList'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function postEdit(TransactionTypeEditRequest $request, $id)
	{
		DB::beginTransaction();
		try {
			$transactionType = TransactionType::findOrFail($id);
			$transactionType->title = $request->title;
            $transactionType->status = $request->status;
			$transactionType->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.transaction_type.edit', ['id' => $transactionType->id])->withErrors($e->getErrors());
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.transaction_type.edit', ['id' => $transactionType->id])->with('success', trans('admin_backend.data_saved'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		DB::beginTransaction();
		try {
			$transactionType = TransactionType::findOrFail($id);
			$transactionType->delete();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.transaction_type.list')->withErrors($e->getErrors());
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.transaction_type.list')->with('success', trans('admin_backend.data_deleted'));
	}
	
    /**
     * Batch activate the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function getBatchActivate(Request $request)
    {
        $i = 0;
        DB::beginTransaction();
        try {
            if($request->ajax()){
                if(is_array($request->ids)){
                    foreach($request->ids as $id){
                        $transactionType = TransactionType::findOrFail($id);
                        if($transactionType->status == 0){
                            $transactionType->status = 1;
                            $transactionType->save();
                            $i++;
                        }
                    }
                }
            }
        } catch(ValidationException $e)
        {
            DB::rollback();
            $i = 0;
        } catch(\Exception $e)
        {
            DB::rollback();
            throw $e;
        }
        DB::commit();
        
        $data = array('total' => $i, 'message' => trans('admin_backend.total_activated', ['total' => $i]));
        return collect($data)->toJson();
    }

    /**
     * Batch deactivate the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function getBatchDeactivate(Request $request)
    {
        $i = 0;
        DB::beginTransaction();
        try {
            if($request->ajax()){
                if(is_array($request->ids)){
                    foreach($request->ids as $id){
                        $transactionType = TransactionType::findOrFail($id);
                        if($transactionType->status == 1){
                            $transactionType->status = 0;
                            $transactionType->save();
                            $i++;
                        }
                    }
                }
            }
        } catch(ValidationException $e)
        {
            DB::rollback();
            $i = 0;
        } catch(\Exception $e)
        {
            DB::rollback();
            throw $e;
        }
        DB::commit();
        
        $data = array('total' => $i, 'message' => trans('admin_backend.total_deactivated', ['total' => $i]));
        return collect($data)->toJson();
    }
    
	/**
	 * Batch delete the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchDelete(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$transactionType = TransactionType::findOrFail($id);
						$transactionType->delete();
						$i++;
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_deleted', ['total' => $i]));
		return collect($data)->toJson();
	}

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		$transactionTypes = TransactionType::select(array(
			'transaction_types.id',
			'transaction_types.created_at',
			'transaction_types.code',
			'transaction_types.title',
            'transaction_types.status'
		));
		
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['created_from'])){
			$transactionTypes->where('transaction_types.created_at', '>=', $filterData['created_from'] . ' 00:00:00');
		}
		
		if(!empty($filterData['created_to'])){
			$transactionTypes->where('transaction_types.created_at', '<=', $filterData['created_to'] . ' 59:59:59');
		}
		
		if(!empty($filterData['code'])){
			$transactionTypes->where('transaction_types.code', '=', $filterData['code']);
		}
        
        if(!empty($filterData['status'])){
            $transactionTypes->where('transaction_types.status', '=', $filterData['status']);
        }
		
		return Datatables::of($transactionTypes)
			->edit_column('created_at', function ($transactionType) {
                return $transactionType->created_at->format('d/m/Y H:i:s');
            })
            ->edit_column('title', function ($transactionType) {
                return $transactionType->locale_title;
            })
            ->edit_column('status', function ($transactionType) {
                return $transactionType->status_title;
            })
			->add_column('actions', '<a href="{{ route(\'admin.transaction_type.edit\', [\'id\' => $id]) }}" class="btn btn-success btn-sm edit-action" data-title="@lang(\'admin_backend.edit_transaction_type\')"><span class="glyphicon glyphicon-pencil"></span> @lang(\'admin_backend.edit\')</a> <a href="{{ route(\'admin.transaction_type.delete\', [\'id\' => $id]) }}" class="btn btn-danger btn-sm delete-action"><span class="glyphicon glyphicon-trash"></span> @lang(\'admin_backend.delete\')</a>')
			->make(true);
	}
}
