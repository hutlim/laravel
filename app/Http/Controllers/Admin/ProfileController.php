<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\User;
use App\Http\Requests\Admin\ProfileEditRequest;
use DB;

class ProfileController extends AdminController
{
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param id
	 * @return Response
	 */
	public function getEdit()
	{

		$user = $this->auth->user();
		$typeList = User::TypeList();
		$statusList = User::StatusList();
		return view('admin.profile.edit', compact('user', 'typeList', 'statusList'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param id
	 * @return Response
	 */
	public function postEdit(ProfileEditRequest $request)
	{
		DB::beginTransaction();
		try {
			$user = $this->auth->user();
			$user->email = $request->email;
			$user->name = $request->name;
	
			$password = $request->password;
			$passwordConfirmation = $request->password_confirmation;
	
			if(!empty($password) && $password === $passwordConfirmation)
			{
				$user->password = bcrypt($password);
			}
			$user->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.profile')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.profile')->with('success', trans('admin_backend.data_saved'));
	}
}
