<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Sponsor;
use App\Models\Member;
use App\Models\MavroWallet;
use App\Models\PHOrder;
use App\Models\OrderTransaction;
use App\Models\GHOrder;
use App\Http\Requests\Admin\GHCreateRequest;
use App\Http\Requests\Admin\GHEditRequest;
use Datatables;
use Illuminate\Http\Request;
use DB;
use Countries;
use App;

class GHOrderController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{
		// Show the page
		$typeList = ['' => 'All'] + MavroWallet::typeList();
		$statusList = ['' => 'All'] + GHOrder::statusList();
		$countryList = ['' => 'All'] + Countries::getList(App::getLocale());
		return view('admin.gh_order.list', compact('typeList', 'statusList', 'countryList'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		$statusList = GHOrder::statusList();
		return view('admin.gh_order.create', compact('statusList'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate(GHCreateRequest $request)
	{
		DB::beginTransaction();
		try {
			$member = Member::where('username', '=', $request->username)->firstOrFail();
			$ghOrder = new GHOrder();
			$ghOrder->amount = $request->amount;
			$ghOrder->bank_name = $request->bank_name;
			$ghOrder->bank_currency = $request->bank_currency;
			$ghOrder->swift_code = $request->swift_code;
			$ghOrder->bank_country = $request->bank_country;
			$ghOrder->account_number = $request->account_number;
			$ghOrder->account_holder = $request->account_holder;
			$ghOrder->mavro_wallet_id = $request->mavro_wallet_id;
			$member->ghOrders()->save($ghOrder);
			
			if($ghOrder->outstanding > 0 && ($ghOrder->status == 'Queue' || $ghOrder->status == 'Process')){
				$ghOrder->match();
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.gh_order.create')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.gh_order.edit', ['id' => $ghOrder->id])->with('success', trans('admin_backend.data_created'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getEdit($id)
	{

		$ghOrder = GHOrder::findOrFail($id);
		$statusList = GHOrder::statusList();
		return view('admin.gh_order.edit', compact('ghOrder', 'statusList'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function postEdit(GHEditRequest $request, $id)
	{
		DB::beginTransaction();
		try {
			$ghOrder = GHOrder::findOrFail($id);
			$ghOrder->status = $request->status;
			$ghOrder->bank_name = $request->bank_name;
			$ghOrder->bank_currency = $request->bank_currency;
			$ghOrder->swift_code = $request->swift_code;
			$ghOrder->bank_country = $request->bank_country;
			$ghOrder->account_number = $request->account_number;
			$ghOrder->account_holder = $request->account_holder;
			$ghOrder->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.gh_order.edit', ['id' => $ghOrder->id])->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.gh_order.edit', ['id' => $ghOrder->id])->with('success', trans('admin_backend.data_saved'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		DB::beginTransaction();
		try {
			$ghOrder = GHOrder::findOrFail($id);
			$ghOrder->delete();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.gh_order.list')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.gh_order.list')->with('success', trans('admin_backend.data_deleted'));
	}
	
	/**
	 * Batch cancel the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchCancel(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$ghOrder = GHOrder::findOrFail($id);
						if($ghOrder->status == 'Queue'){
							$ghOrder->status = 'Cancelled';
							$ghOrder->save();
							$i++;
						}
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		$data = array('total' => $i, 'message' => trans('admin_backend.total_cancelled', ['total' => $i]));
		return collect($data)->toJson();
	}

	/**
	 * Update the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getRejected($id)
	{
		DB::beginTransaction();
		try {
			$orderTransaction = OrderTransaction::findOrFail($id);
			$orderTransaction->status = 'Rejected';
			$orderTransaction->save();
			$orderTransaction->ghOrder->match();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.gh_order.list')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.gh_order.list')->with('success', trans('admin_backend.data_rejected'));
	}
	
	/**
	 * Update the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getReceived($id)
	{
		DB::beginTransaction();
		try {
			$orderTransaction = OrderTransaction::findOrFail($id);
			$orderTransaction->status = 'Received';
			$orderTransaction->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.gh_order.list')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.gh_order.list')->with('success', trans('admin_backend.data_received'));
	}

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		$ghOrders = GHOrder::join('members', 'gh_orders.member_id', '=', 'members.id')->join('sponsors', 'sponsors.member_id', '=', 'members.id')->leftJoin('members as uplines', 'sponsors.upline_id', '=', 'uplines.id')->select(array(
			'gh_orders.id',
			'gh_orders.created_at',
			'uplines.username as sponsor_username',
			'members.username',
			'gh_orders.reference',
			'gh_orders.bank_name',
			'gh_orders.bank_country',
			'gh_orders.amount',
			'gh_orders.outstanding',
			'gh_orders.status'
		));
		
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['created_from'])){
			$ghOrders->where('created_at', '>=', $filterData['created_from'] . ' 00:00:00');
		}
		
		if(!empty($filterData['created_to'])){
			$ghOrders->where('created_at', '<=', $filterData['created_to'] . ' 59:59:59');
		}
		
		if(!empty($filterData['sponsor_username'])){
			$sponsor = Sponsor::whereHas('member', function($query) use($filterData)
			{
		    	$query->where('members.username', '=', $filterData['sponsor_username']);
			})->first();
			
			if($sponsor){
				$ghOrders->where('sponsors.left', '>=', $sponsor->left)->where('sponsors.right', '<=', $sponsor->right);
			}
			else{
				$ghOrders->where('sponsors.left', '>=', 0)->where('sponsors.right', '<=', 0);
			}
		}
		
		if(!empty($filterData['username'])){
			$ghOrders->where('members.username', '=', $filterData['username']);
		}
		
		if(!empty($filterData['bank_name'])){
			$ghOrders->where('gh_orders.bank_name', '=', $filterData['bank_name']);
		}
		
		if(!empty($filterData['bank_country'])){
			$ghOrders->where('gh_orders.bank_country', '=', $filterData['bank_country']);
		}
		
		if(!empty($filterData['reference'])){
			$ghOrders->where('gh_orders.reference', '=', $filterData['reference']);
		}
		
		if(!empty($filterData['status'])){
			$ghOrders->where('gh_orders.status', '=', $filterData['status']);
		}
		
		if(isset($filterData['amount']) && $filterData['amount'] > 0){
			$ghOrders->where('gh_orders.amount', '>=', $filterData['amount']);
		}
		
		if(isset($filterData['outstanding']) && $filterData['outstanding'] > 0){
			$ghOrders->where('gh_orders.outstanding', '>=', $filterData['outstanding']);
		}

		return Datatables::of($ghOrders)
			->edit_column('created_at', function ($ghOrder) {
                return $ghOrder->created_at->format('d/m/Y H:i:s');
            })
			->edit_column('bank_country', function ($ghOrder) {
                return $ghOrder->bank_country_title;
            })
			->edit_column('status', function ($ghOrder) {
                return $ghOrder->status_title;
            })
			->add_column('actions', '<a href="{{ route(\'admin.gh_order.edit\', [\'id\' => $id]) }}" class="btn btn-success btn-sm edit-action" data-title="@lang(\'admin_backend.edit_gh_order\')"><span class="glyphicon glyphicon-pencil"></span> @lang(\'admin_backend.edit\')</a> <a href="{{ route(\'admin.gh_order.delete\', [\'id\' => $id]) }}" class="btn btn-danger btn-sm delete-action"><span class="glyphicon glyphicon-trash"></span> @lang(\'admin_backend.delete\')</a>')
			->make(true);
	}

	public function getLoadMember(Request $request){
		$data = array();
		if($request->ajax()){
			$list = Member::where('username', 'like', sprintf('%%%s%%', $request->search))->limit(10)->get();
			foreach($list as $item){
				$data[] = $item->username;
			}
		}
		
		return collect($data)->toJson();
	}

	public function getCheckMember(Request $request){
		$data = array('result' => 'error', 'message' => 'Invalid member username');
		if($request->ajax()){
			if($member = Member::where('username', '=', $request->username)->first()){
				$data = array(
					'result' => 'success',
					'data' => array(
						'id' => $member->id,
						'name' => $member->name,
						'bank_country' => $member->bank_country,
						'bank_name' => $member->bank_name,
						'bank_currency' => $member->bank_currency,
						'swift_code' => $member->swift_code,
						'account_number' => $member->account_number,
						'account_holder' => $member->account_holder,
						'mavroWallets' => $member->mavroWallets()->where('status', 'Started')->where('balance', '>', 0)->getResults()
					),
					'message' => ''
				);
				
				if(!sizeof($data['data']['mavroWallets'])){
					$data['message'] = trans('admin_backend.not_own_mavro_wallet');
				}
			}
		}
		
		return collect($data)->toJson();
	}
}
