<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\User;
use App\Http\Requests\Admin\UserCreateRequest;
use App\Http\Requests\Admin\UserEditRequest;
use Datatables;
use Illuminate\Http\Request;
use DB;

class UserController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{
		$typeList = User::TypeList(true);
		$statusList = User::StatusList(true);
		return view('admin.user.list', compact('typeList', 'statusList'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		$typeList = User::TypeList();
		$statusList = User::StatusList();
		return view('admin.user.create', compact('typeList', 'statusList'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate(UserCreateRequest $request)
	{
		DB::beginTransaction();
		try {
			$user = new User();
			$user->username = $request->username;
			$user->email = $request->email;
			$user->name = $request->name;
			$user->password = bcrypt($request->password);
			$user->type = $request->type;
			$user->status = $request->status;
			$user->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.user.create')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.user.edit', ['id' => $user->id])->with('success', trans('admin_backend.data_created'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param id
	 * @return Response
	 */
	public function getEdit($id)
	{

		$user = User::findOrFail($id);
		$typeList = User::TypeList();
		$statusList = User::StatusList();
		return view('admin.user.edit', compact('user', 'typeList', 'statusList'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param id
	 * @return Response
	 */
	public function postEdit(UserEditRequest $request, $id)
	{
		DB::beginTransaction();
		try {
			$user = User::findOrFail($id);
			$user->username = $request->username;
			$user->email = $request->email;
			$user->name = $request->name;
			$user->type = $request->type;
			$user->status = $request->status;
	
			$password = $request->password;
			$passwordConfirmation = $request->password_confirmation;
	
			if(!empty($password) && $password === $passwordConfirmation)
			{
				$user->password = bcrypt($password);
			}
			$user->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.user.edit', ['id' => $user->id])->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.user.edit', ['id' => $user->id])->with('success', trans('admin_backend.data_saved'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param id
	 * @return Response
	 */
	public function getDelete($id)
	{
		DB::beginTransaction();
		try {
			$user = User::findOrFail($id);
			$user->delete();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.user.list')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		return redirect()->route('admin.user.list')->with('success', trans('admin_backend.data_deleted'));
	}
	
	/**
	 * Batch activate the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchActivate(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$user = User::findOrFail($id);
						if($user->status == 0){
							$user->status = 1;
							$user->save();
							$i++;
						}
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_activated', ['total' => $i]));
		return collect($data)->toJson();
	}

	/**
	 * Batch deactivate the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchDeactivate(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						if($id == 1) continue;
						$user = User::findOrFail($id);
						if($user->status == 1){
							$user->status = 0;
							$user->save();
							$i++;
						}
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_deactivated', ['total' => $i]));
		return collect($data)->toJson();
	}

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		$users = User::select(array(
			'users.id',
			'users.username',
			'users.email',
			'users.name',
			'users.type',
			'users.status',
			'users.created_at',
			'users.updated_at'
		));
		
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['username'])){
			$users->where('users.username', '=', $filterData['username']);
		}
		
		if(!empty($filterData['email'])){
			$users->where('users.email', '=', $filterData['email']);
		}
		
		if(!empty($filterData['name'])){
			$users->where('users.name', '=', $filterData['name']);
		}
		
		if(!empty($filterData['type'])){
			$users->where('users.type', '=', $filterData['type']);
		}
		
		if(!empty($filterData['status'])){
			$users->where('users.status', '=', $filterData['status']);
		}

		return Datatables::of($users)
			->edit_column('type', function ($user) {
                return $user->type_title;
            })
			->edit_column('status', function ($user) {
                return $user->status_title;
            })
			->edit_column('created_at', function ($user) {
                return $user->created_at->format('d/m/Y h:i:s A');
            })
			->edit_column('updated_at', function ($user) {
                return $user->updated_at->format('d/m/Y h:i:s A');
            })
			->add_column('actions', '@if ($id != "1")<a href="{{ route(\'admin.user.edit\', [\'id\' => $id]) }}" class="btn btn-success btn-sm edit-action" data-title="@lang(\'admin_backend.edit_user\')"><span class="glyphicon glyphicon-pencil"></span> @lang(\'admin_backend.edit\')</a> <a href="{{ route(\'admin.user.delete\', [\'id\' => $id]) }}" class="btn btn-danger btn-sm delete-action"><span class="glyphicon glyphicon-trash"></span> @lang(\'admin_backend.delete\')</a>@endif')
			->make(true);
	}

}
