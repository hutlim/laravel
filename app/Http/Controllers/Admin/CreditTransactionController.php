<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Member;
use App\Models\CreditTransaction;
use Datatables;
use Illuminate\Http\Request;
use DB;

class CreditTransactionController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{
		// Show the page
		$typeList = CreditTransaction::TypeList(true);
		return view('admin.credit_transaction.list', compact('typeList'));
	}

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		$creditTransactions = CreditTransaction::join('members', 'credit_transactions.member_id', '=', 'members.id')->select(array(
			'credit_transactions.id',
			'credit_transactions.date',
			'members.username',
			'credit_transactions.type',
			'credit_transactions.reference',
			'credit_transactions.description',
			'credit_transactions.credit',
			'credit_transactions.debit'
		));
		
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['date_from'])){
			$creditTransactions->where('credit_transactions.date', '>=', $filterData['date_from'] . ' 00:00:00');
		}
		
		if(!empty($filterData['date_to'])){
			$creditTransactions->where('credit_transactions.date', '<=', $filterData['date_to'] . ' 59:59:59');
		}
		
		if(!empty($filterData['username'])){
			$creditTransactions->where('members.username', '=', $filterData['username']);
		}
		
		if(!empty($filterData['type'])){
			$creditTransactions->where('credit_transactions.type', '=', $filterData['type']);
		}
		
		if(!empty($filterData['reference'])){
			$creditTransactions->where('credit_transactions.reference', '=', $filterData['reference']);
		}
		
		return Datatables::of($creditTransactions)
			->edit_column('date', function ($creditTransaction) {
                return $creditTransaction->date->format('d/m/Y H:i:s');
            })
			->edit_column('type', function ($creditTransaction) {
                return $creditTransaction->type_title;
            })
			->make(true);
	}
}
