<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Member;
use App\Models\Placement;
use App\Models\Sponsor;
use App\Http\Requests\Admin\PlacementEditRequest;
use Datatables;
use Illuminate\Http\Request;
use DB;
use Countries;
use App;
use Response;

class PlacementController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{
		$defaultPlacement = Sponsor::defaultSponsor();
		$positionList = Placement::PositionList(true);
		$typeList = Member::TypeList(true);
		$statusList = Member::StatusList(true);
		$countryList = ['' => 'All'] + Countries::getList(App::getLocale());
		return view('admin.placement.list', compact('defaultPlacement', 'positionList', 'typeList', 'statusList', 'countryList'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param id
	 * @return Response
	 */
	public function getEdit($id)
	{

		$member = Member::findOrFail($id);
		$positionList = Placement::PositionList();
		return view('admin.placement.edit', compact('member', 'positionList'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param id
	 * @return Response
	 */
	public function postEdit(PlacementEditRequest $request, $id)
	{
		DB::beginTransaction();
		try {
			$member = Member::findOrFail($id);
			$member->updateSponsor($request->sponsor_username);
			$member->updatePlacement($request->placement_username, $request->placement_position);
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.placement.edit', ['id' => $member->id])->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.placement.edit', ['id' => $member->id])->with('success', trans('admin_backend.data_saved'));
	}

	/*
	 * Display a tree of the resource.
	 *
	 * @return Response
	 */
	public function getTree()
	{
		$root = $this->Root();
		return view('admin.placement.tree', compact('root'));
	}
	
	public function getProcessTree(Request $request){
		if($request->operation && method_exists($this, $request->operation)) {
			$contents = $this->{$request->operation}($request->id);
			$response = Response::make($contents, 200); 
    		$response->header('Content-Type', 'application/json; charset=utf-8');
			$response->header('Pragma', 'no-cache');
			$response->header('Cache-Control', 'no-cache, must-revalidate');
    		return $response;
        }
		
		abort(404);
	}
	
	protected function Root(){
		return Placement::whereHas('member', function($query)
		{
	    	$query->where('members.username', '=', Sponsor::defaultSponsor());
		})->firstOrFail();
	}
	
	protected function LoadTree($username){
		$placement = Placement::where('placements.left', '>=', $this->Root()->left)->where('placements.right', '<=', $this->Root()->right)->whereHas('member', function($query) use($username)
		{
	    	$query->where('members.username', '=', $username);
		})->first();
		
		if($placement){
			$show_level = Placement::showLevel();
			$level = (int)$placement->level;
	        $left = (int)$placement->left;
	        $right = (int)$placement->right;
			$upline_id = (int)$placement->upline_id;
			$position = (int)$placement->seqno;
	        $max_level = $placement->level + $show_level;
	        
			$items = Placement::where('left', '>=', $left)->where('right', '<=', $right)->where('level', '<=', $max_level)->orderBy('level')->orderBy('seqno')->orderBy('id')->get();
	
	        $downlines = array();
	        foreach($items as $key => $item){
	            if($item->member){
	                $data = array();
	                $data['level'] = (int)($item->level - $level);
	                $data['upline_id'] = (int)$item->upline_id;
	                $data['position'] = (int)$item->seqno;
	                if(isset($downlines[$data['level']][$data['upline_id']][$data['position']])){
	                    continue;
	                }
	                
	                $downlines[$data['level']][$data['upline_id']][$data['position']] = $item;
	            }
	        }
	
			$current_column = 1;
	        $child_limit = Placement::maxDownline();
			$max_child = pow($child_limit, $show_level);
	
			$node_data = array();
	        $post_upline = array();
			for ($i = 0; $i <= $show_level; $i++) {
	            $node_key = 0;
				$colspan = $max_child / $current_column;
				$width = (1 / $current_column * 100) . '%';
	            $upline_ids = array();
	            $data_by_level = isset($downlines[$i]) ? $downlines[$i] : array();
				$current_upline_username = '';
	            $current_upline_id = 0;
				$current_upline_position = 0;
	            for ($j = 1; $j <= $current_column; $j++) {
	                $data = array();
	                if($i == 0){
	                    $data = isset($data_by_level[$upline_id][$position]) ? $data_by_level[$upline_id][$position] : array();
	                }
	                else{
	                	$current_upline_position += 1;
						
	                    if(isset($post_upline[$node_key]) && $current_upline_id = (int)$post_upline[$node_key]['id']){
	                    	$current_upline_username = $post_upline[$node_key]['username'];
	                        if(isset($data_by_level[$current_upline_id][$current_upline_position])){
	                            $data = $data_by_level[$current_upline_id][$current_upline_position];
	                        }
	                    }
	                    
	                    if($current_upline_position >= $child_limit){
	                        $node_key += 1;
	                    }
	                }
	                if(sizeof($data)){
	                	$node_data[$i][$j] = array(
							'next' => '',
							'data' => $data,
							'colspan' => $colspan,
							'width' => $width,
							'last' => $i == $show_level,
							'register' => '',
							'upline' => '',
							'position' => '',
							'icon' => 'member_icon.png'
						);
	
						if($i == $show_level){
							$node_data[$i][$j]['next'] = $data->member->username;
						}
	                        
	                    $upline_ids[] = array(
							'id' => $data->id,
							'username' => $data->username
						);
	                }
	                else{
	                	if($current_upline_id){
		                	$node_data[$i][$j] = array(
								'next' => '',
								'data' => new Placement(),
								'colspan' => $colspan,
								'width' => $width,
								'last' => $i == $show_level,
								'upline' => $current_upline_id,
								'position' => $current_upline_position,
								'icon' => 'register_icon.png'
							);
						}
						else{
							$node_data[$i][$j] = array(
								'next' => '',
								'data' => new Placement(),
								'colspan' => $colspan,
								'width' => $width,
								'last' => $i == $show_level,
								'register' => '',
								'upline' => '',
								'position' => '',
								'icon' => 'empty_icon.png'
							);
						}
						
	                    $upline_ids[] = array(
							'id' => 0,
							'username' => ''
						);
	                }
	
					if(!($j % $child_limit)){
	                	$current_upline_position = 0;
					}
	            }
	            $current_column *= $child_limit;
	            $post_upline = $upline_ids;
	        }

			$top = '';
			if(strtoupper($username) != strtoupper($this->Root()->member->username)){
				$top = array(
					'next' => '',
					'data' => $this->Root(),
					'colspan' => $max_child,
					'width' => '100%',
					'last' => false,
					'register' => '',
					'upline' => '',
					'position' => '',
					'icon' => 'member_icon.png'
				);
			}

			$previous = strtoupper($username) != strtoupper($this->Root()->member->username) ? $placement->upline->member->username : '';

			$data = array(
				'result' => 'success',
	            'contents' => view('admin.placement.nodes', compact('node_data', 'top', 'previous'))->render()
	        );
		}
		else{
			$data = array(
				'result' => 'error',
	            'msg' => 'Invalid member'
	        );
		}

        return collect($data)->toJson();
    }
    
    function MemberDetails($id){
    	$placement = Placement::where('placements.left', '>=', $this->Root()->left)->where('placements.right', '<=', $this->Root()->right)->find($id);
		
		if($placement){
			$data = array(
				'result' => 'success',
				'username' => $placement->member->username,
				'contents' => view('admin.placement.member_details', compact('placement'))->render()
			);
		}
		else{
			$data = array(
				'result' => 'error',
	            'msg' => trans('admin_backend.invalid_member_username')
	        );
		}
		
		return collect($data)->toJson();
    }
	
	public function getProcessRegister(Request $request){
		if($request->operation && method_exists($this, $request->operation)) {
			$contents = $this->{$request->operation}($request->id);
			$response = Response::make($contents, 200); 
    		$response->header('Content-Type', 'application/json; charset=utf-8');
			$response->header('Pragma', 'no-cache');
			$response->header('Cache-Control', 'no-cache, must-revalidate');
    		return $response;
        }
		
		abort(404);
	}

	/**
	 * Batch activate the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchActivate(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$member = Member::findOrFail($id);
						if($member->status == 0){
							$member->status = 1;
							$member->save();
							$i++;
						}
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_activated', ['total' => $i]));
		return collect($data)->toJson();
	}

	/**
	 * Batch deactivate the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchDeactivate(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$member = Member::findOrFail($id);
						if($member->status == 1){
							$member->status = 0;
							$member->save();
							$i++;
						}
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_deactivated', ['total' => $i]));
		return collect($data)->toJson();
	}

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['placement_username'])){
			$placement = Placement::whereHas('member', function($query) use($filterData)
			{
		    	$query->where('members.username', '=', $filterData['placement_username']);
			})->first();
			
			if(!empty($filterData['placement_position']) && $placement){
				$placement = $placement->downlines()->where('seqno', $filterData['placement_position'])->first();
			}
		}
		else{
			$placement = Placement::whereHas('member', function($query) use($position)
			{
		    	$query->where('members.username', '=', Sponsor::defaultSponsor());
			})->first();
			
			if(!empty($filterData['placement_position']) && $placement){
				$placement = $placement->downlines()->where('seqno', $filterData['placement_position'])->first();
			}
		}
		
		$members = Member::join('placements', 'placements.member_id', '=', 'members.id')->leftJoin('members as uplines', 'placements.upline_id', '=', 'uplines.id')->join('sponsors', 'sponsors.member_id', '=', 'members.id')
		->leftJoin('placements as left_placement', function($join)
 		{
   			$join->on('left_placement.upline_id', '=', 'placements.id');
			$join->where('left_placement.seqno', '=', 1);
 		})
		->leftJoin('placements as right_placement', function($join)
 		{
   			$join->on('right_placement.upline_id', '=', 'placements.id');
			$join->where('right_placement.seqno', '=', 2);
 		});
		
		if($placement){
			$members->select(array(
				'members.id',
				\DB::raw(sprintf('placements.level - %s as level', $placement->level)),
				'uplines.username as placement_username',
				'members.username',
				'members.type',
				'members.status',
				'members.joined',
				'members.register_country',
				\DB::raw('(SELECT count(sponsor_downlines.member_id) FROM sponsors as sponsor_downlines WHERE sponsor_downlines.upline_id = sponsors.id) as direct_downlines'),
				\DB::raw('(SELECT count(left_placement_downlines.member_id) FROM placements as left_placement_downlines WHERE left_placement_downlines.left >= left_placement.left AND left_placement_downlines.left <= left_placement.right) as left_placements'),
				\DB::raw('(SELECT count(right_placement_downlines.member_id) FROM placements as right_placement_downlines WHERE right_placement_downlines.left >= right_placement.left AND right_placement_downlines.left <= right_placement.right) as right_placements')
			));
			
			$members->where('placements.left', '>=', $placement->left)->where('placements.right', '<=', $placement->right);
		}
		else{
			$members->select(array(
				'members.id',
				'placements.level',
				'uplines.username as placement_username',
				'members.username',
				'members.type',
				'members.status',
				'members.joined',
				'members.register_country',
				\DB::raw('(SELECT count(sponsor_downlines.member_id) FROM sponsors as sponsor_downlines WHERE sponsor_downlines.upline_id = sponsors.id) as direct_downlines'),
				\DB::raw('(SELECT count(left_placement_downlines.member_id) FROM placements as left_placement_downlines WHERE left_placement_downlines.left >= left_placement.left AND left_placement_downlines.left <= left_placement.right) as left_placements'),
				\DB::raw('(SELECT count(right_placement_downlines.member_id) FROM placements as right_placement_downlines WHERE right_placement_downlines.left >= right_placement.left AND right_placement_downlines.left <= right_placement.right) as right_placements')
			));
			
			$members->where('placements.left', '>=', 0)->where('placements.right', '<=', 0);
		}
		
		if(isset($filterData['level_from']) && $filterData['level_from'] >= 0){
			$members->where('placements.level', '>=', $filterData['level_from']);
		}
		
		if(isset($filterData['level_to']) && $filterData['level_to'] > 0){
			$members->where('placements.level', '<=', $filterData['level_to']);
		}
		
		if(isset($filterData['direct_downlines_from']) && $filterData['direct_downlines_from'] >= 0){
			$members->having('direct_downlines', '>=', $filterData['direct_downlines_from']);
		}
		
		if(isset($filterData['direct_downlines_to']) && $filterData['direct_downlines_to'] > 0){
			$members->having('direct_downlines', '<=', $filterData['direct_downlines_to']);
		}

		if(isset($filterData['left_placements_from']) && $filterData['left_placements_from'] >= 0){
			$members->having('left_placements', '>=', $filterData['left_placements_from']);
		}
		
		if(isset($filterData['left_placements_to']) && $filterData['left_placements_to'] > 0){
			$members->having('left_placements', '<=', $filterData['left_placements_to']);
		}
		
		if(isset($filterData['right_placements_from']) && $filterData['right_placements_from'] >= 0){
			$members->having('right_placements', '>=', $filterData['right_placements_from']);
		}
		
		if(isset($filterData['right_placements_to']) && $filterData['right_placements_to'] > 0){
			$members->having('right_placements', '<=', $filterData['right_placements_to']);
		}
		
		if(!empty($filterData['type'])){
			$members->where('members.type', '=', $filterData['type']);
		}
		
		if(!empty($filterData['status'])){
			$members->where('members.status', '=', $filterData['status']);
		}

		if(!empty($filterData['joined_from'])){
			$members->where('members.joined', '>=', $filterData['joined_from']);
		}
		
		if(!empty($filterData['joined_to'])){
			$members->where('members.joined', '<=', $filterData['joined_to']);
		}

		if(!empty($filterData['register_country'])){
			$members->where('members.register_country', '=', $filterData['register_country']);
		}

		return Datatables::of($members)
			->edit_column('type', function ($member) {
                return $member->type_title;
            })
			->edit_column('status', function ($member) {
                return $member->status_title;
            })
			->edit_column('joined', function ($member) {
                return $member->joined->format('d/m/Y');
            })
			->edit_column('register_country', function ($member) {
                return $member->register_country_title;
            })
			->add_column('actions', '@if ($id != "1")<a href="{{ route(\'admin.placement.edit\', [\'id\' => $id]) }}" class="btn btn-success btn-sm edit-action" data-title="@lang(\'admin_backend.edit_genealogy\')"><span class="glyphicon glyphicon-pencil"></span> @lang(\'admin_backend.edit\')</a>@endif')
			->make(true);
	}

}
