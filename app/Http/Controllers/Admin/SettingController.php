<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Setting;
use App\Http\Requests\Admin\SettingCreateRequest;
use App\Http\Requests\Admin\SettingEditRequest;
use Datatables;
use Illuminate\Http\Request;
use DB;

class SettingController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{
		return view('admin.setting.list');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		return view('admin.setting.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate(SettingCreateRequest $request)
	{
		DB::beginTransaction();
		try {
			$setting = new Setting();
			$setting->param = $request->param;
			$setting->value = $request->value;
            $setting->remark = $request->remark;
			$setting->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.setting.create')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.setting.edit', ['id' => $setting->id])->with('success', trans('admin_backend.data_created'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getEdit($id)
	{

		$setting = Setting::findOrFail($id);
		return view('admin.setting.edit', compact('setting'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function postEdit(Request $request, $id)
	{
		DB::beginTransaction();
		try {
			$setting = Setting::findOrFail($id);
            $setting->value = $request->value;
            $setting->remark = $request->remark;
			$setting->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.setting.edit', ['id' => $setting->id])->withErrors($e->getErrors());
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.setting.edit', ['id' => $setting->id])->with('success', trans('admin_backend.data_saved'));
	}
    
        /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function getDelete($id)
    {
        DB::beginTransaction();
        try {
            $setting = Setting::findOrFail($id);
            $setting->delete();
        } catch(ValidationException $e)
        {
            DB::rollback();
            return redirect()->route('admin.setting.list')->withErrors($e->getErrors());
        } catch(\Exception $e)
        {
            DB::rollback();
            throw $e;
        }
        
        DB::commit();
        return redirect()->route('admin.setting.list')->with('success', trans('admin_backend.data_deleted'));
    }
    
    /**
     * Batch delete the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function getBatchDelete(Request $request)
    {
        $i = 0;
        DB::beginTransaction();
        try {
            if($request->ajax()){
                if(is_array($request->ids)){
                    foreach($request->ids as $id){
                        $setting = Setting::findOrFail($id);
                        $setting->delete();
                        $i++;
                    }
                }
            }
        } catch(ValidationException $e)
        {
            DB::rollback();
            $i = 0;
        } catch(\Exception $e)
        {
            DB::rollback();
            throw $e;
        }
        DB::commit();
        
        $data = array('total' => $i, 'message' => trans('admin_backend.total_deleted', ['total' => $i]));
        return collect($data)->toJson();
    }

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		$settings = Setting::select(array(
			'settings.id',
			'settings.created_at',
			'settings.param',
			'settings.value',
            'settings.remark'
		));
		
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['created_from'])){
			$settings->where('settings.created_at', '>=', $filterData['created_from'] . ' 00:00:00');
		}
		
		if(!empty($filterData['created_to'])){
			$settings->where('settings.created_at', '<=', $filterData['created_to'] . ' 59:59:59');
		}
		
		if(!empty($filterData['param'])){
			$settings->where('settings.param', '=', $filterData['param']);
		}
		
		return Datatables::of($settings)
			->edit_column('created_at', function ($setting) {
                return $setting->created_at->format('d/m/Y H:i:s');
            })
			->add_column('actions', '<a href="{{ route(\'admin.setting.edit\', [\'id\' => $id]) }}" class="btn btn-success btn-sm edit-action" data-title="@lang(\'admin_backend.edit_setting\')"><span class="glyphicon glyphicon-pencil"></span> @lang(\'admin_backend.edit\')</a> <a href="{{ route(\'setting.delete\', [\'id\' => $id]) }}" class="btn btn-danger btn-sm delete-action"><span class="glyphicon glyphicon-trash"></span> @lang(\'admin_backend.delete\')</a>')
			->make(true);
	}
}
