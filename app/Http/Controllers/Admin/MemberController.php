<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Member;
use App\Models\Sponsor;
use App\Models\Placement;
use Auth;
use App\Http\Requests\Admin\MemberCreateRequest;
use App\Http\Requests\Admin\MemberEditRequest;
use Datatables;
use Illuminate\Http\Request;
use DB;
use Countries;
use App;

class MemberController extends AdminController
{

	/*
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList()
	{
		$typeList = Member::TypeList(true);
		$statusList = Member::StatusList(true);
		$countryList = ['' => ucfirst(trans('admin_backend.all'))] + Countries::getList(App::getLocale());
		return view('admin.member.list', compact('typeList', 'statusList', 'countryList'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		$typeList = Member::TypeList();
		$statusList = Member::StatusList();
		$positionList = ['' => 'Auto Weak Leg'] + Placement::positionList();
		return view('admin.member.create', compact('typeList', 'statusList', 'positionList'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate(MemberCreateRequest $request)
	{
		DB::beginTransaction();
		try {
			$member = new Member();
			$member->username = $request->username;
			$member->email = $request->email;
			$member->name = $request->name;
			$member->mobile = $request->mobile;
			$member->password = bcrypt($request->password);
			$member->security_password = bcrypt($request->security_password);
			$member->type = $request->type;
			$member->status = $request->status;
			$member->joined = $request->joined;
			$member->register_country = $request->register_country;
			
			$member->address = $request->address;
			$member->city = $request->city;
			$member->region = $request->region;
			$member->postal = $request->postal;
			$member->country = $request->country;
			
			$member->bank_name = $request->bank_name;
			$member->bank_currency = $request->bank_currency;
			$member->swift_code = $request->swift_code;
			$member->bank_country = $request->bank_country;
			$member->account_number = $request->account_number;
			$member->account_holder = $request->account_holder;
			
			$member->save();
			
			$member->updateSponsor($request->sponsor_username);
			$member->updatePlacement($request->placement_username, $request->placement_position);
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.member.create')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.member.edit', ['id' => $member->id])->with('success', trans('admin_backend.data_created'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param id
	 * @return Response
	 */
	public function getEdit($id)
	{

		$member = Member::findOrFail($id);
		$typeList = Member::TypeList();
		$statusList = Member::StatusList();
		$positionList = Placement::positionList();
		return view('admin.member.edit', compact('member', 'typeList', 'statusList', 'positionList'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param id
	 * @return Response
	 */
	public function postEdit(MemberEditRequest $request, $id)
	{
		DB::beginTransaction();
		try {
			$member = Member::findOrFail($id);
			
			$member->username = $request->username;
			$member->email = $request->email;
			$member->name = $request->name;
			$member->mobile = $request->mobile;
			$member->type = $request->type;
			$member->status = $request->status;
			$member->joined = $request->joined;
			$member->register_country = $request->register_country;
			
			$password = $request->password;
			$passwordConfirmation = $request->password_confirmation;
	
			if(!empty($password) && $password === $passwordConfirmation)
			{
				$member->password = bcrypt($password);
			}
			
			$securityPassword = $request->security_password;
			$securityPasswordConfirmation = $request->security_password_confirmation;
	
			if(!empty($securityPassword) && $securityPassword === $securityPasswordConfirmation)
			{
				$member->security_password = bcrypt($securityPassword);
			}
			
			$member->address = $request->address;
			$member->city = $request->city;
			$member->region = $request->region;
			$member->postal = $request->postal;
			$member->country = $request->country;
			
			$member->bank_name = $request->bank_name;
			$member->bank_currency = $request->bank_currency;
			$member->swift_code = $request->swift_code;
			$member->bank_country = $request->bank_country;
			$member->account_number = $request->account_number;
			$member->account_holder = $request->account_holder;

			$member->save();
			
			$member->updateSponsor($request->sponsor_username);
			$member->updatePlacement($request->placement_username, $request->placement_position);
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.member.edit', ['id' => $member->id])->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.member.edit', ['id' => $member->id])->with('success', trans('admin_backend.data_saved'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param id
	 * @return Response
	 */
	public function getDelete($id)
	{
		DB::beginTransaction();
		try {
			$member = Member::findOrFail($id);
			$member->delete();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('admin.member.list')->withErrors($e->getErrors());
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('admin.member.list')->with('success', trans('admin_backend.data_deleted'));
	}
	
	/**
	 * Login the specified resource from storage.
	 *
	 * @param id
	 * @return Response
	 */
	public function getLogin($id)
	{
		Auth::guard('member')->loginUsingId($id);
		return redirect()->intended('member');
	}
	
	/**
	 * Batch activate the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchActivate(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$member = Member::findOrFail($id);
						if($member->status == 0){
							$member->status = 1;
							$member->save();
							$i++;
						}
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_activated', ['total' => $i]));
		return collect($data)->toJson();
	}

	/**
	 * Batch deactivate the specified resource from storage.
	 *
	 * @param $id
	 * @return Response
	 */
	public function getBatchDeactivate(Request $request)
	{
		$i = 0;
		DB::beginTransaction();
		try {
			if($request->ajax()){
				if(is_array($request->ids)){
					foreach($request->ids as $id){
						$member = Member::findOrFail($id);
						if($member->status == 1){
							$member->status = 0;
							$member->save();
							$i++;
						}
					}
				}
			}
		} catch(ValidationException $e)
		{
		    DB::rollback();
			$i = 0;
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		DB::commit();
		
		$data = array('total' => $i, 'message' => trans('admin_backend.total_deactivated', ['total' => $i]));
		return collect($data)->toJson();
	}

	/**
	 * Show a list of all the specified resource formatted for Datatables.
	 *
	 * @return Datatables JSON
	 */
	public function getData(Request $request)
	{
		$members = Member::join('sponsors', 'sponsors.member_id', '=', 'members.id')->leftJoin('members as uplines', 'sponsors.upline_id', '=', 'uplines.id')->select(array(
			'members.id',
			'members.username',
			'members.email',
			'members.name',
			'members.mobile',
			'members.type',
			'members.status',
			'members.joined',
			'members.register_country',
			'uplines.username as sponsor_username'
		));
		
		parse_str($request->filter, $filterData);
		
		if(!empty($filterData['sponsor_username'])){
			$sponsor = Sponsor::whereHas('member', function($query) use($filterData)
			{
		    	$query->where('members.username', '=', $filterData['sponsor_username']);
			})->first();
			
			if($sponsor){
				$members->where('sponsors.left', '>=', $sponsor->left)->where('sponsors.right', '<=', $sponsor->right);
			}
			else{
				$members->where('sponsors.left', '>=', 0)->where('sponsors.right', '<=', 0);
			}
		}
		
		if(!empty($filterData['username'])){
			$members->where('members.username', '=', $filterData['username']);
		}
		
		if(!empty($filterData['email'])){
			$members->where('members.email', '=', $filterData['email']);
		}
		
		if(!empty($filterData['name'])){
			$members->where('members.name', 'like', '%' . $filterData['name'] . '%');
		}
		
		if(!empty($filterData['type'])){
			$members->where('members.type', '=', $filterData['type']);
		}
		
		if(!empty($filterData['status'])){
			$members->where('members.status', '=', $filterData['status']);
		}

		if(!empty($filterData['joined_from'])){
			$members->where('members.joined', '>=', $filterData['joined_from']);
		}
		
		if(!empty($filterData['joined_to'])){
			$members->where('members.joined', '<=', $filterData['joined_to']);
		}

		if(!empty($filterData['register_country'])){
			$members->where('members.register_country', '=', $filterData['register_country']);
		}

		return Datatables::of($members)
			->edit_column('type', function ($member) {
                return $member->type_title;
            })
			->edit_column('status', function ($member) {
                return $member->status_title;
            })
			->edit_column('joined', function ($member) {
                return $member->joined->format('d/m/Y');
            })
			->edit_column('register_country', function ($member) {
                return $member->register_country_title;
            })
			->add_column('actions', '@if ($sponsor_username != "")<a href="{{ route(\'admin.member.edit\', [\'id\' => $id]) }}" class="btn btn-success btn-sm edit-action" data-title="@lang(\'admin_backend.edit_member\')"><span class="glyphicon glyphicon-pencil"></span> @lang(\'admin_backend.edit\')</a> <a href="{{ route(\'admin.member.delete\', [\'id\' => $id]) }}" class="btn btn-danger btn-sm delete-action"><span class="glyphicon glyphicon-trash"></span> @lang(\'admin_backend.delete\')</a> @endif<a href="{{ route(\'admin.member.login\', [\'id\' => $id]) }}" class="btn btn-warning btn-sm" target="_blank"><span class="glyphicon glyphicon-log-in"></span> @lang(\'admin_backend.login\')</a>')
			->make(true);
	}

	public function getLoadSponsor(Request $request){
		$data = array();
		if($request->ajax()){
			$list = Member::where('username', 'like', sprintf('%%%s%%', $request->search))
			->whereHas('sponsor', function($query)
			{
		    	$query->where('sponsors.id', '>', 0);
			})->limit(10)->get();
			
			foreach($list as $item){
				$data[] = $item->username;
			}
		}
		
		return collect($data)->toJson();
	}

	public function getCheckSponsor(Request $request){
		$data = array('result' => 'error', 'message' => trans('admin_backend.invalid_sponsor_username'));
		if($request->ajax()){
			$sponsor = Sponsor::whereHas('member', function($query) use($request)
			{
		    	$query->where('members.username', '=', $request->username);
			})->where('id', '!=', (int)$request->id)->first();
			
			if($sponsor){
				$data = array(
					'result' => 'success',
					'data' => array(
						'id' => $sponsor->member->id,
						'name' => $sponsor->member->name
					),
					'message' => trans('admin_backend.sponsor_username_valid')
				);
			}
		}
		
		return collect($data)->toJson();
	}
	
	public function getLoadPlacement(Request $request){
		$data = array();
		if($request->ajax()){
			$placement = Placement::whereHas('member', function($query) use($request)
			{
		    	$query->where('members.username', '=', $request->sponsor);
			})->first();
			
			if($placement){
				$list = Member::where('username', 'like', sprintf('%%%s%%', $request->search))
				->whereHas('placement', function($query) use($placement)
				{
			    	$query->where('left', '>=', $placement->left)->where('right', '<=', $placement->right);
				})->limit(10)->get();
			}
			else{
				$list = Member::where('username', 'like', sprintf('%%%s%%', $request->search))
				->whereHas('placement', function($query) use($placement)
				{
			    	$query->where('placements.id', '>', 0);
				})->limit(10)->get();
			}
			
			foreach($list as $item){
				$data[] = $item->username;
			}
		}
		
		return collect($data)->toJson();
	}
	
	public function getCheckPlacement(Request $request){
		$data = array('result' => 'error', 'message' => trans('admin_backend.invalid_placement_username'));
		if($request->ajax()){
			$sponsor = Placement::whereHas('member', function($query) use($request)
			{
		    	$query->where('members.username', '=', $request->sponsor);
			})->first();
			
			if($sponsor){
				$placement = Placement::whereHas('member', function($query) use($request)
				{
			    	$query->where('members.username', '=', $request->username);
				})->where('id', '!=', (int)$request->id)->first();
				
				if($placement){
					if($placement->left >= $sponsor->left && $placement->right <= $sponsor->right){
						$position = Placement::where('upline_id', '=', $sponsor->id)->where('seqno', '=', $request->position)->where('id', '!=', (int)$request->id)->count();
						
						if(!$position){
							$data = array(
								'result' => 'success',
								'data' => array(
									'id' => $placement->member->id,
									'name' => $placement->member->name
								),
								'message' => trans('admin_backend.placement_username_valid')
							);
						}
						else{
							$data = array('result' => 'error', 'message' => trans('admin_backend.invalid_placement_position'));
						}
					}
				}
			}
			else{
				$data = array('result' => 'error', 'message' => trans('admin_backend.sponsor_placement_not_match'));
			}
		}
		
		return collect($data)->toJson();
	}
}
