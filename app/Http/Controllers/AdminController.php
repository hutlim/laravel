<?php namespace App\Http\Controllers;

use Auth;

class AdminController extends Controller {
    /**
     * Initializer.
     *
     * @return \AdminController
     */
    public function __construct()
    {
    	$this->auth = Auth::guard('admin');
		$this->middleware('auth:admin');
    }
}
