<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PageController extends Controller {

	public function welcome()
	{
		return view('page.welcome');
	}

	public function about()
	{
		return view('page.about');
	}

	public function contact()
	{
		return view('page.contact');
	}

}
