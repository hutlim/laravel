<?php namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\PasswordController as ResetPassword;
use Illuminate\Http\Request;
use App\Models\Member;

class PasswordController extends ResetPassword {
	protected $guard = 'member';
    protected $broker = 'member';
    protected $linkRequestView = 'member.auth.password';
    protected $resetView = 'member.auth.reset';
	
	/**
     * Create a new authentication controller instance.
     *
     * @param  Authenticator  $auth
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->redirectTo = route('member.home');
    }

	/**
     * Get the needed credentials for sending the reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getSendResetLinkEmailCredentials(Request $request)
    {
        $member = Member::where('username', $request->only('username'))->first();
        return ['email' => $member ? $member->email : ''];
    }
}
