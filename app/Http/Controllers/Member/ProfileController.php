<?php
namespace App\Http\Controllers\Member;

use App\Http\Controllers\MemberController;
use App\Models\Member;
use App\Models\Placement;
use App\Http\Requests\Member\ProfileEditRequest;
use DB;

class ProfileController extends MemberController
{
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param id
	 * @return Response
	 */
	public function getEdit()
	{

		$user = $this->auth->user();
		$typeList = Member::typeList();
		$statusList = Member::statusList();
		$positionList = Placement::positionList();
		return view('member.profiles.edit', compact('user', 'typeList', 'statusList', 'positionList'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param id
	 * @return Response
	 */
	public function postEdit(ProfileEditRequest $request)
	{
		DB::beginTransaction();
		try {
			$user = $this->auth->user();
			$user->email = $request->email;
			$user->name = $request->name;
			$user->mobile = $request->mobile;
	
			$password = $request->password;
			$passwordConfirmation = $request->password_confirmation;
	
			if(!empty($password) && $password === $passwordConfirmation)
			{
				$user->password = bcrypt($password);
			}
			
			$securityPassword = $request->security_password;
			$securityPasswordConfirmation = $request->security_password_confirmation;
	
			if(!empty($securityPassword) && $securityPassword === $securityPasswordConfirmation)
			{
				$user->security_password = bcrypt($securityPassword);
			}
			
			$user->address = $request->address;
			$user->city = $request->city;
			$user->region = $request->region;
			$user->postal = $request->postal;
			$user->country = $request->country;
			
			$user->bank_name = $request->bank_name;
			$user->bank_currency = $request->bank_currency;
			$user->swift_code = $request->swift_code;
			$user->bank_country = $request->bank_country;
			$user->account_number = $request->account_number;
			$user->account_holder = $request->account_holder;
			
			$user->save();
		} catch(ValidationException $e)
		{
		    DB::rollback();
		    return redirect()->route('member.profile')->withErrors($e->getErrors())->withInput();
		} catch(\Exception $e)
		{
		    DB::rollback();
		    throw $e;
		}
		
		DB::commit();
		return redirect()->route('member.profile')->with('success', trans('member_backend.data_saved'));
	}
}
