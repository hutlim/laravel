<?php namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController as Authentication;
use Illuminate\Http\Request;
use App\Models\Member;
use Validator;

class AuthController extends Authentication {
	protected $username = 'username';
    protected $guard = 'member';
    protected $loginView = 'member.auth.login';
    protected $registerView = 'member.auth.register';
 
    /**
     * Create a new authentication controller instance.
     *
     * @param  Authenticator  $auth
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->redirectTo = route('member.home');
        $this->redirectAfterLogout = route('auth.member.login');  
    }
    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|alpha_num|min:6|unique:members,username',
            'email' => 'required|email',
            'name' => 'required|min:3',
            'password' => 'required|confirmed|min:5',
            'mobile' => 'required|min:3'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return Member::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'name' => $data['name'],
            'password' => bcrypt($data['password']),
            'mobile' => $data['mobile']
        ]);
    }
}
