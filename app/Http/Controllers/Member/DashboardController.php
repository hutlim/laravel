<?php namespace App\Http\Controllers\Member;

use App\Http\Controllers\MemberController;

class DashboardController extends MemberController {
    public function index()
    {
        return view('member.dashboard');
    }
}
