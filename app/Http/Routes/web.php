<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home']);

Route::get('locale/{lang}', ['uses' => 'LocaleController@getLocale', 'as' => 'locale']);

# Admin Backend 
Route::group(['middleware' => []], function () {
    Route::get('admin/login', ['uses' => 'Admin\AuthController@getLogin', 'as' => 'auth.admin.login']);
    Route::post('admin/login', ['uses' => 'Admin\AuthController@postLogin', 'as' => 'auth.admin.login']);
    Route::get('admin/logout', ['uses' => 'Admin\AuthController@getLogout', 'as' => 'auth.admin.logout']);
    Route::get('admin/password/email', ['uses' => 'Admin\PasswordController@getEmail', 'as' => 'auth.admin.forgot_password']);
    Route::post('admin/password/email', ['uses' => 'Admin\PasswordController@postEmail', 'as' => 'auth.admin.forgot_password']);
    Route::get('admin/password/reset/{token}', ['uses' => 'Admin\PasswordController@getReset', 'as' => 'auth.admin.reset_password']);
    Route::post('admin/password/reset', ['uses' => 'Admin\PasswordController@postReset', 'as' => 'auth.admin.reset_password']);
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin'], 'namespace' => 'Admin'], function() {
    Route::pattern('id', '[0-9]+');
	
	Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'admin.home']);
	
	# Profile
    Route::get('profile', ['uses' => 'ProfileController@getEdit', 'as' => 'admin.profile']);
	Route::post('profile', ['uses' => 'ProfileController@postEdit', 'as' => 'admin.profile']);
	
    # Dashboard
    Route::get('dashboard', ['uses' => 'DashboardController@index', 'as' => 'admin.dashboard']);
	
	# Users
	Route::group(['prefix' => 'users'], function() {
		Route::get('/', ['uses' => 'UserController@getList', 'as' => 'admin.user.list']);
	    Route::get('list', ['uses' => 'UserController@getList', 'as' => 'admin.user.list']);
	    Route::get('create', ['uses' => 'UserController@getCreate', 'as' => 'admin.user.create']);
	    Route::post('create', ['uses' => 'UserController@postCreate', 'as' => 'admin.user.create']);
	    Route::get('edit/{id}', ['uses' => 'UserController@getEdit', 'as' => 'admin.user.edit']);
	    Route::post('edit/{id}', ['uses' => 'UserController@postEdit', 'as' => 'admin.user.edit']);
	    Route::get('delete/{id}', ['uses' => 'UserController@getDelete', 'as' => 'admin.user.delete']);
		Route::get('batch/activate', ['uses' => 'UserController@getBatchActivate', 'as' => 'admin.user.batch.activate']);
		Route::get('batch/deactivate', ['uses' => 'UserController@getBatchDeactivate', 'as' => 'admin.user.batch.deactivate']);
	    Route::get('data', ['uses' => 'UserController@getData', 'as' => 'admin.user.data']);
	});
	
	# Members
	Route::group(['prefix' => 'members'], function() {
		Route::get('/', ['uses' => 'MemberController@getList', 'as' => 'admin.member.list']);
	    Route::get('list', ['uses' => 'MemberController@getList', 'as' => 'admin.member.list']);
	    Route::get('create', ['uses' => 'MemberController@getCreate', 'as' => 'admin.member.create']);
	    Route::post('create', ['uses' => 'MemberController@postCreate', 'as' => 'admin.member.create']);
	    Route::get('edit/{id}', ['uses' => 'MemberController@getEdit', 'as' => 'admin.member.edit']);
	    Route::post('edit/{id}', ['uses' => 'MemberController@postEdit', 'as' => 'admin.member.edit']);
	    Route::get('delete/{id}', ['uses' => 'MemberController@getDelete', 'as' => 'admin.member.delete']);
		Route::get('login/{id}', ['uses' => 'MemberController@getLogin', 'as' => 'admin.member.login']);
		Route::get('batch/activate', ['uses' => 'MemberController@getBatchActivate', 'as' => 'admin.member.batch.activate']);
		Route::get('batch/deactivate', ['uses' => 'MemberController@getBatchDeactivate', 'as' => 'admin.member.batch.deactivate']);
	    Route::get('data', ['uses' => 'MemberController@getData', 'as' => 'admin.member.data']);
		Route::get('load_sponsor', ['uses' => 'MemberController@getLoadSponsor', 'as' => 'member.load_sponsor']);
		Route::get('check_sponsor', ['uses' => 'MemberController@getCheckSponsor', 'as' => 'member.check_sponsor']);
		Route::get('load_placement', ['uses' => 'MemberController@getLoadPlacement', 'as' => 'member.load_placement']);
		Route::get('check_placement', ['uses' => 'MemberController@getCheckPlacement', 'as' => 'member.check_placement']);
		Route::get('check_position', ['uses' => 'MemberController@getCheckPosition', 'as' => 'member.check_position']);
	});
	
	# Sponsors
	Route::group(['prefix' => 'sponsors'], function() {
		Route::get('/', ['uses' => 'SponsorController@getTree', 'as' => 'admin.sponsor.tree']);
		Route::get('tree', ['uses' => 'SponsorController@getTree', 'as' => 'admin.sponsor.tree']);
	    Route::get('list', ['uses' => 'SponsorController@getList', 'as' => 'admin.sponsor.list']);
	    Route::get('edit/{id}', ['uses' => 'SponsorController@getEdit', 'as' => 'admin.sponsor.edit']);
	    Route::post('edit/{id}', ['uses' => 'SponsorController@postEdit', 'as' => 'admin.sponsor.edit']);
		Route::get('batch/activate', ['uses' => 'SponsorController@getBatchActivate', 'as' => 'admin.sponsor.batch.activate']);
		Route::get('batch/deactivate', ['uses' => 'SponsorController@getBatchDeactivate', 'as' => 'admin.sponsor.batch.deactivate']);
	    Route::get('data', ['uses' => 'SponsorController@getData', 'as' => 'admin.sponsor.data']);
		Route::get('process_tree', ['uses' => 'SponsorController@getProcessTree', 'as' => 'admin.sponsor.process_tree']);
	});
	
	# Placements
	Route::group(['prefix' => 'placements'], function() {
		Route::get('/', ['uses' => 'PlacementController@getTree', 'as' => 'admin.placement.tree']);
		Route::get('tree', ['uses' => 'PlacementController@getTree', 'as' => 'admin.placement.tree']);
	    Route::get('list', ['uses' => 'PlacementController@getList', 'as' => 'admin.placement.list']);
	    Route::get('edit/{id}', ['uses' => 'PlacementController@getEdit', 'as' => 'admin.placement.edit']);
	    Route::post('edit/{id}', ['uses' => 'PlacementController@postEdit', 'as' => 'admin.placement.edit']);
		Route::get('batch/activate', ['uses' => 'PlacementController@getBatchActivate', 'as' => 'admin.placement.batch.activate']);
		Route::get('batch/deactivate', ['uses' => 'PlacementController@getBatchDeactivate', 'as' => 'admin.placement.batch.deactivate']);
	    Route::get('data', ['uses' => 'PlacementController@getData', 'as' => 'admin.placement.data']);
		Route::get('process_tree', ['uses' => 'PlacementController@getProcessTree', 'as' => 'admin.placement.process_tree']);
		Route::get('process_register', ['uses' => 'PlacementController@getProcessRegister', 'as' => 'admin.placement.process_register']);
	});
	
	# PH Orders
	Route::group(['prefix' => 'ph_orders'], function() {
		Route::get('/', ['uses' => 'PHOrderController@getList', 'as' => 'admin.ph_order.list']);
	    Route::get('list', ['uses' => 'PHOrderController@getList', 'as' => 'admin.ph_order.list']);
	    Route::get('create', ['uses' => 'PHOrderController@getCreate', 'as' => 'admin.ph_order.create']);
	    Route::post('create', ['uses' => 'PHOrderController@postCreate', 'as' => 'admin.ph_order.create']);
	    Route::get('edit/{id}', ['uses' => 'PHOrderController@getEdit', 'as' => 'admin.ph_order.edit']);
	    Route::post('edit/{id}', ['uses' => 'PHOrderController@postEdit', 'as' => 'admin.ph_order.edit']);
	    Route::get('delete/{id}', ['uses' => 'PHOrderController@getDelete', 'as' => 'admin.ph_order.delete']);
		Route::get('batch/cancel', ['uses' => 'PHOrderController@getBatchCancel', 'as' => 'admin.ph_order.batch.cancel']);
		Route::get('cancelled/{id}', ['uses' => 'PHOrderController@getCancelled', 'as' => 'admin.ph_order.cancelled']);
		Route::get('paid/{id}', ['uses' => 'PHOrderController@getPaid', 'as' => 'admin.ph_order.paid']);
	    Route::get('data', ['uses' => 'PHOrderController@getData', 'as' => 'admin.ph_order.data']);
		Route::get('load_member', ['uses' => 'PHOrderController@getLoadMember', 'as' => 'admin.ph_order.load_member']);
		Route::get('check_member', ['uses' => 'PHOrderController@getCheckMember', 'as' => 'admin.ph_order.check_member']);
	});
	
	# GH Orders
	Route::group(['prefix' => 'gh_orders'], function() {
		Route::get('/', ['uses' => 'GHOrderController@getList', 'as' => 'admin.gh_order.list']);
	    Route::get('list', ['uses' => 'GHOrderController@getList', 'as' => 'admin.gh_order.list']);
		Route::get('create', ['uses' => 'GHOrderController@getCreate', 'as' => 'admin.gh_order.create']);
	    Route::post('create', ['uses' => 'GHOrderController@postCreate', 'as' => 'admin.gh_order.create']);
	    Route::get('edit/{id}', ['uses' => 'GHOrderController@getEdit', 'as' => 'admin.gh_order.edit']);
	    Route::post('edit/{id}', ['uses' => 'GHOrderController@postEdit', 'as' => 'admin.gh_order.edit']);
	    Route::get('delete/{id}', ['uses' => 'GHOrderController@getDelete', 'as' => 'admin.gh_order.delete']);
		Route::get('batch/cancel', ['uses' => 'GHOrderController@getBatchCancel', 'as' => 'admin.gh_order.batch.cancel']);
		Route::get('rejected/{id}', ['uses' => 'GHOrderController@getRejected', 'as' => 'admin.gh_order.rejected']);
		Route::get('received/{id}', ['uses' => 'GHOrderController@getReceived', 'as' => 'admin.gh_order.received']);
	    Route::get('data', ['uses' => 'GHOrderController@getData', 'as' => 'admin.gh_order.data']);
		Route::get('load_member', ['uses' => 'GHOrderController@getLoadMember', 'as' => 'admin.gh_order.load_member']);
		Route::get('check_member', ['uses' => 'GHOrderController@getCheckMember', 'as' => 'admin.gh_order.check_member']);
	});
	
	# Mavro Wallets
	Route::group(['prefix' => 'mavro_wallets'], function() {
		Route::get('/', ['uses' => 'MavroWalletController@getList', 'as' => 'admin.mavro_wallet.list']);
	    Route::get('list', ['uses' => 'MavroWalletController@getList', 'as' => 'admin.mavro_wallet.list']);
	    Route::get('create', ['uses' => 'MavroWalletController@getCreate', 'as' => 'admin.mavro_wallet.create']);
	    Route::post('create', ['uses' => 'MavroWalletController@postCreate', 'as' => 'admin.mavro_wallet.create']);
	    Route::get('edit/{id}', ['uses' => 'MavroWalletController@getEdit', 'as' => 'admin.mavro_wallet.edit']);
	    Route::post('edit/{id}', ['uses' => 'MavroWalletController@postEdit', 'as' => 'admin.mavro_wallet.edit']);
	    Route::get('delete/{id}', ['uses' => 'MavroWalletController@getDelete', 'as' => 'admin.mavro_wallet.delete']);
		Route::get('batch/cancel', ['uses' => 'MavroWalletController@getBatchCancel', 'as' => 'admin.mavro_wallet.batch.cancel']);
		Route::get('batch/close', ['uses' => 'MavroWalletController@getBatchClose', 'as' => 'admin.mavro_wallet.batch.close']);
	    Route::get('data', ['uses' => 'MavroWalletController@getData', 'as' => 'admin.mavro_wallet.data']);
		Route::get('transactions', ['uses' => 'MavroWalletController@getTransactions', 'as' => 'admin.mavro_wallet.transactions']);
		Route::get('load_member', ['uses' => 'MavroWalletController@getLoadMember', 'as' => 'admin.mavro_wallet.load_member']);
		Route::get('check_member', ['uses' => 'MavroWalletController@getCheckMember', 'as' => 'admin.mavro_wallet.check_member']);
	});
    
        
    # Wallets
    Route::group(['prefix' => 'wallets'], function() {
        Route::get('', ['uses' => 'WalletBalanceController@getList', 'as' => 'admin.wallet_balance.list']);
        Route::get('list', ['uses' => 'WalletBalanceController@getList', 'as' => 'admin.wallet_balance.list']);
        Route::get('adjustment/{id}', ['uses' => 'WalletBalanceController@getAdjustment', 'as' => 'admin.wallet_balance.adjustment']);
        Route::post('adjustment/{id}', ['uses' => 'WalletBalanceController@postAdjustment', 'as' => 'admin.wallet_balance.adjustment']);
        Route::get('data', ['uses' => 'WalletBalanceController@getData', 'as' => 'admin.wallet_balance.data']);
    });
    
    # Points
    Route::group(['prefix' => 'points'], function() {
        Route::group(['prefix' => 'transactions'], function() {
            Route::get('', ['uses' => 'PointTransactionController@getList', 'as' => 'admin.point_transaction.list']);
            Route::get('list', ['uses' => 'PointTransactionController@getList', 'as' => 'admin.point_transaction.list']);
            Route::get('data', ['uses' => 'PointTransactionController@getData', 'as' => 'admin.point_transaction.data']);
        });
    });

    # Credits
    Route::group(['prefix' => 'credits'], function() {
        Route::group(['prefix' => 'transactions'], function() {
            Route::get('/', ['uses' => 'CreditTransactionController@getList', 'as' => 'admin.credit_transaction.list']);
            Route::get('list', ['uses' => 'CreditTransactionController@getList', 'as' => 'admin.credit_transaction.list']);
            Route::get('data', ['uses' => 'CreditTransactionController@getData', 'as' => 'admin.credit_transaction.data']);
        });
    });

    # Configurations
    Route::group(['prefix' => 'configurations'], function() {
        Route::group(['prefix' => 'transaction-types'], function() {
            Route::get('/', ['uses' => 'TransactionTypeController@getList', 'as' => 'admin.transaction_type.list']);
            Route::get('list', ['uses' => 'TransactionTypeController@getList', 'as' => 'admin.transaction_type.list']);
            Route::get('create', ['uses' => 'TransactionTypeController@getCreate', 'as' => 'admin.transaction_type.create']);
            Route::post('create', ['uses' => 'TransactionTypeController@postCreate', 'as' => 'admin.transaction_type.create']);
            Route::get('edit/{id}', ['uses' => 'TransactionTypeController@getEdit', 'as' => 'admin.transaction_type.edit']);
            Route::post('edit/{id}', ['uses' => 'TransactionTypeController@postEdit', 'as' => 'admin.transaction_type.edit']);
            Route::get('delete/{id}', ['uses' => 'TransactionTypeController@getDelete', 'as' => 'admin.transaction_type.delete']);
            Route::get('batch/delete', ['uses' => 'TransactionTypeController@getBatchDelete', 'as' => 'admin.transaction_type.batch.delete']);
            Route::get('batch/activate', ['uses' => 'TransactionTypeController@getBatchActivate', 'as' => 'admin.transaction_type.batch.activate']);
            Route::get('batch/deactivate', ['uses' => 'TransactionTypeController@getBatchDeactivate', 'as' => 'admin.transaction_type.batch.deactivate']);
            Route::get('data', ['uses' => 'TransactionTypeController@getData', 'as' => 'admin.transaction_type.data']);
        });
        
        Route::group(['prefix' => 'settings'], function() {
            Route::get('/', ['uses' => 'SettingController@getList', 'as' => 'admin.setting.list']);
            Route::get('list', ['uses' => 'SettingController@getList', 'as' => 'admin.setting.list']);
            Route::get('create', ['uses' => 'SettingController@getCreate', 'as' => 'admin.setting.create']);
            Route::post('create', ['uses' => 'SettingController@postCreate', 'as' => 'admin.setting.create']);
            Route::get('edit/{id}', ['uses' => 'SettingController@getEdit', 'as' => 'admin.setting.edit']);
            Route::post('edit/{id}', ['uses' => 'SettingController@postEdit', 'as' => 'admin.setting.edit']);
            Route::get('delete/{id}', ['uses' => 'SettingController@getDelete', 'as' => 'admin.setting.delete']);
            Route::get('batch/delete', ['uses' => 'SettingController@getBatchDelete', 'as' => 'admin.setting.batch.delete']);
            Route::get('data', ['uses' => 'SettingController@getData', 'as' => 'admin.setting.data']);
        });
        
        Route::group(['prefix' => 'params'], function() {
            Route::get('/', ['uses' => 'ParamController@getList', 'as' => 'admin.param.list']);
            Route::get('list', ['uses' => 'ParamController@getList', 'as' => 'admin.param.list']);
            Route::get('create', ['uses' => 'ParamController@getCreate', 'as' => 'admin.param.create']);
            Route::post('create', ['uses' => 'ParamController@postCreate', 'as' => 'admin.param.create']);
            Route::get('edit/{id}', ['uses' => 'ParamController@getEdit', 'as' => 'admin.param.edit']);
            Route::post('edit/{id}', ['uses' => 'ParamController@postEdit', 'as' => 'admin.param.edit']);
            Route::get('delete/{id}', ['uses' => 'ParamController@getDelete', 'as' => 'admin.param.delete']);
            Route::get('batch/delete', ['uses' => 'ParamController@getBatchDelete', 'as' => 'admin.param.batch.delete']);
            Route::get('batch/activate', ['uses' => 'ParamController@getBatchActivate', 'as' => 'admin.param.batch.activate']);
            Route::get('batch/deactivate', ['uses' => 'ParamController@getBatchDeactivate', 'as' => 'admin.param.batch.deactivate']);
            Route::get('data', ['uses' => 'ParamController@getData', 'as' => 'admin.param.data']);
        });
    });

    # Language
    Route::group(['prefix' => 'language'], function() {
	    Route::get('/', 'LanguageController@index');
	    Route::get('create', 'LanguageController@getCreate');
	    Route::post('create', 'LanguageController@postCreate');
	    Route::get('{id}/edit', 'LanguageController@getEdit');
	    Route::post('{id}/edit', 'LanguageController@postEdit');
	    Route::get('{id}/delete', 'LanguageController@getDelete');
	    Route::post('{id}/delete', 'LanguageController@postDelete');
	    Route::get('data', 'LanguageController@data');
	    Route::get('reorder', 'LanguageController@getReorder');
	});

    # News category
    Route::group(['prefix' => 'news_category'], function() {
	    Route::get('/', 'ArticleCategoriesController@index');
	    Route::get('create', 'ArticleCategoriesController@getCreate');
	    Route::post('create', 'ArticleCategoriesController@postCreate');
	    Route::get('{id}/edit', 'ArticleCategoriesController@getEdit');
	    Route::post('{id}/edit', 'ArticleCategoriesController@postEdit');
	    Route::get('{id}/delete', 'ArticleCategoriesController@getDelete');
	    Route::post('{id}/delete', 'ArticleCategoriesController@postDelete');
	    Route::get('data', 'ArticleCategoriesController@data');
	    Route::get('reorder', 'ArticleCategoriesController@getReorder');
	});

    # News
    Route::group(['prefix' => 'news'], function() {
	    Route::get('/', 'ArticlesController@index');
	    Route::get('create', 'ArticlesController@getCreate');
	    Route::post('create', 'ArticlesController@postCreate');
	    Route::get('{id}/edit', 'ArticlesController@getEdit');
	    Route::post('{id}/edit', 'ArticlesController@postEdit');
	    Route::get('{id}/delete', 'ArticlesController@getDelete');
	    Route::post('{id}/delete', 'ArticlesController@postDelete');
	    Route::get('data', 'ArticlesController@data');
	    Route::get('reorder', 'ArticlesController@getReorder');
	});
});

# Member Backend
Route::group(['middleware' => []], function () {
    Route::get('member/login', ['uses' => 'Member\AuthController@getLogin', 'as' => 'auth.member.login']);
    Route::post('member/login', ['uses' => 'Member\AuthController@postLogin', 'as' => 'auth.member.login']);
    Route::get('member/logout', ['uses' => 'Member\AuthController@getLogout', 'as' => 'auth.member.logout']);
    Route::get('member/password/email', ['uses' => 'Member\PasswordController@getEmail', 'as' => 'auth.member.forgot_password']);
    Route::post('member/password/email', ['uses' => 'Member\PasswordController@postEmail', 'as' => 'auth.member.forgot_password']);
    Route::get('member/password/reset/{token}', ['uses' => 'Member\PasswordController@getReset', 'as' => 'auth.member.reset_password']);
    Route::post('member/password/reset', ['uses' => 'Member\PasswordController@postReset', 'as' => 'auth.member.reset_password']);
});

Route::group(['prefix' => 'member', 'middleware' => ['member'], 'namespace' => 'Member'], function() {
	Route::pattern('id', '[0-9]+');
	
	Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'member.home']);
	
	# Profile
    Route::get('profile', ['uses' => 'ProfileController@getEdit', 'as' => 'member.profile']);
	Route::post('profile', ['uses' => 'ProfileController@postEdit', 'as' => 'member.profile']);
	
    # Dashboard
    Route::get('dashboard', ['uses' => 'DashboardController@index', 'as' => 'member.dashboard']);
	
	# Referral
    Route::get('referral', ['uses' => 'ReferralController@index', 'as' => 'member.referral']);
	
	# Account
    Route::get('account', ['uses' => 'AccountController@index', 'as' => 'member.account']);
	
	# Mavro
    Route::get('mavro', ['uses' => 'MavroController@index', 'as' => 'member.mavro']);
});

Route::group(['prefix' => 'crud', 'middleware' => ['admin', 'type:super|admin'], 'namespace' => 'Crud'], function() {
    Route::pattern('id', '[0-9]+');

	# Curd
	Route::get('/', ['uses' => 'CrudController@getList', 'as' => 'crud.setting']);
	Route::get('list', ['uses' => 'CrudController@getList', 'as' => 'crud.list']);
	Route::get('create', ['uses' => 'CrudController@getCreate', 'as' => 'crud.create']);
	Route::post('create', ['uses' => 'CrudController@postCreate', 'as' => 'crud.create']);
	Route::get('edit/{id}', ['uses' => 'CrudController@getEdit', 'as' => 'crud.edit']);
	Route::post('edit/{id}', ['uses' => 'CrudController@postEdit', 'as' => 'crud.edit']);
	Route::get('delete/{id}', ['uses' => 'CrudController@getDelete', 'as' => 'crud.delete']);
	Route::get('data', ['uses' => 'CrudController@getData', 'as' => 'crud.data']);
});

Route::group(['prefix' => 'table', 'middleware' => ['admin', 'type:super'], 'namespace' => 'Crud'], function() {
    Route::pattern('id', '[0-9]+');

	# Table
	Route::get('{table_name}/settings', ['uses' => 'SettingsController@getSettings', 'as' => 'table.setting']);
	Route::post('{table_name}/settings', ['uses' => 'SettingsController@postSettings', 'as' => 'table.setting']);
	
	Route::get('{table_name}/list', ['uses' => 'TablesController@getList', 'as' => 'table.list']);
	Route::get('{table_name}/create', ['uses' => 'TablesController@getCreate', 'as' => 'table.create']);
	Route::post('{table_name}/create', ['uses' => 'TablesController@postCreate', 'as' => 'table.create']);
	Route::get('{table_name}/edit/{id}', ['uses' => 'TablesController@getEdit', 'as' => 'table.edit']);
	Route::post('{table_name}/edit/{id}', ['uses' => 'TablesController@postEdit', 'as' => 'table.edit']);
	Route::get('{table_name}/view/{id}', ['uses' => 'TablesController@getView', 'as' => 'table.view']);
	Route::get('{table_name}/delete/{id}', ['uses' => 'TablesController@getDelete', 'as' => 'table.delete']);
	Route::get('{table_name}/data', ['uses' => 'TablesController@getData', 'as' => 'table.data']);
});
	