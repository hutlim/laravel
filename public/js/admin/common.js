// spinner
if ($('#spinner').length) {
	$('#spinner').remove();
}

var styles = {
	'position' : 'fixed',
	'top' : '50%',
	'left' : '50%',
	'margin-left' : '-50px',
	'margin-top' : '-50px',
	'text-align' : 'center',
	'z-index' : '99999',
	'overflow' : 'auto',
	'width' : '100px',
	'height' : '102px',
	'display' : 'none'
};

$('body').append($('<div>', {
	id : 'spinner',
	css : styles
}).append($('<img />', {
	id : 'img-spinner',
	src : '/img/ajax-loader.gif'
})));
// spinner

// start modal div
var content = '<div id="delete_modal" class="modal fade">';

// start dialog
content += '<div class="modal-dialog">';

// start content
content += '<div class="modal-content">';

// header
content += '<div class="modal-header">';
content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
content += '<h4 class="modal-title">Delete Confirmation</h4>';
content += '</div>';

// body
content += '<div class="modal-body">Are you sure you want to delete these item?</div>';

// footer
content += '<div class="modal-footer">';
content += '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
content += '<a class="btn btn-danger btn-delete">Delete</a>';
content += '</div>';

// close content
content += '</div>';

// close dialog
content += '</div>';

// close modal div
content += '</div>';
$('body').append(content);

var delete_modal = $('#delete_modal');

$('body').on('click', '.delete-action', function(e) {
	e.preventDefault();
	delete_modal.find('.btn-delete').attr('href', $(this).attr('href'));
	delete_modal.modal('show');
});


// start modal div
var content = '<div id="form_modal" class="modal fade">';

// start dialog
content += '<div class="modal-dialog modal-lg">';

// start content
content += '<div class="modal-content">';

// header
content += '<div class="modal-header">';
content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
content += '<h4 class="modal-title">&nbsp</h4>';
content += '</div>';

// body
content += '<div class="modal-body"></div>';

// footer
content += '<div class="modal-footer">';
content += '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
content += '<a class="btn btn-success btn-submit">Submit</a>';
content += '</div>';
	
// close content
content += '</div>';

// close dialog
content += '</div>';

// close modal div
content += '</div>';
$('body').append(content);

// create/edit form modal
var form_modal = $('#form_modal');

$('body').on('click', '.edit-action, .add-action', function(e) {
	e.preventDefault();
	
	var url = this.href, title = $(this).data('title');
	
	$.ajax({
		url: url,
	  	method: 'GET',
	  	dataType: 'html',
  		beforeSend: function(jqXHR){
    		$('#spinner').show();
  		},
  		success: function(data) {
  			form_modal.unbind('show.bs.modal');
  			form_modal.unbind('shown.bs.modal');
  			form_modal.unbind('hide.bs.modal');
  			form_modal.unbind('hidden.bs.modal');
  			form_modal.unbind('loaded.bs.modal');
		    form_modal.find('.modal-title').text(title);
  			form_modal.find('.modal-body').html(data);
  			form_modal.modal('handleUpdate');
  			form_modal.modal('show');
	  	}
	})
  	.fail(function(jqXHR) {
  		$('#spinner').hide();
  		if(jqXHR.status === 401){
        	$(location).prop('pathname', 'admin/login');
       	}
       	else{
    		alert('Error code ' + jqXHR.status + ': ' + jqXHR.statusText);
    	}
  	})
  	.always(function() {
    	$('#spinner').hide();
  	});
});

form_modal.on('click', '.btn-submit', function(e) {
	e.preventDefault();
	form_modal.find('form').ajaxSubmit({
		delegation : true,
		target : '#form_modal .modal-body',
		'beforeSubmit' : function(arr, form, options) {
			form_modal.find('div.alert').remove();
			form_modal.find('p.error').remove();
			form_modal.find('.has-error').removeClass('has-error');
			$('#spinner').show();
		},
		'success' : function(data) {
			$('#spinner').hide();
			$($.fn.dataTable.fnTables(true)).each(function(i, table) {
				$(table).DataTable().ajax.reload();
			});
			form_modal.modal('handleUpdate');
		},
		'error' : function(jqXHR) {
			$('#spinner').hide();
			if(jqXHR.status === 401){
            	$(location).prop('pathname', 'admin/login');
           	}
	        else if(jqXHR.status === 422){
	        	var form = form_modal.find('form'), show_alert = false;
		        error_html = '<div class="alert alert-danger form-errors"><ul>';
		
		        $.each(jqXHR.responseJSON, function(key, value) {
                    if(key.indexOf('.') >= 0){
                        key = key.substring(0, key.indexOf('.')) + '[' + key.substr(key.indexOf('.') + 1) + ']';  
                    }
                    
		        	var field = form.find(':input[name="' + key + '"]');
		        	if(field.length){
		        		field.parents('div.form-group').addClass('has-error');
		        		$.each(value, function(index, msg) {
		        			field.parents('div.form-group').append('<p class="help-block error">' + msg + '</p>');
		        		});
		        	}
		        	else{
		        		show_alert = true;
		            	$.each(value, function(index, msg) {
			            	error_html += '<li>' + msg + '</li>';
			            });
		           	}
		        });
		        error_html += '</ul></div>';
		        
		        if(show_alert){
		        	form_modal.find('form').prepend(error_html);
		        	form_modal.modal('handleUpdate');
		        }
	        } 
	        else {
	            alert('Error code ' + jqXHR.status + ': ' + jqXHR.statusText);
	        }
		}
	});
});
// create/edit form modal

// custom alert
function alert($msg){
	$.notify({
		icon: 'glyphicon glyphicon-warning-sign',
		message: $msg
	},{
		z_index: 1051,
		type: 'warning'
	});
}
// custom alert

// datatable default options
$(document).ready(function() {
	if($.fn.dataTable){
		$.extend(true, $.fn.dataTable.defaults, {
			"dom": "<'row'<'col-sm-8'B><'col-sm-4 text-right'l>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		    "scrollX": true,
			"scrollCollapse": true,
			"autoWidth": false,
			"processing": true,
			"serverSide": true,
			"deferRender": true,
			"rowId": "id",
			"select": {
		        style: 'multi',
		        selector: 'td:first-child'
		   	},
		   	"buttons": [
	            {
	                text: 'Reload',
	                action: function (e, datatable, node, config) {
	                    datatable.ajax.reload();
	                }
	            },
	            'excelFlash',
        		'print'
	        ],
		   	"searching": false
		});
	}
});
// datatable default options

// select all
	$('body').on('click ifClicked', '.select_all', function(e) {
		var element = $(document.getElementById($(this).data('table-id')));
		if(element.length){
			if($(this).is(":checked")) {
				element.DataTable().rows().deselect();
			}
			else{
				element.DataTable().rows().select();
			}
		}
	});
// select all

// batch action
	$('body').on('click', '.batch-action', function(e) {
		e.preventDefault();
		var table_element = $(document.getElementById($(this).data('table-id')));
		if(table_element.length){
			$('#spinner').show();
			var ids = [];
			$.each(table_element.DataTable().rows({selected: true}).ids(), function(index, value) {
  				ids.push(value);
			});

			$.ajax({
				url: $(this).attr('href'),
			  	method: 'GET',
			  	dataType: 'json',
			  	data: {'ids': ids},
		  		beforeSend: function(jqXHR){
		    		$('#spinner').show();
		  		},
		  		success: function(json) {
				    if(json.total > 0){
						table_element.DataTable().ajax.reload();
					}
					
				  	if(json.message != ''){
				  		alert(json.message);
				  	}
			  	}
			})
			.fail(function(e) {
				$('#spinner').hide();
			    if(jqXHR.status === 401){
		        	$(location).prop('pathname', 'admin/login');
		       	}
		       	else{
		    		alert('Error code ' + jqXHR.status + ': ' + jqXHR.statusText);
		    	}
			})
			.always(function() {
			    $('#spinner').hide();
			});
		}
	});
// batch action

// tooltip
$('[data-toggle="tooltip"]').tooltip();
// tooltip