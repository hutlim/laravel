<?php namespace HutL\Themes;

use File;
use Response;
use Illuminate\Routing\Controller;

class ThemesController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Theme Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	public function css($theme, $path1, $path2 = null, $path3 = null)
	{
		if($path3){
			$file = base_path(sprintf('themes/%s/css/%s/%s/%s', $theme, $path1, $path2, $path3));
		}
		else if($path2){
			$file = base_path(sprintf('themes/%s/css/%s/%s', $theme, $path1, $path2));
		}
		else{
			$file = base_path(sprintf('themes/%s/css/%s', $theme, $path1));
		}
		
		if(File::exists($file)){
			try
			{
			    $contents = File::get($file);
				$response = Response::make($contents, 200); 
	    		$response->header('Content-Type', 'text/css'); 
	    		return $response;
			}
			catch (Illuminate\Filesystem\FileNotFoundException $exception)
			{
			    die("The file doesn't exist");
			}
		}
		
		abort(404);
	}

	public function js($theme, $path1, $path2 = null, $path3 = null)
	{
		if($path3){
			$file = base_path(sprintf('themes/%s/js/%s/%s/%s', $theme, $path1, $path2, $path3));
		}
		else if($path2){
			$file = base_path(sprintf('themes/%s/js/%s/%s', $theme, $path1, $path2));
		}
		else{
			$file = base_path(sprintf('themes/%s/js/%s', $theme, $path1));
		}
		
		if(File::exists($file)){
			try
			{
			    $contents = File::get($file);
				$response = Response::make($contents, 200); 
	    		$response->header('Content-Type', 'application/javascript'); 
	    		return $response;
			}
			catch (Illuminate\Filesystem\FileNotFoundException $exception)
			{
			    die("The file doesn't exist");
			}
		}
		
		abort(404);
	}
	
	public function img($theme, $path1, $path2 = null, $path3 = null)
	{
		if($path3){
			$file = base_path(sprintf('themes/%s/img/%s/%s/%s', $theme, $path1, $path2, $path3));
		}
		else if($path2){
			$file = base_path(sprintf('themes/%s/img/%s/%s', $theme, $path1, $path2));
		}
		else{
			$file = base_path(sprintf('themes/%s/img/%s', $theme, $path1));
		}

		if(File::exists($file)){
			try
			{
			    $contents = File::get($file);
				$response = Response::make($contents, 200); 
	    		$response->header('Content-Type', File::mimeType($file)); 
	    		return $response;
			}
			catch (Illuminate\Filesystem\FileNotFoundException $exception)
			{
			    die("The file doesn't exist");
			}
		}
		
		abort(404);
	}
}
