<?php namespace HutL\Themes;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use View;
use Blade;

/**
 * Class ThemesServiceProvider
 * @package HutL\Themes
 */
class ThemesServiceProvider extends ServiceProvider
{
	public function boot(Router $router)
	{
		$theme = config('site.theme');
		View::addLocation(base_path() . '/themes/' . $theme . '/views/');
		
		$router->group(['prefix' => 'themes/{theme}'], function($router)
        {
            $router->get('css/{path1}', ['uses' => 'HutL\Themes\ThemesController@css', 'as' => 'theme.css']);
			$router->get('css/{path1}/{path2}', ['uses' => 'HutL\Themes\ThemesController@css', 'as' => 'theme.css']);
			$router->get('css/{path1}/{path2}/{path3}', ['uses' => 'HutL\Themes\ThemesController@css', 'as' => 'theme.css']);
			$router->get('js/{path1}', ['uses' => 'HutL\Themes\ThemesController@js', 'as' => 'theme.js']);
			$router->get('js/{path1}/{path2}', ['uses' => 'HutL\Themes\ThemesController@js', 'as' => 'theme.js']);
			$router->get('js/{path1}/{path2}/{path3}', ['uses' => 'HutL\Themes\ThemesController@js', 'as' => 'theme.js']);
			$router->get('img/{path1}', ['uses' => 'HutL\Themes\ThemesController@img', 'as' => 'theme.js']);
			$router->get('img/{path1}/{path2}', ['uses' => 'HutL\Themes\ThemesController@img', 'as' => 'theme.js']);
			$router->get('img/{path1}/{path2}/{path3}', ['uses' => 'HutL\Themes\ThemesController@img', 'as' => 'theme.js']);
        });
		
		Blade::extend(function($view, $compiler)
		{
			$view = preg_replace('#\s*@style\(\s*\'\s*(.+)\s*\'\s*\)#', '<?php echo Themes::style("$1"); ?>', $view);
			$view = preg_replace('#\s*@script\(\s*\'\s*(.+)\s*\'\s*\)#', '<?php echo Themes::script("$1"); ?>', $view);
			$view = preg_replace('#\s*@image\(\s*\'\s*(.+)\s*\'\s*\)#', '<?php echo Themes::image("$1"); ?>', $view);
			return $view;
		});
		
		parent::boot($router);
	}
	
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$path = __DIR__ . '/../config/site.php';
        $this->mergeConfigFrom($path, 'site');
        $this->publishes([$path => config_path('site.php')], 'config');
		
		$this->app->singleton('themes', function($app)
		{
			return new ThemesBuilder($app['url']);
		});

		$this->app->alias('themes', 'HutL\Themes\ThemesBuilder');
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('themes');
	}

}
