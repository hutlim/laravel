<?php 

return [
    'default'   => [
        'length'    => 5,
        'width'     => 150,
        'height'    => 36,
        'quality'   => 80,
    ],
];