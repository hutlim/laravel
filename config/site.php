<?php

return [
    'theme' => env('SITE_THEME', 'default'),
    'available_locale' => explode(',', env('SITE_AVAILABLE_LOCALE', 'en,zh')),
    'default_sponsor' => 'company',
    'max_downline' => 2,
    'show_level' => 3
];