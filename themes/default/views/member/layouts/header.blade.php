<header class="main-header">
<a href="{{ route('member.home') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
  	<span class="logo-mini"><b>@lang('member_backend.site_title_short')</b></span>
  	<!-- logo for regular state and mobile devices -->
  	<span class="logo-lg"><b>@lang('member_backend.site_title')</b></span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
<!-- Sidebar toggle button-->
<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</a>
<div class="navbar-custom-menu">
<ul class="nav navbar-nav">
	<li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="glyphicon glyphicon-user"></i>
            <span>{{ Auth::guard('member')->user()->name }} <i class="caret"></i></span>
        </a>
        <ul class="dropdown-menu">
            <li class="user-header">
                <p>
                    {{ Auth::guard('member')->user()->name }}
                    <small>@lang('admin_backend.joined_since', ['date' => Auth::guard('member')->user()->created_at->format('d/m/Y')])</small>
                </p>
                <p>
                	{{ Auth::guard('member')->user()->email }}
                </p>
            </li>
            <li class="user-footer">
                <div class="pull-left">
                    <a href="{{ route('member.profile') }}" class="btn btn-default btn-flat">@lang('member_backend.profile_title')</a>
                </div>
                <div class="pull-right">
                    <a href="{{ route('member.logout') }}" class="btn btn-default btn-flat">@lang('member_backend.logout')</a>
                </div>
            </li>
        </ul>
    </li>
</ul>
</div>
</nav>
</header>