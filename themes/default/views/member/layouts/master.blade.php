<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title> @section('title')
			@lang('member_backend.member_backend')
			@show </title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

		{!! HTML::style("plugins/bootstrap/css/bootstrap.min.css") !!}
		{!! HTML::style("plugins/font-awesome/css/font-awesome.min.css") !!}
		{!! HTML::style("plugins/iCheck/minimal/minimal.css") !!}
		@style('AdminLTE.min.css')
		@style('skins/skin-blue.min.css')
		@style('app.min.css')

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		{!! HTML::script("https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js") !!}
		{!! HTML::style("https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js") !!}
		<![endif]-->

		@yield('styles')

	</head>

	<body class="skin-blue sidebar-mini">
		<!-- header logo: style can be found in header.less -->
		<div class="wrapper">
			@include('member.layouts.header')

			<!-- Left side column. contains the logo and sidebar -->

			@include('member.layouts.navigation')

			<!-- Right side column. Contains the navbar and content of the page -->
			<div class="content-wrapper">

				@yield('content')

			</div><!-- /.right-side -->

			@include('member.layouts.footer')
		</div><!-- ./wrapper -->

		{!! HTML::script("plugins/jQuery/jQuery-2.1.4.min.js") !!}
		{!! HTML::script("plugins/ajaxform/jquery.form.min.js") !!}
		{!! HTML::script("plugins/bootstrap/js/bootstrap.min.js") !!}
		{!! HTML::script("plugins/bootstrap-notify/bootstrap-notify.min.js") !!}
		{!! HTML::script("plugins/bootstrap-typeahead/bootstrap-typeahead.min.js") !!}
		{!! HTML::script("plugins/iCheck/icheck.min.js") !!}
		{!! HTML::script("plugins/fastclick/fastclick.min.js") !!}
		{!! HTML::script("js/member/common.js") !!}
		@script('app.min.js')

		<script type="text/javascript">
			$(function() {
				$('input[type="checkbox"], input[type="radio"]').iCheck({
					checkboxClass : 'icheckbox_minimal',
					radioClass : 'iradio_minimal'
				});
			});
		</script>
		@yield('scripts')

	</body>
</html>