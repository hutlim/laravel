<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>@lang('member_backend.version')</b>
	</div>
	<strong>@lang('member_backend.copyright')</strong>
</footer>