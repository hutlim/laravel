@if(Session::has('success'))
<div class="alert alert-success" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <b>Success!</b> {{Session::get('success')}}
</div>
@elseif(Session::has('error'))
<div class="alert alert-danger" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <b>Error!</b> {{Session::get('error')}}
</div>
@elseif(Session::has('info'))
<div class="alert alert-info" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <b>Info!</b> {{Session::get('info')}}
</div>
@elseif(Session::has('warning'))
<div class="alert alert-warning" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <b>Warning!</b> {{Session::get('warning')}}
</div>
@endif