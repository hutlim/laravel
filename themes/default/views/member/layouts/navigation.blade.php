<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active">
                <a href="{{ route('member.dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>@lang('member_backend.home')</span>
                </a>
            </li>
            <li>
                <a href="{{ route('member.profile') }}">
                    <i class="fa fa-user"></i> <span>@lang('member_backend.profile_title')</span>
                </a>
            </li>
            <li>
                <a href="{{ route('member.account') }}">
                    <i class="fa fa-money"></i> <span>@lang('member_backend.account_title')</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>