<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>@lang('member_auth.member_backend')</title>

		{!! HTML::style("plugins/bootstrap/css/bootstrap.min.css") !!}
		{!! HTML::style("plugins/iCheck/minimal/minimal.css") !!}
		@style('AdminLTE.min.css')

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		@yield('styles')
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			@yield('content')
		</div>

		<!-- Scripts -->
		{!! HTML::script("plugins/jQuery/jQuery-2.1.4.min.js") !!}
		{!! HTML::script("plugins/bootstrap/js/bootstrap.min.js") !!}

		@yield('scripts')
		</script>
	</body>
</html>
