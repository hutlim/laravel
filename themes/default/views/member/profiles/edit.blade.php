@extends('member.layouts.master')

@section('title') @parent :: @lang('member_backend.home')
@stop

@section('styles')
{!! HTML::style("plugins/datatables/media/css/dataTables.bootstrap.min.css") !!}
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  	<h1>@lang('member_backend.home')</h1>
  	<ol class="breadcrumb">
  		<li>
			<a href="/"><i class="fa fa-home"></i> @lang('member_backend.home')</a>
		</li>
		<li class="active">
			@lang('member_backend.profile')
		</li>
  	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
		<!-- /.box-header -->
		<div class="box-body">
			@include('member.layouts.notifications')

			{!! Form::open() !!}
			<div class="row">
				<div class="col-md-6">
					<div class="form-group {{ $errors->has('sponsor_username') ? 'has-error' : '' }}">
						{!! Form::label('sponsor_username', trans('admin_backend.sponsor_username')) !!}
						{!! Form::text('sponsor_username', $user->sponsor->upline && $user->sponsor->upline->member ? $user->sponsor->upline->member->username : '', ['class' => 'form-control', 'disabled' => 'disabled']) !!}
							
						{!! Form::text('sponsor_name', $user->sponsor->upline && $user->sponsor->upline->member ? $user->sponsor->upline->member->name : '', ['class' => 'form-control', 'disabled' => 'disabled']) !!}
						@foreach ($errors->get('sponsor_username') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('placement_username') ? 'has-error' : '' }}">
						{!! Form::label('placement_username', trans('admin_backend.placement_username')) !!}
						{!! Form::text('placement_username', $user->placement->upline && $user->placement->upline->member ? $user->placement->upline->member->username : '', ['class' => 'form-control', 'disabled' => 'disabled']) !!}
						
						{!! Form::text('placement_name', $user->placement->upline && $user->placement->upline->member ? $user->placement->upline->member->name : '', ['class' => 'form-control', 'disabled' => 'disabled']) !!}
						{!! Form::text('placement_name', $user->placement->position_title, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
						@foreach ($errors->get('placement_username') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
						{!! Form::label('username', trans('admin_backend.username')) !!}
						{!! Form::text('username', $user->username, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
						@foreach ($errors->get('username') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
						{!! Form::label('email', trans('admin_backend.email')) !!}
						{!! Form::email('email', old('email', $user->email), ['class' => 'form-control', 'placeholder' => trans('admin_backend.email')]) !!}
						@foreach ($errors->get('email') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						{!! Form::label('name', trans('admin_backend.name')) !!}
						{!! Form::text('name', old('name', $user->name), ['class' => 'form-control', 'placeholder' => trans('admin_backend.name')]) !!}
						@foreach ($errors->get('name') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
						{!! Form::label('mobile', trans('admin_backend.mobile')) !!}
						{!! Form::text('mobile', old('mobile', $user->mobile), ['class' => 'form-control', 'placeholder' => trans('admin_backend.mobile')]) !!}
						@foreach ($errors->get('mobile') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
						{!! Form::label('password', trans('admin_backend.password')) !!}
						{!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('admin_backend.password')]) !!}
						@foreach ($errors->get('password') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group">
						{!! Form::label('password_confirmation', trans('admin_backend.confirm_password')) !!}
						{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('admin_backend.confirm_password')]) !!}
					</div>
			
					<div class="form-group {{ $errors->has('security_password') ? 'has-error' : '' }}">
						{!! Form::label('security_password', trans('admin_backend.security_password')) !!}
						{!! Form::password('security_password', ['class' => 'form-control', 'placeholder' => trans('admin_backend.security_password')]) !!}
						@foreach ($errors->get('security_password') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group">
						{!! Form::label('security_password_confirmation', trans('admin_backend.confirm_security_password')) !!}
						{!! Form::password('security_password_confirmation', ['class' => 'form-control', 'placeholder' => trans('admin_backend.confirm_security_password')]) !!}
					</div>
			
					<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
						{!! Form::label('type', trans('admin_backend.type')) !!}
						{!! Form::text('type', $user->type_title, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
						@foreach ($errors->get('type') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
						{!! Form::label('status', trans('admin_backend.status')) !!}
						{!! Form::text('status', $user->status_title, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
						@foreach ($errors->get('status') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('joined') ? 'has-error' : '' }}">
						{!! Form::label('joined', trans('admin_backend.joined')) !!}
						{!! Form::text('joined', $user->joined->format('d/m/Y'), ['class' => 'form-control', 'disabled' => 'disabled']) !!}
						@foreach ($errors->get('joined') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('register_country') ? 'has-error' : '' }}">
						{!! Form::label('register_country', trans('admin_backend.register_country')) !!}
						{!! Form::text('joined', $user->register_country_title, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
						@foreach ($errors->get('register_country') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<hr />
				</div>
				<div class="col-md-6">
					<h3>@lang('admin_backend.residential_address')</h3>
					<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
						{!! Form::label('address', trans('admin_backend.address')) !!}
						{!! Form::text('address', old('address', $user->address), ['class' => 'form-control', 'placeholder' => trans('admin_backend.address')]) !!}
						@foreach ($errors->get('address') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
						{!! Form::label('city', trans('admin_backend.city')) !!}
						{!! Form::text('city', old('city', $user->city), ['class' => 'form-control', 'placeholder' => trans('admin_backend.city')]) !!}
						@foreach ($errors->get('city') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('region') ? 'has-error' : '' }}">
						{!! Form::label('region', trans('admin_backend.region')) !!}
						{!! Form::text('region', old('region', $user->region), ['class' => 'form-control', 'placeholder' => trans('admin_backend.region')]) !!}
						@foreach ($errors->get('region') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('postal') ? 'has-error' : '' }}">
						{!! Form::label('postal', trans('admin_backend.postal')) !!}
						{!! Form::text('postal', old('postal', $user->postal), ['class' => 'form-control', 'placeholder' => trans('admin_backend.postal')]) !!}
						@foreach ($errors->get('postal') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
						{!! Form::label('country', trans('admin_backend.country')) !!}
						{!! Form::select('country', Countries::getList(App::getLocale()), old('country', $user->country), ['class' => 'form-control']) !!}
						@foreach ($errors->get('country') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<hr />
			
					<h3>@lang('admin_backend.bank_details')</h3>
					<div class="form-group {{ $errors->has('bank_country') ? 'has-error' : '' }}">
						{!! Form::label('bank_country', trans('admin_backend.bank_country')) !!}
						{!! Form::select('bank_country', Countries::getList(App::getLocale()), old('bank_country', $user->bank_country), ['class' => 'form-control']) !!}
						@foreach ($errors->get('bank_country') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('bank_name') ? 'has-error' : '' }}">
						{!! Form::label('bank_name', trans('admin_backend.bank_name')) !!}
						{!! Form::text('bank_name', old('bank_name', $user->bank_name), ['class' => 'form-control', 'placeholder' => trans('admin_backend.bank_name')]) !!}
						@foreach ($errors->get('bank_name') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('bank_currency') ? 'has-error' : '' }}">
						{!! Form::label('bank_currency', trans('admin_backend.bank_currency')) !!}
						{!! Form::text('bank_currency', old('bank_currency', $user->bank_currency), ['class' => 'form-control', 'placeholder' => trans('admin_backend.bank_currency')]) !!}
						@foreach ($errors->get('bank_currency') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('swift_code') ? 'has-error' : '' }}">
						{!! Form::label('swift_code', trans('admin_backend.swift_code')) !!}
						{!! Form::text('swift_code', old('swift_code', $user->swift_code), ['class' => 'form-control', 'placeholder' => trans('admin_backend.swift_code')]) !!}
						@foreach ($errors->get('swift_code') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('account_number') ? 'has-error' : '' }}">
						{!! Form::label('account_number', trans('admin_backend.account_number')) !!}
						{!! Form::text('account_number', old('account_number', $user->account_number), ['class' => 'form-control', 'placeholder' => trans('admin_backend.account_number')]) !!}
						@foreach ($errors->get('account_number') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
			
					<div class="form-group {{ $errors->has('account_holder') ? 'has-error' : '' }}">
						{!! Form::label('account_holder', trans('admin_backend.account_holder')) !!}
						{!! Form::text('account_holder', old('account_holder', $user->account_holder), ['class' => 'form-control', 'placeholder' => trans('admin_backend.account_holder')]) !!}
						@foreach ($errors->get('account_holder') as $error)
						<p class="help-block error">
							{{ $error }}
						</p>
						@endforeach
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section><!-- /.content -->
@stop

@section('scripts')
<script type="text/javascript">
    $(function () {

    });
</script>
@stop