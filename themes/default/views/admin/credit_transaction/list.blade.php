@extends('admin.layouts.master')

@section('title') @parent :: @lang('admin_backend.credit_transactions')
@stop

@section('styles')
{!! HTML::style("plugins/datatables/media/css/dataTables.bootstrap.min.css") !!}
{!! HTML::style("plugins/datatables/extensions/Select/css/select.bootstrap.min.css") !!}
{!! HTML::style("plugins/datatables/extensions/Buttons/css/buttons.bootstrap.min.css") !!}
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> @lang('admin_backend.credit_transactions') <small>@lang('admin_backend.list')</small></h1>
	<ol class="breadcrumb">
		<li>
			<a href="/"><i class="fa fa-home"></i> @lang('admin_backend.home')</a>
		</li>
		<li class="active">
			@lang('admin_backend.credit_transactions')
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
		<!-- /.box-header -->
		<div class="box-body">
			@include('admin.layouts.notifications')

			<hr />

			{!! Form::open(['id' => 'search_form']) !!}
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('created_at', trans('admin_backend.date')) !!}
						<div class="row">
							<div class="col-sm-5">
								{!! Form::input('date', 'date_from', old('date_from'), ['class' => 'form-control']) !!}
							</div>
							<div class="col-sm-5">
								{!! Form::input('date', 'date_to', old('date_to'), ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
				    <div class="form-group">
                        {!! Form::label('username', trans('admin_backend.username')) !!}
                        {!! Form::text('username', old('username'), ['class' => 'form-control']) !!}
                    </div>	
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('reference', trans('admin_backend.reference')) !!}
						{!! Form::text('reference', old('reference'), ['class' => 'form-control']) !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('type', trans('admin_backend.type')) !!}
						{!! Form::select('type', $typeList, old('type'), ['class' => 'form-control']) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					{!! Form::submit(trans('admin_backend.search'), ['class' => 'btn btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}

			<hr />

			<table id="credit_transaction_list" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th class="text-center">{!! Form::checkbox('select_all', '1', false, ['class' => 'form-control select_all', 'data-table-id' => 'credit_transaction_list']) !!}</th>
						<th>@lang('admin_backend.date')</th>
						<th>@lang('admin_backend.username')</th>
						<th>@lang('admin_backend.type')</th>
						<th>@lang('admin_backend.reference')</th>
						<th>@lang('admin_backend.description')</th>
						<th>@lang('admin_backend.credit')</th>
						<th>@lang('admin_backend.debit')</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section><!-- /.content -->
@stop

@section('scripts')
{!! HTML::script("plugins/datatables/media/js/jquery.dataTables.min.js") !!}
{!! HTML::script("plugins/datatables/media/js/dataTables.bootstrap.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Select/js/dataTables.select.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.bootstrap.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.flash.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.print.min.js") !!}
<script type="text/javascript">
	$(function() {
		var list = $('#credit_transaction_list').DataTable({
			"order" : [[1, "desc"]],
			"ajax" : "{{ route('admin.credit_transaction.data') }}",
			"columnDefs" : [{
				"className" : 'select-checkbox',
				"render" : function(data, type, row) {
					return '';
				},
				"targets" : 0,
				"orderable" : false,
				"searchable" : false
			}, {
				"targets" : -1,
				"orderable" : false,
				"searchable" : false
			}],
			"fnServerParams" : function(data) {
				data.filter = $('#search_form').serialize();
			},
			columns : [{
				data : 'id',
				name : 'id'
			}, {
				data : 'date',
				name : 'date'
			}, {
				data : 'username',
				name : 'members.username'
			}, {
				data : 'type',
				name : 'type'
			}, {
				data : 'reference',
				name : 'reference'
			}, {
				data : 'description',
				name : 'description'
			}, {
				data : 'credit',
				name : 'credit'
			}, {
				data : 'debit',
				name : 'debit'
			}]
		});

		$('#search_form').submit(function(e) {
			e.preventDefault();
			list.ajax.reload();
		});
	}); 
</script>
@stop