@if(session('success'))
<div class="alert alert-success" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<b>Success!</b> {{session('success')}}
</div>
@elseif(session('error'))
<div class="alert alert-danger" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<b>Error!</b> {{session('error')}}
</div>
@elseif(session('info'))
<div class="alert alert-info" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<b>Info!</b> {{session('info')}}
</div>
@elseif(session('warning'))
<div class="alert alert-warning" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<b>Warning!</b> {{session('warning')}}
</div>
@endif