<header class="main-header">
	<a href="{{ route('admin.home') }}" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels --> <span class="logo-mini"><b></b></span> <!-- logo for regular state and mobile devices --> <span class="logo-lg"><b>@lang('admin_backend.admin_backend')</b></span> </a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                        <img src="{{ Avatar::create(Auth::guard('admin')->user()->name)->setDimension(160, 160)->toBase64() }}" class="user-image" alt="{{ Auth::guard('admin')->user()->name }}">
                        <span class="hidden-xs">{{ Auth::guard('admin')->user()->name }}</span> 
                    </a>
					<ul class="dropdown-menu">
						<li class="user-header">
                            <img src="{{ Avatar::create(Auth::guard('admin')->user()->name)->setDimension(160, 160)->toBase64() }}" class="img-circle" alt="{{ Auth::guard('admin')->user()->name }}">
							<p>
								{{ Auth::guard('admin')->user()->name }}
								<small>@lang('admin_backend.joined_since', ['date' => Auth::guard('admin')->user()->created_at->format('d/m/Y')])</small>
							</p>
						</li>
						<li class="user-footer">
							<div class="pull-left">
								<a href="{{ route('admin.profile') }}" data-title="@lang('admin_backend.profile')" class="btn btn-default btn-flat edit-action">@lang('admin_backend.profile')</a>
							</div>
							<div class="pull-right">
								<a href="{{ route('auth.admin.logout') }}" class="btn btn-default btn-flat">@lang('admin_backend.logout')</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>