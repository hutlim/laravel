<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Avatar::create(Auth::guard('admin')->user()->name)->setDimension(160, 160)->toBase64() }}" class="img-circle" alt="{{ Auth::guard('admin')->user()->name }}" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::guard('admin')->user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="active">
				<a href="{{ route('admin.dashboard') }}"> <i class="fa fa-home"></i> <span>@lang('admin_backend.home')</span> </a>
			</li>
			<li class="treeview">
				<a href="#"> <i class="fa fa-user-secret"></i> <span>@lang('admin_backend.user_manage')</span> <i class="fa fa-angle-left pull-right"></i> </a>
				<ul class="treeview-menu">
					<li>
						<a href="{{ route('admin.user.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.user_list')</a>
					</li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"> <i class="fa fa-user"></i> <span>@lang('admin_backend.member_manage')</span> <i class="fa fa-angle-left pull-right"></i> </a>
				<ul class="treeview-menu">
					<li>
						<a href="{{ route('admin.member.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.member_list')</a>
					</li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"> <i class="fa fa-users"></i> <span>@lang('admin_backend.genealogy_manage')</span> <i class="fa fa-angle-left pull-right"></i> </a>
				<ul class="treeview-menu">
					<li>
						<a href="{{ route('admin.sponsor.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.sponsor_list')</a>
					</li>
					<li>
						<a href="{{ route('admin.sponsor.tree') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.sponsor_tree')</a>
					</li>
					<li>
						<a href="{{ route('admin.placement.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.placement_list')</a>
					</li>
					<li>
						<a href="{{ route('admin.placement.tree') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.placement_tree')</a>
					</li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"> <i class="fa fa-heart"></i> <span>@lang('admin_backend.ph_order_manage')</span> <i class="fa fa-angle-left pull-right"></i> </a>
				<ul class="treeview-menu">
					<li>
						<a href="{{ route('admin.ph_order.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.ph_order_list')</a>
					</li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"> <i class="fa fa-heart-o"></i> <span>@lang('admin_backend.gh_order_manage')</span> <i class="fa fa-angle-left pull-right"></i> </a>
				<ul class="treeview-menu">
					<li>
						<a href="{{ route('admin.gh_order.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.gh_order_list')</a>
					</li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"> <i class="fa fa-dollar"></i> <span>@lang('admin_backend.mavro_manage')</span> <i class="fa fa-angle-left pull-right"></i> </a>
				<ul class="treeview-menu">
					<li>
						<a href="{{ route('admin.mavro_wallet.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.mavro_wallet_list')</a>
					</li>
				</ul>
			</li>
            <li class="treeview">
                <a href="#"> <i class="fa fa-dollar"></i> <span>@lang('admin_backend.wallet_manage')</span> <i class="fa fa-angle-left pull-right"></i> </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.wallet_balance.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.wallet_balance_list')</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.point_transaction.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.point_transaction_list')</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.credit_transaction.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.credit_transaction_list')</a>
                    </li>
                </ul>
            </li> 
            <li class="treeview">
                <a href="#"> <i class="fa fa-dollar"></i> <span>@lang('admin_backend.configuration_manage')</span> <i class="fa fa-angle-left pull-right"></i> </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.transaction_type.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.transaction_type_list')</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.param.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.param_list')</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.setting.list') }}"><i class="fa fa-circle-o"></i> @lang('admin_backend.setting_list')</a>
                    </li>
                    <li>
                        <a href="/{{ config('translation-manager.route.prefix') }}" target="_blank"><i class="fa fa-circle-o"></i> @lang('admin_backend.translate_list')</a>
                    </li>
                </ul>
            </li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>