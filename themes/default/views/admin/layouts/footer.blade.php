<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>@lang('admin_backend.version')</b>
	</div>
	<strong>@lang('admin_backend.copyright')</strong>
</footer>