@extends('admin.auth.master')

@section('title') @parent :: @lang('admin_auth.forgot_password')
@stop

@section('styles')
@style('admin/login.css')
@stop

@section('content')
<div class="login-logo">
	<a href="#">@lang('admin_auth.admin_backend')</a>
</div>
<div class="login-box-body">
	<p class="login-box-msg">
		<b>@lang('admin_auth.forgot_password_title')</b>
	</p>
	@if (session('status'))
	<div class="alert alert-success">
		{{ session('status') }}
	</div>
	@endif

	@if (count($errors) > 0)
	<div class="alert alert-danger">
		<ul class="list-unstyled">
			@foreach ($errors->all() as $error)
			<li>
				{{ $error }}
			</li>
			@endforeach
		</ul>
	</div>
	@endif
	{!! Form::open(['id' => 'form', 'name' => 'form']) !!}
	<div class="form-group has-feedback">
		<input id="username" type="text" class="form-control" name="username" required value="{{ old('username') }}" placeholder="@lang('admin_auth.username')">
		<span class="glyphicon glyphicon-user form-control-feedback"></span>
	</div>

	<div class="form-group">
		{!! captcha_img() !!}
		<input id="captcha" type="text" class="form-control captcha" name="captcha" required placeholder="@lang('admin_auth.captcha')">
	</div>

	<button type="submit" class="btn btn-primary btn-block btn-flat">
		@lang('admin_auth.btn_send_pasword')
	</button>
	{!! Form::close() !!}
</div>
@stop

@section('scripts')

@stop
