@extends('admin.auth.master')

@section('title') @parent :: @lang('admin_auth.login')
@stop

@section('styles')
{!! HTML::style("plugins/iCheck/square/blue.css") !!}
@style('admin/login.css')
@stop

@section('content')
<div class="login-logo">
	<a href="#">@lang('admin_auth.admin_backend')</a>
</div>
<div class="login-box-body">
	<p class="login-box-msg">
		<b>@lang('admin_auth.login_title')</b>
	</p>
	@if (count($errors) > 0)
	<div class="alert alert-danger">
		<ul class="list-unstyled">
			@foreach ($errors->all() as $error)
			<li>
				{{ $error }}
			</li>
			@endforeach
		</ul>
	</div>
	@endif
	{!! Form::open(['id' => 'form', 'name' => 'form']) !!}
	<div class="form-group has-feedback">
		<input id="username" type="text" class="form-control" name="username" required value="{{ old('username') }}" placeholder="@lang('admin_auth.username')">
		<span class="glyphicon glyphicon-user form-control-feedback"></span>
	</div>

	<div class="form-group has-feedback">
		<input id="password" type="password" class="form-control" name="password" required placeholder="@lang('admin_auth.password')">
		<span class="glyphicon glyphicon-lock form-control-feedback"></span>
	</div>

	<div class="form-group">
		{!! captcha_img() !!}
		<input id="captcha" type="text" class="form-control captcha" name="captcha" required placeholder="@lang('admin_auth.captcha')">
	</div>

	<div class="row">
		<div class="col-xs-8">
			<div class="checkbox icheck">
				<label>
					<input type="checkbox" name="remember">
					@lang('admin_auth.remember_me') </label>
			</div>
		</div>
		<div class="col-xs-4">
			<button type="submit" class="btn btn-primary btn-block btn-flat">
				@lang('admin_auth.btn_login')
			</button>
		</div>
	</div>
	{!! Form::close() !!}
	<a href="{{ route('auth.admin.forgot_password') }}">@lang('admin_auth.forgot_password')</a>
</div>
@stop

@section('scripts')
{!! HTML::script("plugins/iCheck/icheck.min.js") !!}
<script type="text/javascript">
	$(function() {
		$('input').iCheck({
			checkboxClass : 'icheckbox_square-blue',
			radioClass : 'iradio_square-blue',
			increaseArea : '20%' // optional
		});
	}); 
</script>
@stop
