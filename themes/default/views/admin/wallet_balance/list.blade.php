@extends('admin.layouts.master')

@section('title') @parent :: @lang('admin_backend.wallet_balance')
@stop

@section('styles')
{!! HTML::style("plugins/datatables/media/css/dataTables.bootstrap.min.css") !!}
{!! HTML::style("plugins/datatables/extensions/Select/css/select.bootstrap.min.css") !!}
{!! HTML::style("plugins/datatables/extensions/Buttons/css/buttons.bootstrap.min.css") !!}
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> @lang('admin_backend.wallet_balance') <small>@lang('admin_backend.list')</small></h1>
	<ol class="breadcrumb">
		<li>
			<a href="/"><i class="fa fa-home"></i> @lang('admin_backend.home')</a>
		</li>
		<li class="active">
			@lang('admin_backend.wallet_balance')
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
		<!-- /.box-header -->
		<div class="box-body">
			@include('admin.layouts.notifications')

			<hr />

			{!! Form::open(['id' => 'search_form']) !!}
			<div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('username', trans('admin_backend.username')) !!}
                        {!! Form::text('username', old('username'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('mobile', trans('admin_backend.mobile')) !!}
                        {!! Form::text('mobile', old('mobile'), ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('email', trans('admin_backend.email')) !!}
                        {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('name', trans('admin_backend.name')) !!}
                        {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

			<div class="row">
				<div class="col-xs-12">
					{!! Form::submit(trans('admin_backend.search'), ['class' => 'btn btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}

			<hr />

			<table id="wallet_balance_list" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th class="text-center">{!! Form::checkbox('select_all', '1', false, ['class' => 'form-control select_all', 'data-table-id' => 'wallet_balance_list']) !!}</th>
						<th>@lang('admin_backend.username')</th>
                        <th>@lang('admin_backend.email')</th>
                        <th>@lang('admin_backend.name')</th>
                        <th>@lang('admin_backend.mobile')</th>
						<th>@lang('admin_backend.point')</th>
                        <th>@lang('admin_backend.credit')</th>
                        <th>@lang('admin_backend.actions')</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section><!-- /.content -->
@stop

@section('scripts')
{!! HTML::script("plugins/datatables/media/js/jquery.dataTables.min.js") !!}
{!! HTML::script("plugins/datatables/media/js/dataTables.bootstrap.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Select/js/dataTables.select.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.bootstrap.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.flash.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.print.min.js") !!}
<script type="text/javascript">
	$(function() {
		var list = $('#wallet_balance_list').DataTable({
			"order" : [[1, "desc"]],
			"ajax" : "{{ route('admin.wallet_balance.data') }}",
			"columnDefs" : [{
				"className" : 'select-checkbox',
				"render" : function(data, type, row) {
					return '';
				},
				"targets" : 0,
				"orderable" : false,
				"searchable" : false
			}, {
				"targets" : -1,
				"orderable" : false,
				"searchable" : false
			}],
			"fnServerParams" : function(data) {
				data.filter = $('#search_form').serialize();
			},
			columns : [{
				data : 'id',
				name : 'id'
			}, {
				data : 'username',
				name : 'username'
			}, {
				data : 'email',
				name : 'email'
			}, {
				data : 'name',
				name : 'name'
			}, {
				data : 'mobile',
				name : 'mobile'
			}, {
				data : 'point_balance',
				name : 'point_balance'
			}, {
                data : 'credit_balance',
                name : 'credit_balance'
            }, {
				data : 'actions',
				name : 'actions'
			}]
		});

		$('#search_form').submit(function(e) {
			e.preventDefault();
			list.ajax.reload();
		});
	}); 
</script>
@stop