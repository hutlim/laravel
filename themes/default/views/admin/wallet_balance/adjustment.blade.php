{!! Form::open() !!}

@include('admin.layouts.notifications')

<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
            {!! Form::label('username', trans('admin_backend.username')) !!}
            {!! Form::text('username', old('username', $member->username), ['class' => 'form-control', 'id' => 'username', 'disabled' => 'disabled']) !!}
            @foreach ($errors->get('username') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>

        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
            {!! Form::label('email', trans('admin_backend.email')) !!}
            {!! Form::text('email', old('title', $member->email), ['class' => 'form-control', 'id' => 'email', 'disabled' => 'disabled']) !!}
            @foreach ($errors->get('email') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>
        
        <div class="form-group {{ $errors->has('wallet_type') ? 'has-error' : '' }}">
            {!! Form::label('wallet_type', trans('admin_backend.wallet_type')) !!}
            {!! Form::select('wallet_type', $walletTypeList, old('wallet_type'), ['class' => 'form-control', 'id' => 'wallet_type']) !!}
            @foreach ($errors->get('wallet_type') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>
        
        <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
            {!! Form::label('description', trans('admin_backend.description')) !!}
            {!! Form::text('description', old('description'), ['class' => 'form-control', 'id' => 'description', 'placeholder' => trans('admin_backend.description')]) !!}
            @foreach ($errors->get('description') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            {!! Form::label('name', trans('admin_backend.name')) !!}
            {!! Form::text('name', old('title', $member->name), ['class' => 'form-control', 'id' => 'name', 'disabled' => 'disabled']) !!}
            @foreach ($errors->get('name') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>

        <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
            {!! Form::label('mobile', trans('admin_backend.mobile')) !!}
            {!! Form::text('mobile', old('title', $member->mobile), ['class' => 'form-control', 'id' => 'mobile', 'disabled' => 'disabled']) !!}
            @foreach ($errors->get('mobile') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>
        
        <div class="form-group {{ $errors->has('transaction_type') ? 'has-error' : '' }}">
            {!! Form::label('transaction_type', trans('admin_backend.transaction_type')) !!}
            {!! Form::select('transaction_type', $transactionTypeList, old('transaction_type'), ['class' => 'form-control', 'id' => 'transaction_type']) !!}
            @foreach ($errors->get('transaction_type') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>
        
        <div class="form-group {{ $errors->has('adjust_amount') ? 'has-error' : '' }}">
            {!! Form::label('adjust_amount', trans('admin_backend.adjust_amount')) !!}
            {!! Form::input('number', 'adjust_amount', old('adjust_amount'), ['class' => 'form-control', 'id' => 'adjust_amount', 'placeholder' => trans('admin_backend.adjust_amount')]) !!}
            @foreach ($errors->get('adjust_amount') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>
    </div>
</div>
{!! Form::close() !!}