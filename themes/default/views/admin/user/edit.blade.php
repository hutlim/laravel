{!! Form::open() !!}

@include('admin.layouts.notifications')

<div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
	{!! Form::label('username', trans('admin_backend.username')) !!}
	{!! Form::text('username', old('username', $user->username), ['class' => 'form-control', 'placeholder' => trans('admin_backend.username')]) !!}
	@foreach ($errors->get('username') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
	{!! Form::label('email', trans('admin_backend.email')) !!}
	{!! Form::text('email', old('email', $user->email), ['class' => 'form-control', 'placeholder' => trans('admin_backend.email')]) !!}
	@foreach ($errors->get('email') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
	{!! Form::label('name', trans('admin_backend.name')) !!}
	{!! Form::text('name', old('name', $user->name), ['class' => 'form-control', 'placeholder' => trans('admin_backend.name')]) !!}
	@foreach ($errors->get('name') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
	{!! Form::label('password', trans('admin_backend.password')) !!}
	{!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('admin_backend.password')]) !!}
	@foreach ($errors->get('password') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group">
	{!! Form::label('password_confirmation', trans('admin_backend.confirm_password')) !!}
	{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('admin_backend.confirm_password')]) !!}
</div>

<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
	{!! Form::label('type', trans('admin_backend.type')) !!}
	{!! Form::select('type', $typeList, old('type', $user->type), ['class' => 'form-control']) !!}
	@foreach ($errors->get('type') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
	{!! Form::label('status', trans('admin_backend.status')) !!}
	{!! Form::select('status', $statusList, old('status', $user->status), ['class' => 'form-control']) !!}
	@foreach ($errors->get('status') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

{!! Form::close() !!}