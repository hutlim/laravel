{!! Form::open() !!}

@include('admin.layouts.notifications')

<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
            {!! Form::label('type', trans('admin_backend.type')) !!}
            {!! Form::text('type', old('type', $param->type), ['class' => 'form-control', 'id' => 'type', 'disabled' => 'disabled']) !!}
            <small class="form-text text-muted">@lang('admin_backend.alpha_underscore_allowed')</small>
            @foreach ($errors->get('type') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>

        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
            {!! Form::label('title', trans('admin_backend.title')) !!}
            @foreach (config('site.available_locale') as $locale)
                <div class="input-group">
                    <div class="input-group-addon">{{ strtoupper($locale) }}</div>
                    {{ Form::text("title[{$locale}]", old('title', isset($param->title[$locale]) ? $param->title[$locale] : ''), ['class' => 'form-control', 'id' => 'title']) }}
                </div>
            @endforeach
            @foreach ($errors->get('title') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('code') ? 'has-error' : '' }}">
            {!! Form::label('code', trans('admin_backend.code')) !!}
            {!! Form::text('code', old('code', $param->code), ['class' => 'form-control', 'id' => 'code', 'disabled' => 'disabled']) !!}
            <small class="form-text text-muted">@lang('admin_backend.alpha_underscore_allowed')</small>
            @foreach ($errors->get('code') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>
        
        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            {!! Form::label('status', trans('admin_backend.status')) !!}
            {!! Form::select('status', $statusList, old('status', $param->status), ['class' => 'form-control']) !!}
            @foreach ($errors->get('status') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>
    </div>
</div>
{!! Form::close() !!}