@extends('admin.layouts.master')

@section('title') @parent :: @lang('admin_backend.param')
@stop

@section('styles')
{!! HTML::style("plugins/datatables/media/css/dataTables.bootstrap.min.css") !!}
{!! HTML::style("plugins/datatables/extensions/Select/css/select.bootstrap.min.css") !!}
{!! HTML::style("plugins/datatables/extensions/Buttons/css/buttons.bootstrap.min.css") !!}
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> @lang('admin_backend.param') <small>@lang('admin_backend.list')</small></h1>
	<ol class="breadcrumb">
		<li>
			<a href="/"><i class="fa fa-home"></i> @lang('admin_backend.home')</a>
		</li>
		<li class="active">
			@lang('admin_backend.param')
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
		<!-- /.box-header -->
		<div class="box-body">
			@include('admin.layouts.notifications')

			<a href="{{ route('admin.param.create') }}" class="btn btn-success add-action" data-title="@lang('admin_backend.create_param')"><span class="glyphicon glyphicon-plus-sign"></span> @lang('admin_backend.create_param')</a>

			<hr />

			{!! Form::open(['id' => 'search_form']) !!}
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('created_at', trans('admin_backend.created')) !!}
						<div class="row">
							<div class="col-sm-5">
								{!! Form::input('date', 'created_from', old('created_from'), ['class' => 'form-control']) !!}
							</div>
							<div class="col-sm-5">
								{!! Form::input('date', 'created_to', old('created_to'), ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
				    <div class="form-group">
                        {!! Form::label('type', trans('admin_backend.type')) !!}
                        {!! Form::text('type', old('type'), ['class' => 'form-control']) !!}
                    </div>	
				</div>
			</div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('code', trans('admin_backend.code')) !!}
                        {!! Form::text('code', old('code'), ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('status', trans('admin_backend.status')) !!}
                        {!! Form::select('status', $statusList, old('status'), ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

			<div class="row">
				<div class="col-xs-12">
					{!! Form::submit(trans('admin_backend.search'), ['class' => 'btn btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}

			<hr />

			<p>
				<a href="{{ route('admin.param.batch.activate') }}" class="btn btn-success batch-action" data-table-id="param_list"><span class="glyphicon glyphicon-ok-sign"></span> @lang('admin_backend.activate')</a>
                <a href="{{ route('admin.param.batch.deactivate') }}" class="btn btn-warning batch-action" data-table-id="param_list"><span class="glyphicon glyphicon-remove-sign"></span> @lang('admin_backend.deactivate')</a>
                <a href="{{ route('admin.param.batch.delete') }}" class="btn btn-danger batch-action" data-table-id="param_list"><span class="glyphicon glyphicon-trash"></span> @lang('admin_backend.delete')</a>
			</p>
			<table id="param_list" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th class="text-center">{!! Form::checkbox('select_all', '1', false, ['class' => 'form-control select_all', 'data-table-id' => 'param_list']) !!}</th>
						<th>@lang('admin_backend.created')</th>
                        <th>@lang('admin_backend.type')</th>
						<th>@lang('admin_backend.code')</th>
						<th>@lang('admin_backend.title')</th>
                        <th>@lang('admin_backend.status')</th>
						<th>@lang('admin_backend.actions')</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section><!-- /.content -->
@stop

@section('scripts')
{!! HTML::script("plugins/datatables/media/js/jquery.dataTables.min.js") !!}
{!! HTML::script("plugins/datatables/media/js/dataTables.bootstrap.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Select/js/dataTables.select.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.bootstrap.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.flash.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.print.min.js") !!}
<script type="text/javascript">
	$(function() {
		var list = $('#param_list').DataTable({
			"order" : [[1, "desc"]],
			"ajax" : "{{ route('admin.param.data') }}",
			"columnDefs" : [{
				"className" : 'select-checkbox',
				"render" : function(data, type, row) {
					return '';
				},
				"targets" : 0,
				"orderable" : false,
				"searchable" : false
			}, {
				"targets" : -1,
				"orderable" : false,
				"searchable" : false
			}],
			"fnServerParams" : function(data) {
				data.filter = $('#search_form').serialize();
			},
			columns : [{
				data : 'id',
				name : 'id'
			}, {
				data : 'created_at',
				name : 'created_at'
			}, {
                data : 'type',
                name : 'type'
            }, {
				data : 'code',
				name : 'code'
			}, {
				data : 'title',
				name : 'title'
			}, {
                data : 'status',
                name : 'status'
            }, {
				data : 'actions',
				name : 'actions'
			}]
		});

		$('#search_form').submit(function(e) {
			e.preventDefault();
			list.ajax.reload();
		});
	}); 
</script>
@stop