@extends('admin.layouts.master')

@section('title') @parent :: @lang('admin_backend.ph_orders')
@stop

@section('styles')
{!! HTML::style("plugins/datatables/media/css/dataTables.bootstrap.min.css") !!}
{!! HTML::style("plugins/datatables/extensions/Select/css/select.bootstrap.min.css") !!}
{!! HTML::style("plugins/datatables/extensions/Buttons/css/buttons.bootstrap.min.css") !!}
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> @lang('admin_backend.ph_orders') <small>@lang('admin_backend.list')</small></h1>
	<ol class="breadcrumb">
		<li>
			<a href="/"><i class="fa fa-home"></i> @lang('admin_backend.home')</a>
		</li>
		<li class="active">
			@lang('admin_backend.ph_orders')
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
		<!-- /.box-header -->
		<div class="box-body">
			@include('admin.layouts.notifications')

			<a href="{{ route('admin.ph_order.create') }}" class="btn btn-success add-action" data-title="@lang('admin_backend.create_ph_order')"><span class="glyphicon glyphicon-plus-sign"></span> @lang('admin_backend.create_ph_order')</a>

			<hr />

			{!! Form::open(['id' => 'search_form']) !!}
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('created_at', trans('admin_backend.created')) !!}
						<div class="row">
							<div class="col-sm-5">
								{!! Form::input('date', 'created_from', old('created_from'), ['class' => 'form-control']) !!}
							</div>
							<div class="col-sm-5">
								{!! Form::input('date', 'created_to', old('created_to'), ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('sponsor_username', trans('admin_backend.sponsor_username')) !!}
						{!! Form::text('sponsor_username', old('sponsor_username'), ['class' => 'form-control']) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('username', trans('admin_backend.username')) !!}
						{!! Form::text('username', old('username'), ['class' => 'form-control']) !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('reference', trans('admin_backend.reference')) !!}
						{!! Form::text('reference', old('reference'), ['class' => 'form-control']) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('type', trans('admin_backend.type')) !!}
						{!! Form::select('type', $typeList, old('type'), ['class' => 'form-control']) !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('status', trans('admin_backend.status')) !!}
						{!! Form::select('status', $statusList, old('status'), ['class' => 'form-control']) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('amount', trans('admin_backend.amount')) !!}
						{!! Form::input('number', 'amount', old('amount'), ['class' => 'form-control', 'min' => '0.01', 'step' => '100']) !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('outstanding', trans('admin_backend.outstanding')) !!}
						{!! Form::input('number', 'outstanding', old('outstanding'), ['class' => 'form-control', 'min' => '0.01', 'step' => '100']) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					{!! Form::submit(trans('admin_backend.search'), ['class' => 'btn btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}

			<hr />

			<p>
				<a href="{{ route('admin.ph_order.batch.cancel') }}" class="btn btn-warning batch-action" data-table-id="order_list"><span class="glyphicon glyphicon-minus-sign"></span> @lang('admin_backend.cancel')</a>
			</p>
			<table id="order_list" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th class="text-center">{!! Form::checkbox('select_all', '1', false, ['class' => 'form-control select_all', 'data-table-id' => 'order_list']) !!}</th>
						<th>@lang('admin_backend.created')</th>
						<th>@lang('admin_backend.sponsor')</th>
						<th>@lang('admin_backend.username')</th>
						<th>@lang('admin_backend.type')</th>
						<th>@lang('admin_backend.reference')</th>
						<th>@lang('admin_backend.amount')</th>
						<th>@lang('admin_backend.outstanding')</th>
						<th>@lang('admin_backend.status')</th>
						<th>@lang('admin_backend.actions')</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section><!-- /.content -->
@stop

@section('scripts')
{!! HTML::script("plugins/datatables/media/js/jquery.dataTables.min.js") !!}
{!! HTML::script("plugins/datatables/media/js/dataTables.bootstrap.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Select/js/dataTables.select.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.bootstrap.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.flash.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.print.min.js") !!}
<script type="text/javascript">
	$(function() {
		var list = $('#order_list').DataTable({
			"order" : [[1, "desc"]],
			"ajax" : "{{ route('admin.ph_order.data') }}",
			"columnDefs" : [{
				"className" : 'select-checkbox',
				"render" : function(data, type, row) {
					return '';
				},
				"targets" : 0,
				"orderable" : false,
				"searchable" : false
			}, {
				"targets" : -1,
				"orderable" : false,
				"searchable" : false
			}],
			"fnServerParams" : function(data) {
				data.filter = $('#search_form').serialize();
			},
			columns : [{
				data : 'id'
			}, {
				data : 'created_at'
			}, {
				data : 'sponsor_username'
			}, {
				data : 'username'
			}, {
				data : 'type'
			}, {
				data : 'reference'
			}, {
				data : 'amount'
			}, {
				data : 'outstanding'
			}, {
				data : 'status'
			}, {
				data : 'actions'
			}]
		});

		$('#search_form').submit(function(e) {
			e.preventDefault();
			list.ajax.reload();
		});
	}); 
</script>
@stop