{!! Form::open() !!}

@include('admin.layouts.notifications')

<div class="form-group {{ $errors->has('created_at') ? 'has-error' : '' }}">
	{!! Form::label('created_at', trans('admin_backend.created')) !!}
	{!! Form::input('datetime', 'created_at', old('created_at', $phOrder->created_at->format('d/m/Y H:i:s')), ['class' => 'form-control', 'placeholder' => trans('admin_backend.created'), 'disabled' => 'disabled']) !!}
	@foreach ($errors->get('created_at') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
	{!! Form::label('username', trans('admin_backend.username')) !!}
	{!! Form::text('username', old('username', $phOrder->member->username), ['class' => 'form-control', 'placeholder' => trans('admin_backend.username'), 'disabled' => 'disabled']) !!}
	@foreach ($errors->get('username') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
	{!! Form::label('type', trans('admin_backend.type')) !!}
	{!! Form::text('type', old('type', $phOrder->type_title), ['class' => 'form-control', 'placeholder' => trans('admin_backend.type'), 'disabled' => 'disabled']) !!}
	@foreach ($errors->get('type') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
	{!! Form::label('amount', trans('admin_backend.amount')) !!}
	{!! Form::input('number', 'amount', old('amount', $phOrder->amount), ['class' => 'form-control', 'placeholder' => trans('admin_backend.amount'), 'disabled' => 'disabled']) !!}
	@foreach ($errors->get('amount') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('outstanding') ? 'has-error' : '' }}">
	{!! Form::label('outstanding', trans('admin_backend.outstanding')) !!}
	{!! Form::input('number', 'outstanding', old('outstanding', $phOrder->outstanding), ['class' => 'form-control', 'placeholder' => trans('admin_backend.outstanding'), 'disabled' => 'disabled']) !!}
	@foreach ($errors->get('outstanding') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
	{!! Form::label('status', trans('admin_backend.status')) !!}
	{!! Form::select('status', $statusList, old('status', $phOrder->status), ['class' => 'form-control', 'disabled' => 'disabled']) !!}
	@foreach ($errors->get('status') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

{!! Form::close() !!}

<h4>@lang('admin_backend.match_gh_orders')</h4>
<div class="table-responsive">
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>@lang('admin_backend.username')</th><th>@lang('admin_backend.reference')</th><th>@lang('admin_backend.started')</th><th>@lang('admin_backend.ended')</th><th>@lang('admin_backend.amount')</th><th>@lang('admin_backend.status')</th><th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($phOrder->orderTransactions as $orderTransaction)
			<tr>
				<td>{{ $orderTransaction->ghMember->username }}</td><td>{{ $orderTransaction->reference }}</td><td>{{ $orderTransaction->started->format('d/m/Y H:i:s') }}</td><td>{{ $orderTransaction->ended->format('d/m/Y H:i:s') }}</td><td>{{ $orderTransaction->amount }}</td><td>{{ $orderTransaction->status_title }}</td><td>@if($orderTransaction->status == 'Pending')<a href="{{ route('admin.ph_order.cancelled', ['id' => $orderTransaction->id]) }}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> @lang('admin_backend.cancelled')</a> <a href="{{ route('admin.ph_order.paid', ['id' => $orderTransaction->id]) }}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span> @lang('admin_backend.paid')</a>@endif</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>