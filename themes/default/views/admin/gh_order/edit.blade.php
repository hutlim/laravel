{!! Form::open() !!}

@include('admin.layouts.notifications')

<div class="row">
	<div class="col-md-6">
		<div class="form-group {{ $errors->has('created_at') ? 'has-error' : '' }}">
			{!! Form::label('created_at', trans('admin_backend.created')) !!}
			{!! Form::input('datetime', 'created_at', old('created_at', $ghOrder->created_at->format('d/m/Y H:i:s')), ['class' => 'form-control', 'placeholder' => trans('admin_backend.created'), 'disabled' => 'disabled']) !!}
			@foreach ($errors->get('created_at') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
			{!! Form::label('username', trans('admin_backend.username')) !!}
			{!! Form::text('username', old('username', $ghOrder->member->username), ['class' => 'form-control', 'placeholder' => trans('admin_backend.username'), 'disabled' => 'disabled']) !!}
			@foreach ($errors->get('username') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
			{!! Form::label('amount', trans('admin_backend.amount')) !!}
			{!! Form::input('number', 'amount', old('amount', $ghOrder->amount), ['class' => 'form-control', 'placeholder' => trans('admin_backend.amount'), 'disabled' => 'disabled']) !!}
			@foreach ($errors->get('amount') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('outstanding') ? 'has-error' : '' }}">
			{!! Form::label('outstanding', trans('admin_backend.outstanding')) !!}
			{!! Form::input('number', 'outstanding', old('outstanding', $ghOrder->outstanding), ['class' => 'form-control', 'placeholder' => trans('admin_backend.outstanding'), 'disabled' => 'disabled']) !!}
			@foreach ($errors->get('outstanding') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
			{!! Form::label('status', trans('admin_backend.status')) !!}
			{!! Form::select('status', $statusList, old('status', $ghOrder->status), ['class' => 'form-control', 'disabled' => 'disabled']) !!}
			@foreach ($errors->get('status') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>
		<hr />
	</div>
	<div class="col-md-6">
		<div class="form-group {{ $errors->has('bank_country') ? 'has-error' : '' }}">
			{!! Form::label('bank_country', trans('admin_backend.bank_country')) !!}
			{!! Form::select('bank_country', Countries::getList(App::getLocale()), old('bank_country', $ghOrder->bank_country), ['class' => 'form-control']) !!}
			@foreach ($errors->get('bank_country') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('bank_name') ? 'has-error' : '' }}">
			{!! Form::label('bank_name', trans('admin_backend.bank_name')) !!}
			{!! Form::text('bank_name', old('bank_name', $ghOrder->bank_name), ['class' => 'form-control', 'placeholder' => trans('admin_backend.bank_name')]) !!}
			@foreach ($errors->get('bank_name') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('bank_currency') ? 'has-error' : '' }}">
			{!! Form::label('bank_currency', trans('admin_backend.bank_currency')) !!}
			{!! Form::text('bank_currency', old('bank_currency', $ghOrder->bank_currency), ['class' => 'form-control', 'placeholder' => trans('admin_backend.bank_currency')]) !!}
			@foreach ($errors->get('bank_currency') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('swift_code') ? 'has-error' : '' }}">
			{!! Form::label('swift_code', trans('admin_backend.swift_code')) !!}
			{!! Form::text('swift_code', old('swift_code', $ghOrder->swift_code), ['class' => 'form-control', 'placeholder' => trans('admin_backend.swift_code')]) !!}
			@foreach ($errors->get('swift_code') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('account_number') ? 'has-error' : '' }}">
			{!! Form::label('account_number', trans('admin_backend.account_number')) !!}
			{!! Form::text('account_number', old('account_number', $ghOrder->account_number), ['class' => 'form-control', 'placeholder' => trans('admin_backend.account_number')]) !!}
			@foreach ($errors->get('account_number') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('account_holder') ? 'has-error' : '' }}">
			{!! Form::label('account_holder', trans('admin_backend.account_holder')) !!}
			{!! Form::text('account_holder', old('account_holder', $ghOrder->account_holder), ['class' => 'form-control', 'placeholder' => trans('admin_backend.account_holder')]) !!}
			@foreach ($errors->get('account_holder') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>
		<hr />
	</div>
</div>

{!! Form::close() !!}

<h4>@lang('admin_backend.match_ph_orders')</h4>
<div class="table-responsive">
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>@lang('admin_backend.username')</th><th>@lang('admin_backend.reference')</th><th>@lang('admin_backend.started')</th><th>@lang('admin_backend.ended')</th><th>@lang('admin_backend.amount')</th><th>@lang('admin_backend.status')</th><th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($ghOrder->orderTransactions as $orderTransaction)
			<tr>
				<td>{{ $orderTransaction->phMember->username }}</td><td>{{ $orderTransaction->reference }}</td><td>{{ $orderTransaction->started->format('d/m/Y H:i:s') }}</td><td>{{ $orderTransaction->ended->format('d/m/Y H:i:s') }}</td><td>{{ $orderTransaction->amount }}</td><td>{{ $orderTransaction->status_title }}</td><td>@if($orderTransaction->status == 'Paid')<a href="{{ route('admin.gh_order.rejected', ['id' => $orderTransaction->id]) }}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> @lang('admin_backend.rejected')</a> <a href="{{ route('admin.gh_order.received', ['id' => $orderTransaction->id]) }}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span> @lang('admin_backend.received')</a>@endif</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>