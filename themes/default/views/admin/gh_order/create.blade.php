{!! Form::open(['id' => 'gp_order_create_form']) !!}

@include('admin.layouts.notifications')

<div class="row">
	<div class="col-md-6">
		<div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
			{!! Form::label('username', trans('admin_backend.username')) !!}
			<div class="input-group">
				{!! Form::text('username', old('username'), ['class' => 'form-control', 'id' => 'username', 'placeholder' => trans('admin_backend.username'), 'autocomplete' => 'off']) !!}
				<span class="input-group-btn">
					<button class="btn btn-default check-member" type="button">
						@lang('admin_backend.load')
					</button> </span>
			</div>
			{!! Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'readonly' => 'readonly']) !!}
			@foreach ($errors->get('username') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('mavro_wallet_id') ? 'has-error' : '' }}">
			{!! Form::label('mavro_wallet_id', trans('admin_backend.mavro_wallet')) !!}
			{!! Form::select('mavro_wallet_id', [], old('mavro_wallet_id'), ['class' => 'form-control']) !!}
			@foreach ($errors->get('mavro_wallet_id') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
			{!! Form::label('amount', trans('admin_backend.amount')) !!}
			{!! Form::input('number', 'amount', old('amount'), ['class' => 'form-control', 'placeholder' => trans('admin_backend.amount'), 'min' => '0.01', 'step' => '100']) !!}
			@foreach ($errors->get('amount') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<hr />
	</div>
	<div class="col-md-6">
		<div class="form-group {{ $errors->has('bank_country') ? 'has-error' : '' }}">
			{!! Form::label('bank_country', trans('admin_backend.bank_country')) !!}
			{!! Form::select('bank_country', Countries::getList(App::getLocale()), old('bank_country'), ['class' => 'form-control', 'id' => 'bank_country']) !!}
			@foreach ($errors->get('bank_country') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('bank_name') ? 'has-error' : '' }}">
			{!! Form::label('bank_name', trans('admin_backend.bank_name')) !!}
			{!! Form::text('bank_name', old('bank_name'), ['class' => 'form-control', 'placeholder' => trans('admin_backend.bank_name'), 'id' => 'bank_name']) !!}
			@foreach ($errors->get('bank_name') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('bank_currency') ? 'has-error' : '' }}">
			{!! Form::label('bank_currency', trans('admin_backend.bank_currency')) !!}
			{!! Form::text('bank_currency', old('bank_currency'), ['class' => 'form-control', 'placeholder' => trans('admin_backend.bank_currency'), 'id' => 'bank_currency']) !!}
			@foreach ($errors->get('bank_currency') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('swift_code') ? 'has-error' : '' }}">
			{!! Form::label('swift_code', trans('admin_backend.swift_code')) !!}
			{!! Form::text('swift_code', old('swift_code'), ['class' => 'form-control', 'placeholder' => trans('admin_backend.swift_code'), 'id' => 'swift_code']) !!}
			@foreach ($errors->get('swift_code') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('account_number') ? 'has-error' : '' }}">
			{!! Form::label('account_number', trans('admin_backend.account_number')) !!}
			{!! Form::text('account_number', old('account_number'), ['class' => 'form-control', 'placeholder' => trans('admin_backend.account_number'), 'id' => 'account_number']) !!}
			@foreach ($errors->get('account_number') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('account_holder') ? 'has-error' : '' }}">
			{!! Form::label('account_holder', trans('admin_backend.account_holder')) !!}
			{!! Form::text('account_holder', old('account_holder'), ['class' => 'form-control', 'placeholder' => trans('admin_backend.account_holder'), 'id' => 'account_holder']) !!}
			@foreach ($errors->get('account_holder') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>
		<hr />
	</div>
</div>

{!! Form::close() !!}

<script type="text/javascript">
	$(function() {
		var form = $('#gp_order_create_form');
		form.find("#username").typeahead({
			ajax: {
				url: '{{ route('admin.gh_order.load_member') }}',
				triggerLength: 2,
				preDispatch: function (query) {
					return {
						search : query
					}
				}, 
				preProcess: function (data) {
					return data;
				}
			}
		});

		form.find('button.check-member').on('click', function(e) {
			e.preventDefault();
			var self = $(this), input = form.find('#username');
			if(input.val() == ''){
				alert('@lang('admin_backend.username_required')');
				return false;
			}

			$.ajax({
				url: '{{ route('admin.gh_order.check_member') }}',
				dataType: 'json',
				data: {'username': input.val()},
				beforeSend: function(jqXHR, settings) {
					self.button('loading');
					input.prop('disabled', true);
					form.find('#name').val('');
					form.find('#mavro_wallet_id').find('option').remove();
				}, 
				success: function(json) {
					if (json.result == 'success') {
						form.find('#name').val(json.data.name);
						form.find('#bank_country').val(json.data.bank_country);
						form.find('#bank_name').val(json.data.bank_name);
						form.find('#bank_currency').val(json.data.bank_currency);
						form.find('#swift_code').val(json.data.swift_code);
						form.find('#account_number').val(json.data.account_number);
						form.find('#account_holder').val(json.data.account_holder);
						form.find('#mavro_wallet_id').find('option').remove();
						form.find('#mavro_wallet_id').fadeIn('slow', function() {
							$.each(json.data.mavroWallets, function(key, value) {
								form.find('#mavro_wallet_id').append($('<option>', {
									value : value.id,
									text : value.reference + ' (' + value.balance + ')'
								}));
							});
						});
					}

					if (json.message != '') {
						alert(json.message);
					}
				}
			}).fail(function(e) {
				self.button('reset');
				input.prop('disabled', false);
				if (jqXHR.status === 401) {
					$(location).prop('pathname', 'admin/login');
				} else {
					alert('Error code ' + jqXHR.status + ': ' + jqXHR.statusText);
				}
			}).always(function() {
				self.button('reset');
				input.prop('disabled', false);
			});
		});
	});
</script>