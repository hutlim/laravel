{!! Form::open(['id' => 'mavro_wallet_create_form']) !!}

@include('admin.layouts.notifications')

<div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
	{!! Form::label('username', trans('admin_backend.username')) !!}
	<div class="input-group">
		{!! Form::text('username', old('username'), ['class' => 'form-control', 'id' => 'username', 'placeholder' => trans('admin_backend.username'), 'autocomplete' => 'off']) !!}
		<span class="input-group-btn">
			<button class="btn btn-default check-member" type="button">
				@lang('admin_backend.check')
			</button> </span>
	</div>
	{!! Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'readonly' => 'readonly']) !!}
	@foreach ($errors->get('username') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
	{!! Form::label('type', trans('admin_backend.type')) !!}
	{!! Form::select('type', $typeList, old('type'), ['class' => 'form-control']) !!}
	@foreach ($errors->get('type') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
	{!! Form::label('amount', trans('admin_backend.amount')) !!}
	{!! Form::input('number', 'amount', old('amount'), ['class' => 'form-control', 'placeholder' => trans('admin_backend.amount'), 'min' => '0.01', 'step' => '100']) !!}
	@foreach ($errors->get('amount') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

{!! Form::close() !!}

<script type="text/javascript">
	$(function() {
		var form = $('#mavro_wallet_create_form');
		form.find("#username").typeahead({
			ajax: {
				url: '{{ route('admin.mavro_wallet.load_member') }}',
				triggerLength: 2,
				preDispatch: function (query) {
					return {
						search : query
					}
				}, 
				preProcess: function (data) {
					return data;
				}
			}
		});

		form.find('button.check-member').on('click', function(e) {
			e.preventDefault();
			var self = $(this), input = form.find('#username');
			if(input.val() == ''){
				alert('@lang('admin_backend.username_required')');
				return false;
			}
	
			$.ajax({
				url: '{{ route('admin.mavro_wallet.check_member') }}',
				dataType: 'json',
				data: {'username': input.val()},
				beforeSend: function(jqXHR, settings) {
					self.button('loading');
					input.prop('disabled', true);
					form.find('#name').val('');
				}, 
				success: function(json) {
					if (json.result == 'success') {
						form.find('#name').val(json.data.name);
					}
			
					if (json.message != '') {
						alert(json.message);
					}
				}
			}).fail(function(e) {
				self.button('reset');
				input.prop('disabled', false);
				if (jqXHR.status === 401) {
					$(location).prop('pathname', 'admin/login');
				} else {
					alert('Error code ' + jqXHR.status + ': ' + jqXHR.statusText);
				}
			}).always(function() {
				self.button('reset');
				input.prop('disabled', false);
			});
		});
	});
</script>