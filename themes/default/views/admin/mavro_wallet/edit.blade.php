{!! Form::open() !!}

@include('admin.layouts.notifications')

<div class="form-group {{ $errors->has('created_at') ? 'has-error' : '' }}">
	{!! Form::label('created_at', trans('admin_backend.created')) !!}
	{!! Form::input('datetime', 'created_at', old('created_at', $mavroWallet->created_at->format('d/m/Y H:i:s')), ['class' => 'form-control', 'placeholder' => trans('admin_backend.created'), 'disabled' => 'disabled']) !!}
	@foreach ($errors->get('created_at') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
	{!! Form::label('username', trans('admin_backend.username')) !!}
	{!! Form::text('username', old('username', $mavroWallet->member->username), ['class' => 'form-control', 'placeholder' => trans('admin_backend.username'), 'disabled' => 'disabled']) !!}
	@foreach ($errors->get('username') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
	{!! Form::label('type', trans('admin_backend.type')) !!}
	{!! Form::text('type', old('type', $mavroWallet->type_title), ['class' => 'form-control', 'placeholder' => trans('admin_backend.type'), 'disabled' => 'disabled']) !!}
	@foreach ($errors->get('type') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('balance') ? 'has-error' : '' }}">
	{!! Form::label('balance', trans('admin_backend.balance')) !!}
	{!! Form::input('number', 'adjust', old('adjust'), ['class' => 'form-control', 'placeholder' => trans('admin_backend.adjust_amount')]) !!}
	{!! Form::input('number', 'amount', old('balance', $mavroWallet->balance), ['class' => 'form-control', 'placeholder' => trans('admin_backend.balance'), 'disabled' => 'disabled']) !!}
	@foreach ($errors->get('balance') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
	{!! Form::label('status', trans('admin_backend.status')) !!}
	{!! Form::select('status', $statusList, old('status', $mavroWallet->status), ['class' => 'form-control', 'disabled' => 'disabled']) !!}
	@foreach ($errors->get('status') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

{!! Form::close() !!}

<h4>@lang('admin_backend.transaction_histories')</h4>
<div class="table-responsive">
	<table id="transaction_list" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>@lang('admin_backend.id')</th><th>@lang('admin_backend.date')</th><th>@lang('admin_backend.type')</th><th>@lang('admin_backend.reference')</th><th>@lang('admin_backend.credit')</th><th>@lang('admin_backend.debit')</th><th>@lang('admin_backend.balance')</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
</div>

<script type="text/javascript">
	$(function() {
		var list = $('#transaction_list').DataTable({
			"order" : [[1, "desc"]],
			"ajax" : "{{ route('admin.mavro_wallet.transactions', ['id' => $mavroWallet->id]) }}",
			"columnDefs" : [{
				"targets" : 0,
				"visible" : true,
			}],
			columns : [{
				data : 'id',
				name : 'id'
			}, {
				data : 'date',
				name : 'date'
			}, {
				data : 'type',
				name : 'type'
			}, {
				data : 'reference',
				name : 'reference'
			}, {
				data : 'credit',
				name : 'credit'
			}, {
				data : 'debit',
				name : 'debit'
			}, {
				data : 'balance',
				name : 'balance'
			}],
			"select" : false
		});
	}); 
</script>