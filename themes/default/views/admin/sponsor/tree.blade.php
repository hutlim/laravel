@extends('admin.layouts.master')

@section('title') @parent :: @lang('admin_backend.sponsors')
@stop

@section('styles')
{!! HTML::style("plugins/jstree/themes/default/style.min.css") !!}
@style('admin/sponsor_tree.css')
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> @lang('admin_backend.sponsors') <small>@lang('admin_backend.tree')</small></h1>
	<ol class="breadcrumb">
		<li>
			<a href="/"><i class="fa fa-home"></i> @lang('admin_backend.home')</a>
		</li>
		<li class="active">
			@lang('admin_backend.sponsors')
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
		<!-- /.box-header -->
		<div class="box-body">

			@include('admin.layouts.notifications')

			<div id="treemenu" class="treemenu row">
				<div class="col-sm-6">
					<div class="btn-group">
						<a href="#" class="btn btn-default" id="refresh">@lang('admin_backend.refresh')</a>
						<a href="#" class="btn btn-default" id="open_all">@lang('admin_backend.open_all')</a>
						<a href="#" class="btn btn-default" id="close_all">@lang('admin_backend.close_all')</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="input-group">
						{!! Form::text('search_text', old('search_text'), ['class' => 'form-control', 'id' => 'search_text']) !!}
						<span class="input-group-btn"> <a href="#" class="btn btn-default" id="clear_search">@lang('admin_backend.clear')</a> <a href="#" class="btn btn-default" id="search">@lang('admin_backend.search')</a> </span>
					</div>
				</div>
			</div>
			<br />
			<p>
				<span class="tree-title">@lang('admin_backend.username')</span>
				<span class="tree-title">@lang('admin_backend.type')</span>
				<span class="tree-title">@lang('admin_backend.status')</span>
				<span class="tree-title">@lang('admin_backend.joined')</span>
				<span class="tree-title">@lang('admin_backend.total_direct_downlines')</span>
				<span class="tree-title">@lang('admin_backend.total_sponsor_downlines')</span>
			</p>
			<div id="tree" class="tree" data-default-root="{{ $root->id }}" data-root="{{ $root->id }}" data-process-url="{{ route('admin.sponsor.process_tree') }}"></div>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section><!-- /.content -->

<div class="modal fade" id="member_details" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
				<h4 class="modal-title">@lang('admin_backend.member_details')</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">
					@lang('admin_backend.close')
				</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
{!! HTML::script("plugins/jstree/jstree.min.js") !!}
@script('admin/sponsor_tree.min.js')
@stop