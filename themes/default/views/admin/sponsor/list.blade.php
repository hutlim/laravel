@extends('admin.layouts.master')

@section('title') @parent :: @lang('admin_backend.sponsors')
@stop

@section('styles')
{!! HTML::style("plugins/datatables/media/css/dataTables.bootstrap.min.css") !!}
{!! HTML::style("plugins/datatables/extensions/Select/css/select.bootstrap.min.css") !!}
{!! HTML::style("plugins/datatables/extensions/Buttons/css/buttons.bootstrap.min.css") !!}
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> @lang('admin_backend.sponsors') <small>@lang('admin_backend.list')</small></h1>
	<ol class="breadcrumb">
		<li>
			<a href="/"><i class="fa fa-home"></i> @lang('admin_backend.home')</a>
		</li>
		<li class="active">
			@lang('admin_backend.sponsors')
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
		<!-- /.box-header -->
		<div class="box-body">

			@include('admin.layouts.notifications')

			{!! Form::open(['id' => 'search_form']) !!}
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('sponsor_username', trans('admin_backend.sponsor_username')) !!}
						{!! Form::text('sponsor_username', old('sponsor_username', $defaultSponsor), ['class' => 'form-control']) !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('level', trans('admin_backend.level')) !!}
						<div class="row">
							<div class="col-sm-5">
								{!! Form::input('number', 'level_from', old('level_from', 0), ['class' => 'form-control', 'min' => '0', 'step' => '1']) !!}
							</div>
							<div class="col-sm-5">
								{!! Form::input('number', 'level_to', old('level_to', 999999999), ['class' => 'form-control', 'min' => '0', 'step' => '1']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('direct_downlines', trans('admin_backend.direct_downlines')) !!}
						<div class="row">
							<div class="col-sm-5">
								{!! Form::input('number', 'direct_downlines_from', old('direct_downlines_from', 0), ['class' => 'form-control', 'min' => '0', 'step' => '1']) !!}
							</div>
							<div class="col-sm-5">
								{!! Form::input('number', 'direct_downlines_to', old('direct_downlines_to', 999999999), ['class' => 'form-control', 'min' => '0', 'step' => '1']) !!}
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('group_placements', trans('admin_backend.group_placements')) !!}
						<div class="row">
							<div class="col-sm-5">
								{!! Form::input('number', 'group_placements_from', old('group_placements_from', 0), ['class' => 'form-control', 'min' => '0', 'step' => '1']) !!}
							</div>
							<div class="col-sm-5">
								{!! Form::input('number', 'group_placements_to', old('group_placements_to', 999999999), ['class' => 'form-control', 'min' => '0', 'step' => '1']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('type', trans('admin_backend.type')) !!}
						{!! Form::select('type', $typeList, old('type'), ['class' => 'form-control']) !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('status', trans('admin_backend.status')) !!}
						{!! Form::select('status', $statusList, old('status'), ['class' => 'form-control']) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('joined', trans('admin_backend.joined')) !!}
						<div class="row">
							<div class="col-sm-5">
								{!! Form::input('date', 'joined_from', old('joined_from'), ['class' => 'form-control']) !!}
							</div>
							<div class="col-sm-5">
								{!! Form::input('date', 'joined_to', old('joined_to'), ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('register_country', trans('admin_backend.register_country')) !!}
						{!! Form::select('register_country', $countryList, old('register_country'), ['class' => 'form-control']) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					{!! Form::submit(trans('admin_backend.search'), ['class' => 'btn btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}

			<hr />

			<p>
				<a href="{{ route('admin.sponsor.batch.activate') }}" class="btn btn-success batch-action" data-table-id="sponsor_list"><span class="glyphicon glyphicon-ok-sign"></span> @lang('admin_backend.activate')</a>
				<a href="{{ route('admin.sponsor.batch.deactivate') }}" class="btn btn-warning batch-action" data-table-id="sponsor_list"><span class="glyphicon glyphicon-remove-sign"></span> @lang('admin_backend.deactivate')</a>
			</p>
			<table id="sponsor_list" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th class="text-center">{!! Form::checkbox('select_all', '1', false, ['class' => 'form-control select_all', 'data-table-id' => 'sponsor_list']) !!}</th>
						<th>@lang('admin_backend.level')</th>
						<th>@lang('admin_backend.sponsor')</th>
						<th>@lang('admin_backend.username')</th>
						<th>@lang('admin_backend.direct_downlines')</th>
						<th>@lang('admin_backend.group_placements')</th>
						<th>@lang('admin_backend.type')</th>
						<th>@lang('admin_backend.status')</th>
						<th>@lang('admin_backend.joined')</th>
						<th>@lang('admin_backend.register_country')</th>
						<th>@lang('admin_backend.actions')</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section><!-- /.content -->
@stop

@section('scripts')
{!! HTML::script("plugins/datatables/media/js/jquery.dataTables.min.js") !!}
{!! HTML::script("plugins/datatables/media/js/dataTables.bootstrap.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Select/js/dataTables.select.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.bootstrap.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.flash.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.print.min.js") !!}
<script type="text/javascript">
	$(function() {
		var list = $('#sponsor_list').DataTable({
			"order" : [[1, "asc"]],
			"ajax" : "{{ route('admin.sponsor.data') }}",
			"columnDefs" : [{
				"className" : 'select-checkbox',
				"render" : function(data, type, row) {
					return '';
				},
				"targets" : 0,
				"orderable" : false,
				"searchable" : false
			}, {
				"targets" : -1,
				"orderable" : false,
				"searchable" : false
			}],
			"fnServerParams" : function(data) {
				data.filter = $('#search_form').serialize();
			},
			columns : [{
				data : 'id'
			}, {
				data : 'level'
			}, {
				data : 'sponsor_username'
			}, {
				data : 'username'
			}, {
				data : 'direct_downlines'
			}, {
				data : 'group_placements'
			}, {
				data : 'type'
			}, {
				data : 'status'
			}, {
				data : 'joined'
			}, {
				data : 'register_country'
			}, {
				data : 'actions'
			}]
		});

		$('#search_form').submit(function(e) {
			e.preventDefault();
			list.ajax.reload();
		});
	}); 
</script>
@stop