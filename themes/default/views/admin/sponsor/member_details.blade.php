<div class="table-responsive">
	<table class="table table-striped table-bordered">
		<tr>
			<td>@lang('admin_backend.username')</td><td>{{ $sponsor->member->username }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.joined')</td><td>{{ $sponsor->member->joined->format('d/m/Y') }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.status')</td><td>{{ $sponsor->member->status_title }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.type')</td><td>{{ $sponsor->member->type_title }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.register_country')</td><td>{{ $sponsor->member->register_country_title }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.total_direct_downlines')</td><td>{{ $sponsor->downlines()->count() }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.total_sponsor_downlines')</td><td>{{ $sponsor->groups()->count() }}</td>
		</tr>
	</table>
</div>