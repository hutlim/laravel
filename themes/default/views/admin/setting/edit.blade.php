{!! Form::open() !!}

@include('admin.layouts.notifications')

<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('param') ? 'has-error' : '' }}">
            {!! Form::label('param', trans('admin_backend.param')) !!}
            {!! Form::text('param', old('param', $setting->param), ['class' => 'form-control', 'id' => 'param', 'disabled' => 'disabled']) !!}
            <small class="form-text text-muted">@lang('admin_backend.alpha_underscore_allowed')</small>
            @foreach ($errors->get('param') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>

        <div class="form-group {{ $errors->has('remark') ? 'has-error' : '' }}">
            {!! Form::label('remark', trans('admin_backend.remark')) !!}
            {!! Form::text('remark', old('remark', $setting->remark), ['class' => 'form-control', 'id' => 'remark']) !!}
            @foreach ($errors->get('remark') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('value') ? 'has-error' : '' }}">
            {!! Form::label('value', trans('admin_backend.value')) !!}
            {!! Form::text('value', old('value', $setting->value), ['class' => 'form-control', 'id' => 'value']) !!}
            @foreach ($errors->get('value') as $error)
            <p class="help-block error">
                {{ $error }}
            </p>
            @endforeach
        </div>
    </div>
</div>
{!! Form::close() !!}