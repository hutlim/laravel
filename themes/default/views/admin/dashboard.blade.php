@extends('admin.layouts.master')

@section('title') @parent :: @lang('admin_backend.home')
@stop

@section('styles')

@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    @lang('admin_backend.home')
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> @lang('admin_backend.home')</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Info boxes -->
  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green">PH</span>
        <div class="info-box-content">
          <span class="info-box-text">@lang('admin_backend.total_ph')</span>
          <span class="info-box-number">
          	${{ $totalPH }}
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red">GH</span>
        <div class="info-box-content">
          <span class="info-box-text">@lang('admin_backend.total_gh')</span>
          <span class="info-box-number">
          	${{ $totalGH }}
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">@lang('admin_backend.total_members')</span>
          <span class="info-box-number">
          	{{ $totalMember }}
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-dollar"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">@lang('admin_backend.total_mavro')</span>
          <span class="info-box-number">
          	${{ $totalMavro }}
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-12">
          <!-- MEMBERS LIST -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">@lang('admin_backend.latest_members')</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
              <ul class="users-list clearfix">
              	@foreach ($members as $member)
              		<li>
                      <a class="users-list-name edit-action" href="{{ route('admin.member.edit', ['id' => $member->id]) }}" data-title="@lang('admin_backend.edit_member')">{{ strtoupper($member->username) }}</a>
                      <span class="users-list-date">{{ $member->joined->subDays(5)->diffForHumans() }}</span>
                    </li>
				@endforeach
              </ul><!-- /.users-list -->
            </div><!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="{{ route('admin.member.list') }}" class="uppercase btn btn-sm btn-default btn-flat">@lang('admin_backend.view_all_members')</a>
            </div><!-- /.box-footer -->
          </div><!--/.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->

      <!-- TABLE: LATEST PH -->
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">@lang('admin_backend.latest_ph')</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
                <tr>
                  <th>@lang('admin_backend.reference')</th>
                  <th>@lang('admin_backend.type')</th>
                  <th>@lang('admin_backend.status')</th>
                  <th>@lang('admin_backend.amount')</th>
                  <th>@lang('admin_backend.outstanding')</th>
                </tr>
              </thead>
              <tbody>
              	@foreach ($phOrders as $phOrder)
                <tr>
                  <td><a href="{{ route('admin.ph_order.edit', ['id' => $phOrder->id]) }}" class="edit-action">{{ $phOrder->reference }}</a></td>
                  <td>{{ $phOrder->type_title }}</td>
                  <td><span class="label label-{{ $phOrder->status_color }}">{{ $phOrder->status_title }}</span></td>
                  <td>{{ $phOrder->amount }}</td>
                  <td>{{ $phOrder->outstanding }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div><!-- /.table-responsive -->
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
          <a href="{{ route('admin.ph_order.create') }}" class="uppercase btn btn-sm btn-info btn-flat pull-left add-action" data-title="@lang('admin_backend.create_ph_order')">@lang('admin_backend.create_ph_order')</a>
          <a href="{{ route('admin.ph_order.list') }}" class="uppercase btn btn-sm btn-default btn-flat pull-right">@lang('admin_backend.view_all_ph_orders')</a>
        </div><!-- /.box-footer -->
      </div><!-- /.box -->
      
      <!-- TABLE: LATEST GH -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">@lang('admin_backend.latest_gh')</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
                <tr>
                  <th>@lang('admin_backend.reference')</th>
                  <th>@lang('admin_backend.type')</th>
                  <th>@lang('admin_backend.status')</th>
                  <th>@lang('admin_backend.amount')</th>
                  <th>@lang('admin_backend.outstanding')</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($ghOrders as $ghOrder)
                <tr>
                  <td><a href="{{ route('admin.gh_order.edit', ['id' => $ghOrder->id]) }}" class="edit-action">{{ $ghOrder->reference }}</a></td>
                  <td>{{ $ghOrder->type_title }}</td>
                  <td><span class="label label-{{ $ghOrder->status_color }}">{{ $ghOrder->status_title }}</span></td>
                  <td>{{ $ghOrder->amount }}</td>
                  <td>{{ $ghOrder->outstanding }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div><!-- /.table-responsive -->
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
          <a href="{{ route('admin.gh_order.create') }}" class="uppercase btn btn-sm btn-info btn-flat pull-left add-action" data-title="@lang('admin_backend.create_gh_order')">@lang('admin_backend.create_gh_order')</a>
          <a href="{{ route('admin.gh_order.list') }}" class="uppercase btn btn-sm btn-default btn-flat pull-right">@lang('admin_backend.view_all_gh_orders')</a>
        </div><!-- /.box-footer -->
      </div><!-- /.box -->
    </div><!-- /.col -->

    <div class="col-md-4">
      <!-- Info Boxes Style 2 -->
      <div class="info-box bg-green">
      	<span class="info-box-icon">PH</span>
        <div class="info-box-content">
          <span class="info-box-text">@lang('admin_backend.queue_ph')</span>
          <span class="info-box-number">{{ $queuePH->count() }}</span>
          <div class="progress">
            <div class="progress-bar" style="width: 100%"></div>
          </div>
          <span class="progress-description">
            ${{ $queuePH->sum('amount') }}
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
      <div class="info-box bg-yellow">
      	<span class="info-box-icon">PH</span>
        <div class="info-box-content">
          <span class="info-box-text">@lang('admin_backend.process_ph')</span>
          <span class="info-box-number">{{ $processPH->count() }}</span>
          <div class="progress">
            <div class="progress-bar" style="width: 100%"></div>
          </div>
          <span class="progress-description">
            ${{ $processPH->sum('amount') }}
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
      <div class="info-box bg-red">
        <span class="info-box-icon">GH</span>
        <div class="info-box-content">
          <span class="info-box-text">@lang('admin_backend.queue_gh')</span>
          <span class="info-box-number">{{ $queueGH->count() }}</span>
          <div class="progress">
            <div class="progress-bar" style="width: 100%"></div>
          </div>
          <span class="progress-description">
            ${{ $queueGH->sum('amount') }}
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
      <div class="info-box bg-yellow">
        <span class="info-box-icon">GH</span>
        <div class="info-box-content">
          <span class="info-box-text">@lang('admin_backend.process_gh')</span>
          <span class="info-box-number">{{ $processGH->count() }}</span>
          <div class="progress">
            <div class="progress-bar" style="width: 100%"></div>
          </div>
          <span class="progress-description">
            ${{ $processGH->sum('amount') }}
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->

      <!-- PRODUCT LIST -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">@lang('admin_backend.top_ph_member')</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <ul class="nav nav-pills nav-stacked"> 
          	@if($topPH->count())
	          @foreach ($topPH as $ph)
	          <li>
	            <a href="javascript::;">{{ $ph->member->username }} ({{ $ph->member->name }}) <span class="pull-right text-success">${{ $ph->total_amount }}</span></a>
	          </li>
	          @endforeach
	        @else
	          <li>
	            <a href="javascript::;">@lang('admin_backend.no_ph_completed')</a>
	          </li>
            @endif
          </ul>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
@stop

@section('scripts')
<script type="text/javascript">
    $(function () {

    });
</script>
@stop