{!! Form::open(['id' => 'member_edit_form']) !!}

@include('admin.layouts.notifications')
<div class="row">
	<div class="col-md-6">
		<div class="form-group {{ $errors->has('sponsor_username') ? 'has-error' : '' }}">
			{!! Form::label('sponsor_username', trans('admin_backend.sponsor_username')) !!}
			<div class="input-group">
				{!! Form::text('sponsor_username', old('sponsor_username', $member->sponsor->upline && $member->sponsor->upline->member ? $member->sponsor->upline->member->username : ''), ['class' => 'form-control', 'id' => 'sponsor_username', 'placeholder' => trans('admin_backend.sponsor_username'), 'autocomplete' => 'off']) !!}
				<span class="input-group-btn">
					<button class="btn btn-default check-sponsor" type="button">
						@lang('admin_backend.check')
					</button> </span>
			</div>
			{!! Form::text('sponsor_name', old('sponsor_name', $member->sponsor->upline && $member->sponsor->upline->member ? $member->sponsor->upline->member->name : ''), ['class' => 'form-control', 'id' => 'sponsor_name', 'readonly' => 'readonly']) !!}
			@foreach ($errors->get('sponsor_username') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('placement_username') ? 'has-error' : '' }}">
			{!! Form::label('placement_username', trans('admin_backend.placement_username')) !!}
			<div class="input-group">
				{!! Form::hidden('placement_id', $member->placement->id, ['id' => 'placement_id']) !!}
				{!! Form::text('placement_username', old('placement_username', $member->placement->upline && $member->placement->upline->member ? $member->placement->upline->member->username : ''), ['class' => 'form-control', 'id' => 'placement_username', 'placeholder' => trans('admin_backend.placement_username'), 'autocomplete' => 'off']) !!}
				<span class="input-group-btn">
					<button class="btn btn-default check-placement" type="button">
						@lang('admin_backend.check')
					</button> </span>
			</div>
			{!! Form::text('placement_name', old('placement_name', $member->placement->upline && $member->placement->upline->member ? $member->placement->upline->member->name : ''), ['class' => 'form-control', 'id' => 'placement_name', 'readonly' => 'readonly']) !!}
			{!! Form::select('placement_position', $positionList, old('placement_position', $member->placement->seqno), ['class' => 'form-control', 'id' => 'placement_position']) !!}
			@foreach ($errors->get('placement_username') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
			{!! Form::label('username', trans('admin_backend.username')) !!}
			{!! Form::text('username', old('username', $member->username), ['class' => 'form-control', 'placeholder' => trans('admin_backend.username')]) !!}
			@foreach ($errors->get('username') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
			{!! Form::label('email', trans('admin_backend.email')) !!}
			{!! Form::email('email', old('email', $member->email), ['class' => 'form-control', 'placeholder' => trans('admin_backend.email')]) !!}
			@foreach ($errors->get('email') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
			{!! Form::label('name', trans('admin_backend.name')) !!}
			{!! Form::text('name', old('name', $member->name), ['class' => 'form-control', 'placeholder' => trans('admin_backend.name')]) !!}
			@foreach ($errors->get('name') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
			{!! Form::label('mobile', trans('admin_backend.mobile')) !!}
			{!! Form::text('mobile', old('mobile', $member->mobile), ['class' => 'form-control', 'placeholder' => trans('admin_backend.mobile')]) !!}
			@foreach ($errors->get('mobile') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
			{!! Form::label('password', trans('admin_backend.password')) !!}
			{!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('admin_backend.password')]) !!}
			@foreach ($errors->get('password') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group">
			{!! Form::label('password_confirmation', trans('admin_backend.confirm_password')) !!}
			{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('admin_backend.confirm_password')]) !!}
		</div>

		<div class="form-group {{ $errors->has('security_password') ? 'has-error' : '' }}">
			{!! Form::label('security_password', trans('admin_backend.security_password')) !!}
			{!! Form::password('security_password', ['class' => 'form-control', 'placeholder' => trans('admin_backend.security_password')]) !!}
			@foreach ($errors->get('security_password') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group">
			{!! Form::label('security_password_confirmation', trans('admin_backend.confirm_security_password')) !!}
			{!! Form::password('security_password_confirmation', ['class' => 'form-control', 'placeholder' => trans('admin_backend.confirm_security_password')]) !!}
		</div>

		<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
			{!! Form::label('type', trans('admin_backend.type')) !!}
			{!! Form::select('type', $typeList, old('type', $member->type), ['class' => 'form-control']) !!}
			@foreach ($errors->get('type') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
			{!! Form::label('status', trans('admin_backend.status')) !!}
			{!! Form::select('status', $statusList, old('status', $member->status), ['class' => 'form-control']) !!}
			@foreach ($errors->get('status') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('joined') ? 'has-error' : '' }}">
			{!! Form::label('joined', trans('admin_backend.joined')) !!}
			{!! Form::input('date', 'joined', old('joined', $member->joined->format('Y-m-d')), ['class' => 'form-control', 'placeholder' => trans('admin_backend.joined')]) !!}
			@foreach ($errors->get('joined') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('register_country') ? 'has-error' : '' }}">
			{!! Form::label('register_country', trans('admin_backend.register_country')) !!}
			{!! Form::select('register_country', Countries::getList(App::getLocale()), old('register_country', $member->register_country), ['class' => 'form-control']) !!}
			@foreach ($errors->get('register_country') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<hr />
	</div>
	<div class="col-md-6">
		<h3>@lang('admin_backend.residential_address')</h3>
		<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
			{!! Form::label('address', trans('admin_backend.address')) !!}
			{!! Form::text('address', old('address', $member->address), ['class' => 'form-control', 'placeholder' => trans('admin_backend.address')]) !!}
			@foreach ($errors->get('address') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
			{!! Form::label('city', trans('admin_backend.city')) !!}
			{!! Form::text('city', old('city', $member->city), ['class' => 'form-control', 'placeholder' => trans('admin_backend.city')]) !!}
			@foreach ($errors->get('city') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('region') ? 'has-error' : '' }}">
			{!! Form::label('region', trans('admin_backend.region')) !!}
			{!! Form::text('region', old('region', $member->region), ['class' => 'form-control', 'placeholder' => trans('admin_backend.region')]) !!}
			@foreach ($errors->get('region') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('postal') ? 'has-error' : '' }}">
			{!! Form::label('postal', trans('admin_backend.postal')) !!}
			{!! Form::text('postal', old('postal', $member->postal), ['class' => 'form-control', 'placeholder' => trans('admin_backend.postal')]) !!}
			@foreach ($errors->get('postal') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
			{!! Form::label('country', trans('admin_backend.country')) !!}
			{!! Form::select('country', Countries::getList(App::getLocale()), old('country', $member->country), ['class' => 'form-control']) !!}
			@foreach ($errors->get('country') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<hr />

		<h3>@lang('admin_backend.bank_details')</h3>
		<div class="form-group {{ $errors->has('bank_country') ? 'has-error' : '' }}">
			{!! Form::label('bank_country', trans('admin_backend.bank_country')) !!}
			{!! Form::select('bank_country', Countries::getList(App::getLocale()), old('bank_country', $member->bank_country), ['class' => 'form-control']) !!}
			@foreach ($errors->get('bank_country') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('bank_name') ? 'has-error' : '' }}">
			{!! Form::label('bank_name', trans('admin_backend.bank_name')) !!}
			{!! Form::text('bank_name', old('bank_name', $member->bank_name), ['class' => 'form-control', 'placeholder' => trans('admin_backend.bank_name')]) !!}
			@foreach ($errors->get('bank_name') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('bank_currency') ? 'has-error' : '' }}">
			{!! Form::label('bank_currency', trans('admin_backend.bank_currency')) !!}
			{!! Form::text('bank_currency', old('bank_currency', $member->bank_currency), ['class' => 'form-control', 'placeholder' => trans('admin_backend.bank_currency')]) !!}
			@foreach ($errors->get('bank_currency') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('swift_code') ? 'has-error' : '' }}">
			{!! Form::label('swift_code', trans('admin_backend.swift_code')) !!}
			{!! Form::text('swift_code', old('swift_code', $member->swift_code), ['class' => 'form-control', 'placeholder' => trans('admin_backend.swift_code')]) !!}
			@foreach ($errors->get('swift_code') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('account_number') ? 'has-error' : '' }}">
			{!! Form::label('account_number', trans('admin_backend.account_number')) !!}
			{!! Form::text('account_number', old('account_number', $member->account_number), ['class' => 'form-control', 'placeholder' => trans('admin_backend.account_number')]) !!}
			@foreach ($errors->get('account_number') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>

		<div class="form-group {{ $errors->has('account_holder') ? 'has-error' : '' }}">
			{!! Form::label('account_holder', trans('admin_backend.account_holder')) !!}
			{!! Form::text('account_holder', old('account_holder', $member->account_holder), ['class' => 'form-control', 'placeholder' => trans('admin_backend.account_holder')]) !!}
			@foreach ($errors->get('account_holder') as $error)
			<p class="help-block error">
				{{ $error }}
			</p>
			@endforeach
		</div>
	</div>
</div>

{!! Form::close() !!}

<script type="text/javascript">
	$(function() {
		var form = $('#member_edit_form');
		form.find("#sponsor_username").typeahead({
			ajax: {
				url: '{{ route('member.load_sponsor') }}',
				triggerLength: 2,
				preDispatch: function (query) {
					return {
						search : query,
						placement : form.find('#placement_username').val()
					}
				}, 
				preProcess: function (data) {
					return data;
				}
			}
		});

		form.find('button.check-sponsor').on('click', function(e) {
			e.preventDefault();
			var self = $(this), input = form.find('#sponsor_username');
	
			if (input.val() == '') {
				alert('@lang('admin_backend.sponsor_username_required')');
				return false;
			}
	
			$.ajax({
				url : "{{ route('member.check_sponsor') }}",
				dataType : 'json',
				data : {
					'username' : input.val()
				},
				beforeSend : function(jqXHR, settings) {
					self.button('loading');
					input.prop('disabled', true);
					form.find('#sponsor_name').val('');
				},
				success : function(json) {
					if (json.result == 'success') {
						form.find('#sponsor_name').val(json.data.name);
					}
	
					if (json.message != '') {
						alert(json.message);
					}
				}
			}).fail(function(e) {
				self.button('reset');
				input.prop('disabled', false);
				if (e.status === 401) {
					$(location).prop('pathname', 'admin/login');
				} else {
					alert('Error code ' + e.status + ': ' + e.statusText);
				}
			}).always(function() {
				self.button('reset');
				input.prop('disabled', false);
			});
		});

		form.find("#placement_username").typeahead({
			ajax: {
				url: '{{ route('member.load_placement') }}',
				triggerLength: 2,
				preDispatch: function (query) {
					return {
						search : query,
						sponsor : form.find('#sponsor_username').val()
					}
				}, 
				preProcess: function (data) {
					return data;
				}
			}
		});

		form.find('button.check-placement').on('click', function(e) {
			e.preventDefault();
			var self = $(this), input = form.find('#placement_username'), id = form.find('#placement_id').val(), position = form.find('#placement_position').val(), sponsor = form.find('#sponsor_username').val();
	
			if (sponsor == '') {
				alert('@lang('admin_backend.sponsor_username_required')');
				return false;
			} else if (input.val() == '') {
				alert('@lang('admin_backend.placement_username_required')');
				return false;
			}
	
			$.ajax({
				url : "{{ route('member.check_placement') }}",
				dataType : 'json',
				data : {
					'username' : input.val(),
					'id' : id,
					'position' : position,
					'sponsor' : sponsor
				},
				beforeSend : function(jqXHR, settings) {
					self.button('loading');
					input.prop('disabled', true);
					form.find('#placement_name').val('');
				},
				success : function(json) {
					if (json.result == 'success') {
						form.find('#placement_name').val(json.data.name);
					}
	
					if (json.message != '') {
						alert(json.message);
					}
				}
			}).fail(function(e) {
				self.button('reset');
				input.prop('disabled', false);
				if (e.status === 401) {
					$(location).prop('pathname', 'admin/login');
				} else {
					alert('Error code ' + e.status + ': ' + e.statusText);
				}
			}).always(function() {
				self.button('reset');
				input.prop('disabled', false);
			});
		});
	});
</script>