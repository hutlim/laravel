<p>
	<a href="#" id="previous" data-username="{{ $previous }}" class="btn btn-primary @if(!$previous) disabled @endif">@lang('admin_backend.previous_level')</a>
</p>

<div class="team-table table-responsive">
	<table width="100%" class="table table-striped">
		<tbody>
			@if($top)
			<tr>
				<td colspan="{{ $top['colspan'] }}" width="{{ $top['width'] }}" class="text-center"><a class="view_tree" href="#" data-username="{{ $top['data']->member->username }}"><span class="glyphicon glyphicon-chevron-up" style="font-size: 40px"></span></a>
				<div>
					<b>@lang('admin_backend.top')</b>
				</div><a class="node view_member" data-toggle="tooltip" href="#" title="@lang('admin_backend.click_view_member_details')" data-id="{{ $top['data']->id }}"><img src="/themes/default/img/{{ $top['icon'] }}" border="0">
				<br />
				{{ $top['data']->member->username }}</a> @lang('admin_backend.left_point', ["left" => (int)$top["data"]->left_bv]) | @lang('admin_backend.right_point', ["right" => (int)$top["data"]->right_bv])
				<p>
					.
				</p>
				<p>
					<a class="view_tree" href="#" data-username="{{ $previous }}"><span class="glyphicon glyphicon-chevron-up" style="font-size: 40px"></span></a>
				</p></td>
			</tr>
			@endif
			@foreach($node_data as $key => $nodes)
			<tr class="level{{ $key }}">
				@foreach($nodes as $node)
				<td colspan="{{ $node['colspan'] }}" width="{{ $node['width'] }}" class="text-center"> @if($node['data']->id > 0) <a class="node view_member" data-toggle="tooltip" href="#" title="@lang('admin_backend.click_view_member_details')" data-id="{{ $node['data']->id }}"><img src="/themes/default/img/{{ $node['icon'] }}" border="0">
				<br />
				{{ $node['data']->member->username }}</a> @if(!$node['last'])
				@lang('admin_backend.left_point', ["left" => (int)$node["data"]->left_bv]) | @lang('admin_backend.right_point', ["right" => (int)$node["data"]->right_bv])
				@endif

				@if($node['next']) <a class="node view_tree" href="#" data-username="{{ $node['next'] }}"><span class="glyphicon glyphicon-chevron-down" style="font-size: 40px"></span></a> @endif
				@elseif($node['upline'] != '') <a class="node" href="#" data-toggle="tooltip" title="@lang('admin_backend.click_register_new_member')"> <img src="/themes/default/img/{{ $node['icon'] }}" border="0">
				<br />
				@lang('admin_backend.register') </a> @else <a class="node"> <img src="/themes/default/img/{{ $node['icon'] }}" border="0">
				<br />
				</a> @endif </td>
				@endforeach
			</tr>
			@endforeach
		</tbody>
	</table>
</div>