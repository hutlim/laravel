@extends('admin.layouts.master')

@section('title') @parent :: @lang('admin_backend.placements')
@stop

@section('styles')
@style('admin/placement_tree.css')
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> @lang('admin_backend.placements') <small>@lang('admin_backend.tree')</small></h1>
	<ol class="breadcrumb">
		<li>
			<a href="/"><i class="fa fa-home"></i> @lang('admin_backend.home')</a>
		</li>
		<li class="active">
			@lang('admin_backend.placements')
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
		<!-- /.box-header -->
		<div class="box-body">

			@include('admin.layouts.notifications')

			<div class="input-group">
				{!! Form::text('search_text', old('search_text', $root->member->username), ['class' => 'form-control', 'id' => 'search_text']) !!}
				<span class="input-group-btn"> <a href="#" class="btn btn-default" id="search">@lang('admin_backend.search')</a> </span>
			</div>
			<br />
			<div id="tree" class="tree" data-process-url="{{ route('admin.placement.process_tree') }}"></div>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section><!-- /.content -->

<div class="modal fade" id="member_details" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
				<h4 class="modal-title">@lang('admin_backend.member_details')</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">
					@lang('admin_backend.close')
				</button>
				<a href="#" data-username="" class="btn btn-primary view_tree">@lang('admin_backend.view_tree')</a>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
@script('admin/placement_tree.min.js')
@stop