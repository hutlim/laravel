<div class="table-responsive">
	<table class="table table-striped table-bordered">
		<tr>
			<td>@lang('admin_backend.username')</td><td colspan="2">{{ $placement->member->username }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.joined')</td><td colspan="2">{{ $placement->member->joined->format('d/m/Y') }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.status')</td><td colspan="2">{{ $placement->member->status_title }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.type')</td><td colspan="2">{{ $placement->member->type_title }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.register_country')</td><td colspan="2">{{ $placement->member->register_country_title }}</td>
		</tr>
		<tr>
			<th></th><th class="text-right">@lang('admin_backend.left')</th><th class="text-right">@lang('admin_backend.right')</th>
		</tr>
		<tr>
			<td>@lang('admin_backend.accumulate_group_bv')</td><td class="text-right">{{ $placement->left_accumulate_bv }}</td><td class="text-right">{{ $placement->right_accumulate_bv }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.today_group_bv')</td><td class="text-right">{{ $placement->left_today_bv }}</td><td class="text-right">{{ $placement->right_today_bv }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.cf_group_bv')</td><td class="text-right">{{ $placement->left_forward_bv }}</td><td class="text-right">{{ $placement->right_forward_bv }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.today_total_group_bv')</td><td class="text-right">{{ $placement->left_bv }}</td><td class="text-right">{{ $placement->right_bv }}</td>
		</tr>
		<tr>
			<td>@lang('admin_backend.total_placement_downlines')</td><td class="text-right">{{ $placement->groups(1)->count() }}</td><td class="text-right">{{ $placement->groups(2)->count() }}</td>
		</tr>
	</table>
</div>