{!! Form::open(['id' => 'placement_edit_form']) !!}

@include('admin.layouts.notifications')

<div class="form-group {{ $errors->has('sponsor_username') ? 'has-error' : '' }}">
	{!! Form::label('sponsor_username', trans('admin_backend.sponsor_username')) !!}
	<div class="input-group">
		{!! Form::text('sponsor_username', old('sponsor_username', $member->sponsor->upline && $member->sponsor->upline->member ? $member->sponsor->upline->member->username : ''), ['class' => 'form-control', 'id' => 'sponsor_username', 'placeholder' => trans('admin_backend.sponsor_username'), 'autocomplete' => 'off']) !!}
		<span class="input-group-btn">
			<button class="btn btn-default check-sponsor" type="button">
				@lang('admin_backend.check')
			</button> </span>
	</div>
	{!! Form::text('sponsor_name', old('sponsor_name', $member->sponsor->upline && $member->sponsor->upline->member ? $member->sponsor->upline->member->name : ''), ['class' => 'form-control', 'id' => 'sponsor_name', 'readonly' => 'readonly']) !!}
	@foreach ($errors->get('sponsor_username') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

<div class="form-group {{ $errors->has('placement_username') ? 'has-error' : '' }}">
	{!! Form::label('placement_username', trans('admin_backend.placement_username')) !!}
	<div class="input-group">
		{!! Form::hidden('placement_id', $member->placement->id, ['id' => 'placement_id']) !!}
		{!! Form::text('placement_username', old('placement_username', $member->placement->upline && $member->placement->upline->member ? $member->placement->upline->member->username : ''), ['class' => 'form-control', 'id' => 'placement_username', 'placeholder' => trans('admin_backend.placement_username'), 'autocomplete' => 'off']) !!}
		<span class="input-group-btn">
			<button class="btn btn-default check-placement" type="button">
				@lang('admin_backend.check')
			</button> </span>
	</div>
	{!! Form::text('placement_name', old('placement_name', $member->placement->upline && $member->placement->upline->member ? $member->placement->upline->member->name : ''), ['class' => 'form-control', 'id' => 'placement_name', 'readonly' => 'readonly']) !!}
	{!! Form::select('placement_position', $positionList, old('placement_position', $member->placement->seqno), ['class' => 'form-control', 'id' => 'placement_position']) !!}
	@foreach ($errors->get('placement_username') as $error)
	<p class="help-block error">
		{{ $error }}
	</p>
	@endforeach
</div>

{!! Form::close() !!}

<script type="text/javascript">
	$(function() {
		var form = $('#placement_edit_form');
		form.find("#sponsor_username").typeahead({
			ajax: {
				url: '{{ route('member.load_sponsor') }}',
				triggerLength: 2,
				preDispatch: function (query) {
					return {
						search : query,
						placement : form.find('#placement_username').val()
					}
				}, 
				preProcess: function (data) {
					return data;
				}
			}
		});

		form.find('button.check-sponsor').on('click', function(e) {
			e.preventDefault();
			var self = $(this), input = form.find('#sponsor_username');
	
			if (input.val() == '') {
				alert('@lang('admin_backend.sponsor_username_required')');
				return false;
			}
	
			$.ajax({
				url : "{{ route('member.check_sponsor') }}",
				dataType : 'json',
				data : {
					'username' : input.val()
				},
				beforeSend : function(jqXHR, settings) {
					self.button('loading');
					input.prop('disabled', true);
					form.find('#sponsor_name').val('');
				},
				success : function(json) {
					if (json.result == 'success') {
						form.find('#sponsor_name').val(json.data.name);
					}
	
					if (json.message != '') {
						alert(json.message);
					}
				}
			}).fail(function(e) {
				self.button('reset');
				input.prop('disabled', false);
				if (e.status === 401) {
					$(location).prop('pathname', 'admin/login');
				} else {
					alert('Error code ' + e.status + ': ' + e.statusText);
				}
			}).always(function() {
				self.button('reset');
				input.prop('disabled', false);
			});
		});

		form.find("#placement_username").typeahead({
			ajax: {
				url: '{{ route('member.load_placement') }}',
				triggerLength: 2,
				preDispatch: function (query) {
					return {
						search : query,
						sponsor : form.find('#sponsor_username').val()
					}
				}, 
				preProcess: function (data) {
					return data;
				}
			}
		});

		form.find('button.check-placement').on('click', function(e) {
			e.preventDefault();
			var self = $(this), input = form.find('#placement_username'), id = form.find('#placement_id').val(), position = form.find('#placement_position').val(), sponsor = form.find('#sponsor_username').val();
	
			if (sponsor == '') {
				alert('@lang('admin_backend.sponsor_username_required')');
				return false;
			} else if (input.val() == '') {
				alert('@lang('admin_backend.placement_username_required')');
				return false;
			}
	
			$.ajax({
				url : "{{ route('member.check_placement') }}",
				dataType : 'json',
				data : {
					'username' : input.val(),
					'id' : id,
					'position' : position,
					'sponsor' : sponsor
				},
				beforeSend : function(jqXHR, settings) {
					self.button('loading');
					input.prop('disabled', true);
					form.find('#placement_name').val('');
				},
				success : function(json) {
					if (json.result == 'success') {
						form.find('#placement_name').val(json.data.name);
					}
	
					if (json.message != '') {
						alert(json.message);
					}
				}
			}).fail(function(e) {
				self.button('reset');
				input.prop('disabled', false);
				if (e.status === 401) {
					$(location).prop('pathname', 'admin/login');
				} else {
					alert('Error code ' + e.status + ': ' + e.statusText);
				}
			}).always(function() {
				self.button('reset');
				input.prop('disabled', false);
			});
		});
	});
</script>