@extends('crud.layouts.master')

@section('styles')
{!! HTML::style("plugins/datatables/media/css/dataTables.bootstrap.min.css") !!}
{!! HTML::style("plugins/datatables/extensions/Select/css/select.bootstrap.min.css") !!}
{!! HTML::style("plugins/datatables/extensions/Buttons/css/buttons.bootstrap.min.css") !!}
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        CRUD Manager
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <a href="{!! URL::to('crud/create') !!}" class="btn btn-success margin">Create New CRUD</a>
        <!-- /.box-header -->
        <div class="box-body">

            @include('crud.layouts.notifications')

            <table id="crud_list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>CRUD Name</th>
                    <th>Table Name</th>
                    <th>Font Awesome Class</th>
                    <th>Unique/Primary Column</th>
                    <th>Creatable</th>
                    <th>Editable</th>
                    <th>Viewable</th>
                    <th>Removable</th>
                    <th>Listable</th>
                    <th>Created On</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section><!-- /.content -->
@stop

@section('scripts')
{!! HTML::script("plugins/datatables/media/js/jquery.dataTables.min.js") !!}
{!! HTML::script("plugins/datatables/media/js/dataTables.bootstrap.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Select/js/dataTables.select.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.bootstrap.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.flash.min.js") !!}
{!! HTML::script("plugins/datatables/extensions/Buttons/js/buttons.print.min.js") !!}
<script type="text/javascript">
    $(function () {
        $('#crud_list').dataTable({
        	"scrollX": true,
        	"scrollCollapse": true,
        	"autoWidth": false,
        	"processing": true,
        	"serverSide": true,
        	"ajax": "{!! URL::to('crud/data') !!}",
            "columnDefs": [{
				"targets": [ -1 ],
		      	"orderable": false,
		      	"searchable": false
		    }]
        });
    });
</script>
@stop