@extends('crud.layouts.master')

@section('content')
<section class="content-header">
    <h1>
        CRUD Manager
        <small>Edit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
        <div class="box-body">
            {!! Form::open(['url'=>'/crud/edit/'.$crud->id]) !!}
            <div class="col-md-6">

                @include('crud.layouts.notifications')

                <div class="form-group">
                    <label for="crud_name">Enter CRUD Name</label>
                    <input type="text" class="form-control" name="crud_name" value="{{$crud->crud_name}}" id="crud_name" placeholder="Enter CRUD Name">
                </div>

                <div class="form-group">
                    <label for="table_name">Enter Table Name</label>
                    <input type="text" class="form-control" name="table_name" id="table_name" value="{{$crud->table_name}}" placeholder="Enter Table Name">
                </div>


                <div class="form-group">
                    <label for="needle">Unqiue / Primary Column Name (Will be used for editing and deleting)</label>
                    <input type="text" class="form-control" name="needle" id="needle" value="{{$crud->needle}}" placeholder="Ex : id">
                </div>

                <div class="form-group">
                    <label for="fontawesome_class">Font Awesome Class</label>
                    <input type="text" class="form-control" name="fontawesome_class" id="fontawesome_class" value="{{$crud->fontawesome_class}}" placeholder="Ex : fa fa-ellipsis-v">
                </div>

                <div class="form-group">
                    <label>
                        <input {{$crud->creatable==1?"checked":""}} name="creatable" type="checkbox"/>
                        Should have Create Action
                    </label>
                </div>

                <div class="form-group">
                    <label>
                        <input {{$crud->editable==1?"checked":""}} name="editable" type="checkbox"/>
                        Should have Edit Action
                    </label>
                </div>
                
                <div class="form-group">
                    <label>
                        <input {{$crud->viewable==1?"checked":""}} name="viewable" type="checkbox"/>
                        Should have View Action
                    </label>
                </div>
                
                <div class="form-group">
                    <label>
                        <input {{$crud->removable==1?"checked":""}} name="removable" type="checkbox"/>
                        Should have Delete Action
                    </label>
                </div>

                <div class="form-group">
                    <label>
                        <input {{$crud->listable==1?"checked":""}} name="listable" type="checkbox"/>
                        Should have Listing View
                    </label>
                </div>

                <a href="{!! URL::to('crud/list') !!}" class="btn btn-warning">Cancel</a> <button type="submit" class="btn btn-success" name="submit">Update</button>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</section><!-- /.content -->
@stop