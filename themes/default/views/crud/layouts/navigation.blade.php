<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="@if(!$table_name) active @endif">
                <a href="{!! URL::to('crud/list/') !!}">
                    <i class="fa fa-dashboard"></i> <span>CRUD Manager</span>
                </a>
            </li>
            @foreach($cruds as $crud)
            <li class="treeview @if($table_name==$crud->table_name) active @endif">
                <a href="#">
                    <i class="{{$crud->fontawesome_class}}"></i>
                    <span>{{$crud->crud_name}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                @if($crud->creatable==1||$crud->editable==1||$crud->listable==1)
                <ul class="treeview-menu @if($table_name==$crud->table_name) menu-open @endif">
                    @if($crud->creatable==1)
                        <li><a href="{!! URL::to('table/'.$crud->table_name.'/create') !!}"><i class="fa fa-angle-double-right"></i> Create</a></li>
                    @endif
                    @if($crud->listable==1)
                        <li><a href="{!! URL::to('table/'.$crud->table_name.'/list') !!}"><i class="fa fa-angle-double-right"></i> List</a></li>
                    @endif
                    <li><a href="{!! URL::to('table/'.$crud->table_name.'/settings') !!}"><i class="fa fa-angle-double-right"></i> Settings</a></li>
                </ul>
                @endif
            </li>
            @endforeach
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>