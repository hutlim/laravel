<header class="main-header">
<a href="/" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
  	<span class="logo-mini"><b>ADM</b></span>
  	<!-- logo for regular state and mobile devices -->
  	<span class="logo-lg"><b>Admin Backend</b></span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
<!-- Sidebar toggle button-->
<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</a>
<div class="navbar-custom-menu">
<ul class="nav navbar-nav">
	<li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="glyphicon glyphicon-user"></i>
            <span>{{Auth::guard('admin')->user()->name}} <i class="caret"></i></span>
        </a>
        <ul class="dropdown-menu">
            <li class="user-header">
                <p>
                    {{Auth::guard('admin')->user()->name}}
                    <small>Joined since {{ date("d F Y",strtotime(Auth::guard('admin')->user()->created_at)) }}</small>
                </p>
                <p>
                	{{Auth::guard('admin')->user()->email}}
                </p>
            </li>
            <li class="user-footer">
                <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                    <a href="/admin/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
            </li>
        </ul>
    </li>
</ul>
</div>
</nav>
</header>