@extends('crud.layouts.master')

@section('content')
<section class="content-header">
    <h1>
        CRUD Manager
        <small>Create</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
        <div class="box-body">
            {!! Form::open(['url'=>'/crud/create']) !!}
            <div class="col-md-6">

                @include('crud.layouts.notifications')

                <div class="form-group">
                    <label for="crud_name">Enter CRUD Name</label>
                    <input type="text" class="form-control" name="crud_name" id="crud_name" placeholder="Enter CRUD Name">
                </div>

                <div class="form-group">
                    <label for="table_name">Enter Table Name</label>
                    <input type="text" class="form-control" name="table_name" id="table_name" placeholder="Enter Table Name">
                </div>

                <div class="form-group">
                    <label for="needle">Unqiue / Primary Column Name (Will be used for editing and deleting)</label>
                    <input type="text" class="form-control" name="needle" id="needle" placeholder="Ex : id">
                </div>

                <div class="form-group">
                    <label for="fontawesome_class">Font Awesome Class</label>
                    <input type="text" class="form-control" name="fontawesome_class" id="fontawesome_class" placeholder="Ex : fa fa-ellipsis-v">
                </div>

                <div class="form-group">
                    <label>
                        <input name="creatable" type="checkbox"/>
                        Should have Create Action
                    </label>
                </div>

                <div class="form-group">
                    <label>
                        <input name="editable" type="checkbox"/>
                        Should have Edit Action
                    </label>
                </div>
                
                <div class="form-group">
                    <label>
                        <input name="viewable" type="checkbox"/>
                        Should have View Action
                    </label>
                </div>
                
                <div class="form-group">
                    <label>
                        <input name="removable" type="checkbox"/>
                        Should have Delete Action
                    </label>
                </div>

                <div class="form-group">
                    <label>
                        <input name="listable" type="checkbox"/>
                        Should have Listing View
                    </label>
                </div>

                <a href="{!! URL::to('crud/list') !!}" class="btn btn-warning">Cancel</a> <button type="submit" class="btn btn-success" name="submit">Create</button>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</section><!-- /.content -->
@stop