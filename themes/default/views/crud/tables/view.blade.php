@extends('crud.layouts.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> Table Manage ({{$table->table_name}}) <small>View</small></h1>
	<ol class="breadcrumb">
		<li>
			<a href="/"><i class="fa fa-dashboard"></i> Home</a>
		</li>
		<li class="active">
			View
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
        <div class="box-body">
			<div class="col-md-6">
				@include('crud.layouts.notifications')

				@foreach($columns as $column)
				@if($column->viewable)
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					<p class="form-control">{{$cols[$column->column_name]}}</p>
				</div>
				@endif
				@endforeach
			</div>
		</div>
	</div>
</section><!-- /.content -->
@stop