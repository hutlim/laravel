@extends('crud.layouts.master')

@section('title') @parent :: Table Manage ({{$table->table_name}})
@stop

@section('styles')
{!! HTML::style("plugins/datepicker/datepicker3.css") !!}
{!! HTML::style("plugins/timepicker/bootstrap-timepicker.min.css") !!}
{!! HTML::style("plugins/colorpicker/bootstrap-colorpicker.min.css") !!}
{!! HTML::style("plugins/datetimepicker/bootstrap-datetimepicker.min.css") !!}
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> Table Manage ({{$table->table_name}}) <small>Edit</small></h1>
	<ol class="breadcrumb">
		<li>
			<a href="/"><i class="fa fa-dashboard"></i> Home</a>
		</li>
		<li class="active">
			Edit
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
        <div class="box-body">
			{!! Form::open(['url'=>'/table/'.$table->table_name.'/edit/'.$needle,'files'=>'true']) !!}
			<div class="col-md-6">

				@include('crud.layouts.notifications')

				@foreach($columns as $column)
				@if($column->editable)
				@if($column->type=="text")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					<input type="text" class="form-control" name="{{$column->column_name}}" id="{{$column->column_name}}" value="{{$cols[$column->column_name]}}" placeholder="Enter {{$column->column_name}}">
				</div>
				@endif

				@if($column->type=="password")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					<input name="{{$column->column_name}}" type="password" class="form-control" value="{{$cols[$column->column_name]}}" id="{{$column->column_name}}" placeholder="Enter {{$column->column_name}}">
				</div>
				@endif

				@if($column->type=="number")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					<input name="{{$column->column_name}}" type="number" class="form-control" value="{{$cols[$column->column_name]}}" id="{{$column->column_name}}" placeholder="Enter {{$column->column_name}}">
				</div>
				@endif

				@if($column->type=="textarea")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					<textarea name="{{$column->column_name}}" rows="10" cols="80" class="form-control" id="{{$column->column_name}}">{{$cols[$column->column_name]}}</textarea>
				</div>
				@endif

				@if($column->type=="content_editor")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					<textarea name="{{$column->column_name}}" rows="10" cols="80" class="form-control ckeditor" id="{{$column->column_name}}">{{$cols[$column->column_name]}}</textarea>
				</div>
				@endif

				@if($column->type=="gender_full")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					Male
					<input class="form-control" type="radio" name="{{$column->column_name}}" {{$cols[$column->
					column_name]=="male"?"checked":""}} id="{{$column->column_name}}" value="male"/>
					Female
					<input class="form-control" type="radio" name="{{$column->column_name}}" {{$cols[$column->
					column_name]=="female"?"checked":""}} id="{{$column->column_name}}" value="female"/>
				</div>
				@endif

				@if($column->type=="gender_short")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					Male
					<input class="form-control" type="radio" name="{{$column->column_name}}" {{$cols[$column->
					column_name]=="m"?"checked":""}} id="{{$column->column_name}}" value="m"/>
					Female
					<input class="form-control" type="radio" name="{{$column->column_name}}" {{$cols[$column->
					column_name]=="f"?"checked":""}} id="{{$column->column_name}}" value="f"/>
				</div>
				@endif

				@if($column->type=="true_false")
				<div class="form-group">
					<label>
						<input type="checkbox" name="{{$column->column_name}}" {{$cols[$column->
						column_name]=="true"?"checked":""}} id="{{$column->column_name}}" value="true" /> {{$column->column_name}} </label>
				</div>
				@endif

				@if($column->type=="one_or_zero")
				<div class="form-group">
					<label>
						<input type="checkbox" name="{{$column->column_name}}" {{$cols[$column->
						column_name]=="1"?"checked":""}} id="{{$column->column_name}}" value="1" /> {{$column->column_name}} </label>
				</div>
				@endif

				@if($column->type=="range")
				<div class="form-group">
					<label class="form-control" for="{{$column->column_name}}">{{$column->column_name}}</label>
					<input type="range" min="{{$column->range_from}}"  max="{{$column->range_to}}" value="{{$cols[$column->column_name]}}" name="{{$column->column_name}}" id="{{$column->column_name}}"/>
				</div>
				@endif

				@if($column->type=="file")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					<input class="form-control" type="file" name="{{$column->column_name}}" id="{{$column->column_name}}"/>
					<label>{{$cols[$column->column_name]}}</label>
				</div>
				@endif

				@if($column->type=="date")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					<div class='input-group'>
						<input class="form-control datepickers" type="text" name="{{$column->column_name}}" id="{{$column->column_name}}" />
						<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span>
					</div>
				</div>
				@endif

				@if($column->type=="datetime")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					<div class='input-group'>
						<input id="{{$column->column_name}}" name="{{$column->column_name}}" type='text' class="form-control" />
						<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span>
					</div>
				</div>
				@endif

				@if($column->type=="time")
				<div class="bootstrap-timepicker">
					<div class="form-group">
						<label for="{{$column->column_name}}">{{$column->column_name}}</label>
						<div class="input-group">
							<input type="text" name="{{$column->column_name}}" id="{{$column->column_name}}" class="form-control timepickers" />
							<div class="input-group-addon">
								<i class="fa fa-clock-o"></i>
							</div>
						</div>
					</div>
				</div>
				@endif

				@if($column->type=="colorpicker")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					<div class="input-group">
						<input type="text" name="{{$column->column_name}}" id="{{$column->column_name}}" class="form-control colorpickers"/>
						<div class="input-group-addon">
							<i class="fa fa-stop"></i>
						</div>
					</div><!-- /.input group -->
				</div>
				@endif

				@if($column->type=="radio")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					@foreach($column->radios as $radio)
					{{$radio->key}}
					<input type="radio" {{$cols[$column->
					column_name]==$radio->value?"checked":""}} name="{{$column->column_name}}" id="{{$column->column_name}}" value="{{$radio->value}}" class="form-control"/>
					@endforeach
				</div>
				@endif

				@if($column->type=="checkbox")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					@foreach($column->checkboxes as $checkbox)
					{{$checkbox->key}}
					<input type="checkbox" {{$cols[$column->
					column_name]==$checkbox->value?"checked":""}} name="{{$column->column_name}}" id="{{$column->column_name}}" value="{{$checkbox->value}}" class="form-control"/>
					@endforeach
				</div>
				@endif

				@if($column->type=="select")
				<div class="form-group">
					<label for="{{$column->column_name}}">{{$column->column_name}}</label>
					<select name="{{$column->column_name}}" class="form-control">
						@foreach($column->selects as $select)
						<option {{$cols[$column->column_name]==$select->value?"selected":""}} value="{{$select->value}}">{{$select->key}}</option>
						@endforeach
					</select>
				</div>
				@endif

				@endif
				@endforeach

				<button type="submit" class="btn btn-success">Update</button>

			</div>
			{!! Form::close() !!}
		</div>
	</div>
</section>
@stop

@section('scripts')
{!! HTML::script("plugins/ckeditor/ckeditor.js") !!}
{!! HTML::script("plugins/datepicker/bootstrap-datepicker.js") !!}
{!! HTML::script("plugins/timepicker/bootstrap-timepicker.min.js") !!}
{!! HTML::script("plugins/colorpicker/bootstrap-colorpicker.min.js") !!}
{!! HTML::script("js/moment.min.js") !!}
{!! HTML::script("plugins/datetimepicker/bootstrap-datetimepicker.min.js") !!}

<script type="text/javascript">
$(function() {
	$(".datepickers").datepicker();

	@foreach($datetimepickers as $picker)
	$("#{{$picker}}").datetimepicker();
	@endforeach

	$(".timepickers").timepicker({
	showInputs: false
	});

	$(".colorpickers").colorpicker();
});
</script>
@stop