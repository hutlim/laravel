@extends('crud.layouts.master')

@section('title') @parent :: Table Manage ({{$table->table_name}})
@stop

@section('styles')
{!! HTML::style("plugins/datatables/dataTables.bootstrap.css") !!}
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Table Manage ({{$table->table_name}})
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <a href="/table/{{$table->table_name}}/create" class="btn btn-success margin">Create New Entry</a>
        <!-- /.box-header -->
        <div class="box-body">

            @include('crud.layouts.notifications')

            <table id="table_data_list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    @foreach($headers as $header)
                        <th>{{$header}}</th>
                    @endforeach
                        <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section><!-- /.content -->
@stop

@section('scripts')
{!! HTML::script("plugins/datatables/jquery.dataTables.js") !!}
{!! HTML::script("plugins/datatables/dataTables.bootstrap.js") !!}
<script type="text/javascript">
    $(function () {
        $('#table_data_list').dataTable({
            "scrollX": true,
        	"scrollCollapse": true,
        	"autoWidth": false,
        	"processing": true,
        	"serverSide": true,
        	"ajax": "{!! URL::to('table/'.$table->table_name.'/data') !!}",
            "columnDefs": [{
				"targets": [ -1 ],
		      	"orderable": false,
		      	"searchable": false
		    }]
        });
    });
</script>
@stop