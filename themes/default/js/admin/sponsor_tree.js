$(function () {
	// genealogy tree
	$("#tree").jstree({ 
		"core" : {
			"animation" : 0,
			"themes" : { 
				"variant" : "large"
			},
			"data" : {
				"type" : "GET",
	            "global" : false,
				"url" : function (node) {
					return $('#tree').data('process-url');
			    },
			    'data' : function (node) {
			    	return { 
                        "operation" : node.id === '#' ? "TreeRoot" : "TreeChild", 
                        "id" : node.id === '#' ? $("#tree").data('root') : node.data.id
                    };
			    },
			    'dataFilter': function(data, type) {
			    	var parsed_data = JSON.parse(data);
			    	if(parsed_data.result == 'success'){
			    		return JSON.stringify(parsed_data.nodes);
			    	}
			    	else{
			    		alert(parsed_data.msg);
			    		return '[]';
			    	}
			    },
			    'error': function(jqXHR) {
					if(jqXHR.status === 401){
			        	$(location).prop('pathname', 'admin/login');
			       	}
			       	else{
			    		alert('Error code ' + jqXHR.status + ': ' + jqXHR.statusText);
			    	}
			    }
			}
		},
	    // List of active plugins
	    "plugins" : [ 
	        "search", "types"
	    ],
	    // Using types - most of the time this is an overkill
	    // read the docs carefully to decide whether you need types
		"types" : {
			"default" : {
				// If we specify an icon for the default type it WILL OVERRIDE the theme icons
				"icon" : "glyphicon glyphicon-user"
			}
		}
	})
	.bind("select_node.jstree", function (e, obj) {
		if(obj.node.data.id > 0 && obj.node.data.id != undefined){
			$.ajax({
				url: $('#tree').data('process-url'),
			  	method: 'GET',
			  	dataType: 'json',
			  	data: {'operation': 'MemberDetails', 'id': obj.node.data.id},
		  		beforeSend: function(jqXHR){
		    		$('#spinner').show();
		  		},
		  		success: function(data) {
		  			if(data.result == 'success'){
				    	$('#member_details .modal-body').html(data.contents);
				    	$('#member_details').modal('show');
				   	}
				   	else{
						alert(data.msg);
				   	}
			  	}
			})
		  	.fail(function(jqXHR) {
		  		$('#spinner').hide();
		  		if(jqXHR.status === 401){
		        	$(location).prop('pathname', 'admin/login');
		       	}
		       	else{
		    		alert('Error code ' + jqXHR.status + ': ' + jqXHR.statusText);
		    	}
		  	})
		  	.always(function() {
		    	$('#spinner').hide();
		  	});
		}
	});

	$('body').on("click", "#treemenu a", function(e){
		e.preventDefault();
		switch(this.id) {
			case "search":
				var username = $("#search_text").val();
				if(username != '' && username != undefined){
					$.ajax({
						url: $('#tree').data('process-url'),
					  	method: 'GET',
					  	dataType: 'json',
					  	data: {'operation': 'Check', 'id': username},
				  		beforeSend: function(jqXHR){
				    		$('#spinner').show();
				  		},
				  		success: function(data) {
				  			if(data.result == 'success'){
		   						$("#tree").data('root', data.id);
								$("#tree").jstree(true).deselect_all();
								$("#tree").jstree(true).refresh();
							}
							else{
								alert(data.msg);
								$("#search_text").val('');
							}
					  	}
					})
				  	.fail(function(jqXHR) {
				  		$('#spinner').hide();
				  		if(jqXHR.status === 401){
				        	$(location).prop('pathname', 'admin/login');
				       	}
				       	else{
				    		alert('Error code ' + jqXHR.status + ': ' + jqXHR.statusText);
				    	}
				  	})
				  	.always(function() {
				    	$('#spinner').hide();
				  	});
				}
				break;
			case "clear_search":
				$("#search_text").val('');
				$("#tree").data('root', $('#tree').data('default-root'));
				$("#tree").jstree(true).deselect_all();
				$("#tree").jstree(true).refresh();
				break;
			case "refresh":
				$("#tree").jstree(true).deselect_all();
				$("#tree").jstree(true).refresh();
				break;
			case "open_all":
				$("#tree").jstree(true).open_all();
				break;
			case "close_all":
				$("#tree").jstree(true).close_all();
				break;
			case "search_text": break;
			default:
				$("#tree").jstree(this.id);
				break;
		}
	});
});