$(function() {
	// genealogy tree
	function doRefreshTree(username) {
		$.ajax({
			url : $('#tree').data('process-url'),
			method : 'GET',
			dataType : 'json',
			data : {
				'operation' : 'LoadTree',
				'id' : username
			},
			beforeSend : function(jqXHR) {
				$('#spinner').show();
			},
			success : function(data) {
				if (data.result == 'success') {
					$('#tree').html(data.contents);
				} else {
					alert(data.msg);
				}
			}
		}).fail(function(jqXHR) {
			$('#spinner').hide();
			if (jqXHR.status === 401) {
				$(location).prop('pathname', 'admin/login');
			} else {
				alert('Error code ' + jqXHR.status + ': ' + jqXHR.statusText);
			}
		}).always(function() {
			$('#spinner').hide();
		});
	}

	/* Init */
	doRefreshTree($('#search_text').val());

	$('body').on("click", "#search", function(e) {
		e.preventDefault();
		var username = $('#search_text').val();
		if (username != '' && username != undefined) {
			doRefreshTree(username);
		}
	});

	$('body').on("click", "#previous", function(e) {
		e.preventDefault();
		var username = $(this).data('username');
		if (username != '' && username != undefined) {
			$('#search_text').val(username);
			doRefreshTree(username);
		}
	});

	$('body').on("click", "a.view_member", function(e) {
		e.preventDefault();

		var id = $(this).data('id');
		$.ajax({
			url : $('#tree').data('process-url'),
			method : 'GET',
			dataType : 'json',
			data : {
				'operation' : 'MemberDetails',
				'id' : id
			},
			beforeSend : function(jqXHR) {
				$('#spinner').show();
			},
			success : function(data) {
				if (data.result == 'success') {
					$('#member_details .modal-body').html(data.contents);
					$('#member_details .modal-footer').find('a.view_tree').data('username', data.username);
					$('#member_details').modal('show');
				} else {
					alert(data.msg);
				}
			}
		}).fail(function(jqXHR) {
			$('#spinner').hide();
			if (jqXHR.status === 401) {
				$(location).prop('pathname', 'admin/login');
			} else {
				alert('Error code ' + jqXHR.status + ': ' + jqXHR.statusText);
			}
		}).always(function() {
			$('#spinner').hide();
		});
	});

	$('body').on("click", "a.view_tree", function(e) {
		e.preventDefault();
		$('#member_details').modal('hide');
		var username = $(this).data('username');
		if (username != '' && username != undefined) {
			$('#search_text').val(username);
			doRefreshTree(username);
		}
	});
}); 