<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMMMTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mavro_wallets', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->uuid('reference')->unique();
			$table->date('started')->nullable();
			$table->date('closed')->nullable();
			$table->date('next_payout')->nullable();
			$table->decimal('balance', 15, 2);
			$table->string('status');
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('member_id')->unsigned();
			$table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
		
            $table->string('type')->unsigned();
            $table->foreign('type')->references('code')->on('mavro_type')->onDelete('cascade');
        });
		
		Schema::create('mavro_transactions', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->dateTime('date');
			$table->string('type');
			$table->uuid('reference');
			$table->decimal('credit', 15, 2);
			$table->decimal('debit', 15, 2);
			$table->decimal('balance', 15, 2);
			$table->timestamps();
			$table->softDeletes();
			
			$table->index('reference');
			
			$table->integer('mavro_wallet_id')->unsigned();
			$table->foreign('mavro_wallet_id')->references('id')->on('mavro_wallets')->onDelete('cascade');
		});
		
		Schema::create('mavro_type', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('code')->unique();
			$table->string('title');
			$table->decimal('dividend', 15, 2);
			$table->integer('dividend_period');
			$table->string('dividend_period_type');
			$table->boolean('status');
			$table->timestamps();
			$table->softDeletes();
		});
		
		Schema::create('ph_orders', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('type');
			$table->uuid('reference')->unique();
			$table->decimal('amount', 15, 2);
			$table->decimal('outstanding', 15, 2);
			$table->string('status');
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('member_id')->unsigned();
			$table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
			
			$table->integer('mavro_wallet_id')->unsigned();
			$table->foreign('mavro_wallet_id')->references('id')->on('mavro_wallets')->onDelete('cascade');
		});
		
		Schema::create('gh_orders', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->uuid('reference')->unique();
			$table->decimal('amount', 15, 2);
			$table->decimal('outstanding', 15, 2);
			$table->string('bank_name');
			$table->string('bank_currency', 3);
			$table->string('swift_code');
			$table->string('bank_country', 2);
			$table->string('account_number');
			$table->string('account_holder');
			$table->string('status');
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('member_id')->unsigned();
			$table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
			
			$table->integer('mavro_wallet_id')->unsigned();
			$table->foreign('mavro_wallet_id')->references('id')->on('mavro_wallets')->onDelete('cascade');
		});
		
		Schema::create('order_transactions', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->uuid('reference')->unique();
			$table->dateTime('started');
			$table->dateTime('ended');
			$table->decimal('amount', 15, 2);
			$table->string('status');
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('ph_order_id')->unsigned();
			$table->foreign('ph_order_id')->references('id')->on('ph_orders')->onDelete('cascade');
			
			$table->integer('ph_member_id')->unsigned();
			$table->foreign('ph_member_id')->references('id')->on('members')->onDelete('cascade');
			
			$table->integer('gh_order_id')->unsigned();
			$table->foreign('gh_order_id')->references('id')->on('gh_orders')->onDelete('cascade');
			
			$table->integer('gh_member_id')->unsigned();
			$table->foreign('gh_member_id')->references('id')->on('members')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mavro_wallets', function(Blueprint $table)
		{
    		$table->dropForeign('mavro_wallets_member_id_foreign');
		});
		Schema::drop('mavro_wallets');
		
		Schema::table('mavro_transaction', function(Blueprint $table)
		{
    		$table->dropForeign('mavro_transactions_mavro_wallet_id_foreign');
		});
		Schema::drop('mavro_transactions');
		
		Schema::table('ph_orders', function(Blueprint $table)
		{
    		$table->dropForeign('ph_orders_member_id_foreign');
			$table->dropForeign('ph_orders_mavro_wallet_id_foreign');
		});
		Schema::drop('ph_orders');
		
		Schema::table('gh_orders', function(Blueprint $table)
		{
    		$table->dropForeign('gh_orders_member_id_foreign');
			$table->dropForeign('gh_orders_mavro_wallet_id_foreign');
		});
		Schema::drop('gh_orders');
		
		Schema::table('order_transactions', function(Blueprint $table)
		{
    		$table->dropForeign('order_transactions_ph_order_id_foreign');
			$table->dropForeign('order_transactions_ph_member_id_foreign');
			$table->dropForeign('order_transactions_gh_order_id_foreign');
			$table->dropForeign('order_transactions_gh_member_id_foreign');
		});
		Schema::drop('order_transactions');
	}

}
