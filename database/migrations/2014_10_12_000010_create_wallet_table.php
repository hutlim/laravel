<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('point_transactions', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->datetime('date');
			$table->string('type');
			$table->uuid('reference');
			$table->string('description', 250);
			$table->decimal('credit', 15, 2);
            $table->decimal('debit', 15, 2);
            $table->decimal('balance', 15, 2);
			$table->timestamps();
			$table->softDeletes();
			
			$table->integer('member_id')->unsigned();
			$table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
		});
		
		Schema::create('credit_transactions', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
			$table->datetime('date');
            $table->string('type');
            $table->uuid('reference');
            $table->string('description', 250);
            $table->decimal('credit', 15, 2);
            $table->decimal('debit', 15, 2);
            $table->decimal('balance', 15, 2);
            $table->timestamps();
            $table->softDeletes();
            
            $table->integer('member_id')->unsigned();
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
        });
        
        Schema::table('members', function($table)
        {
            $table->decimal('point_balance', 15, 2);
            $table->decimal('credit_balance', 15, 2);
        });

		Schema::create('transaction_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code')->unique();
			$table->text('title');
            $table->boolean('status');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('point_transactions', function(Blueprint $table)
		{
    		$table->dropForeign('point_transactions_member_id_foreign');
		});
		Schema::drop('point_transactions');
		
		Schema::table('credit_transactions', function(Blueprint $table)
        {
            $table->dropForeign('credit_transactions_member_id_foreign');
        });
        Schema::drop('credit_transactions');
		Schema::drop('transaction_types');
	}

}
