<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('members', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username')->unique()->index();
			$table->string('name');
			$table->string('email')->index();
			$table->string('password', 60);
			$table->string('security_password', 60);
			$table->string('type')->index();
			$table->boolean('status');
			$table->string('mobile');
			$table->string('passport');
			$table->date('joined');
			$table->string('register_country', 2)->index();
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
			
			$table->string('address');
			$table->string('city');
			$table->string('region');
			$table->string('postal');
			$table->string('country', 2);
			
			$table->string('bank_name');
			$table->string('bank_currency', 3);
			$table->string('swift_code');
			$table->string('bank_country', 2);
			$table->string('account_number');
			$table->string('account_holder');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('members');
	}

}
