<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenealogyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('sponsors',function($table){
            $table->integer('id')->unique()->index();
            $table->integer('left');
            $table->integer('right');
			$table->integer('level');
			$table->integer('seqno');
            $table->timestamps();
			$table->softDeletes();
			
			$table->index('left');
			$table->index('right');
			$table->index('level');
			
			$table->integer('member_id')->unsigned();
			$table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
			
			$table->integer('upline_id');
			$table->index('upline_id');
        });
		
		Schema::create('placements',function($table){
            $table->integer('id')->unique()->index();
            $table->integer('left');
            $table->integer('right');
			$table->integer('level');
			$table->integer('seqno');
            $table->timestamps();
			$table->softDeletes();
			
			$table->index('left');
			$table->index('right');
			$table->index('level');
			
			$table->integer('member_id')->unsigned();
			$table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
			
			$table->integer('upline_id');
			$table->index('upline_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sponsors', function(Blueprint $table)
		{
    		$table->dropForeign('sponsors_member_id_foreign');
			$table->dropForeign('sponsors_parent_id_foreign');
		});
        Schema::dropIfExists("sponsors");
		
		Schema::table('placements', function(Blueprint $table)
		{
    		$table->dropForeign('placements_member_id_foreign');
			$table->dropForeign('placements_parent_id_foreign');
		});
        Schema::dropIfExists("placements");
	}

}
