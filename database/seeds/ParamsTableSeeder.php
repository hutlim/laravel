<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\Models\Param;

class ParamsTableSeeder extends Seeder {

	public function run()
	{
		Param::create([
			'type' => 'user_type',
			'code' => 'super',
			'title' => ['en' => 'Super Admin', 'zh' => '超级管理员'],
			'status' => 1
		]);

		Param::create([
			'type' => 'user_type',
			'code' => 'admin',
			'title' => ['en' => 'Backend Admin', 'zh' => '后台管理员'],
			'status' => 1
		]);
		
		Param::create([
			'type' => 'member_type',
			'code' => 'basic',
			'title' => ['en' => 'Basic Member', 'zh' => '普通会员'],
			'status' => 1
		]);

		Param::create([
			'type' => 'member_type',
			'code' => 'test',
			'title' => ['en' => 'Test Member', 'zh' => '测试会员'],
			'status' => 1
		]);
        
		#TestDummy::times(10)->create('App\User');

	}

}
