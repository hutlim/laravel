<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\Models\User;

class UsersTableSeeder extends Seeder {

	public function run()
	{

		User::create([
			'username' => 'admin',
			'name' => 'Test Admin',
			'email' => 'test@mailinator.com',
			'password' => bcrypt('1234'),
			'status' => 1,
            'type' => 'super'
		]);

		#TestDummy::times(10)->create('App\User');

	}

}
