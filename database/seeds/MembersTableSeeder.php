<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\Models\Member;
use App\Models\Sponsor;
use App\Models\Placement;

class MembersTableSeeder extends Seeder {

	public function run()
	{
		$member = Member::create([
			'username' => 'company',
			'name' => 'Company',
			'email' => 'test@mailinator.com',
			'password' => bcrypt('1234'),
			'security_password' => bcrypt('1234'),
			'status' => 1,
            'type' => 'basic',
            'joined' => date('Y-m-d'),
            'register_country' => 'MY'
		]);
		
		$sponsor = new Sponsor([
			'id' => $member->id,
			'left' => 1,
			'right' => 2
		]);
		$member->sponsor()->save($sponsor);
		
		$placement = new Placement([
			'id' => $member->id,
			'left' => 1,
			'right' => 2
		]);
		$member->placement()->save($placement);

		#TestDummy::times(10)->create('App\Member');

	}

}
