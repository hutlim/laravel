<?php

return array (
  'home_title' => 'Home',
  'profile_title' => 'Profile',
  'ewallet_title' => 'E-Wallet',
  'site_title_short' => 'Site',
  'site_title' => 'Site',
  'logout' => 'Logout',
  'version' => 'Version',
  'copyright' => 'Copyright',
  'home' => 'Home',
  'account_title' => 'Account',
  'data_saved' => 'Data has been saved',
  'member_backend' => 'Member Backend',
  'profile' => 'Profile',
);
