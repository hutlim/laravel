<?php

return array (
  'pending' => 'Pending',
  'paid' => 'Paid',
  'received' => 'Received',
  'cancelled' => 'Cancelled',
  'rejected' => 'Rejected',
);
