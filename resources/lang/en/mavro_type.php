<?php

return array (
  'daily' => 'Daily',
  'weekly' => 'Weekly',
  'bi-weekly' => 'Bi-Weekly',
  'completed' => 'Completed',
  'semi-annually' => 'Semi-Annually',
  'annually' => 'Annually',
  'all' => 'All',
);
