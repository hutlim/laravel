<?php

return array (
  'pending' => 'Pending',
  'started' => 'Started',
  'closed' => 'Closed',
  'cancelled' => 'Cancelled',
  'all' => 'All',
);
