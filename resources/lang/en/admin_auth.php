<?php

return array (
  'forgot_password_title' => 'Forgot Password',
  'email' => 'Email',
  'btn_send_pasword' => 'Send Password Reset Link',
  'reset_password_title' => 'Reset Password',
  'password' => 'Password',
  'password_confirmation' => 'Password Confirmation',
  'btn_reset_password' => 'Reset Password',
  'login_title' => 'Admin Login',
  'remember_me' => 'Remember Me',
  'btn_login' => 'Login',
  'forgot_password' => 'Forgot Your Password?',
  'error' => 'These credentials do not match our records.',
  'admin_backend' => 'Admin Backend',
  'username' => 'Username',
  'captcha' => 'Captcha',
  'reset_password' => 'Reset Password',
  'login' => 'Login',
);
